﻿=== Visitor Statistics Plugin ===
Contributors: osamaesh
Donate link: https://www.paypal.me/osamaesh
Tags: visitors, statistics, online counter, traffic stats, traffic, real time statistics, histats, online, google analytics, analytics, Geo locations, visits, stats, wp Statistics, post, hits, pagerank, alexa, live visit, counter
Requires at least: 3.0
Tested up to: 4.9.4
Stable tag: 1.5
License: GPL3

This plugin will help you to track your visitors & visits, browsers, operating systems, GEO locations and much more, easy to install and working fine.

== Description ==
A comprehensive plugin for your WordPress visitor statistics, Track statistics for your WordPress site without depending on external services. Enable you to display how many users are online on your WordPress blog with detailed statistics, easy to install and working fine.

When it comes to ease of use, WordPress visitor statistics comes in first, You will have a real counter and statistics plugin for your WordPress website..
This plugin will help you to track your visitors, browsers, operating systems, visits and much more in one dashboard page..


= Features =
* Real time statistics
* Online users, visits, visitors with interactive map of visitors location.
* Comprehensive overview page & User-friendly interface
* GeoIP location by Country & city
* Search Engines queries from popular search engines like Google, Bing, DuckDuckGo, Yahoo, Yandex and Baidu
* Interactive map of visitors location
* E-mail reports of statistics
* Automatically prune the databases of old data
* Widgets to provide information to your users
* Shortcodes for many different types of data.
* Dashboard widgets for the admin area
* Automatic updates to the GeoIP database.


= Translations =
Only english..


= Contributing and Reporting Bugs =
WordPress visitor statistics (Histats) is being developed on GitHub, If you’re interested in contributing to plugin, Please look at [Github page](https://github.com/wp-histats)

= Support =
We're happy to help.  Here are a few things to do before contacting us:

* Have you read the [FAQs] (http://wordpress-statistics.com/Documentation)
* Have you search the [support forum](https://wordpress.org/support/plugin/wp-stats-manager) for a similar issue?
* You can also contact us at support@wordpress-statistics.com if you have any enquiry.

== Installation ==
1. Download the package.
2. Upload the package using your plugins page > add new > upload or Extract the contents of .zip folder to wp-content/plugins/ folder
3. Activate the Plugin via plugins page
4. Clear your site cache (if you have any caching plugin enabled)

Thanks!

== Screenshots ==
1. Main Dashboard
2. Today statistics
3. Online users + Google map
4. Settings Page


== Upgrade Notice ==
= 1.3 =
This is a urgent update, please update immediately.


== Changelog ==

= 1.5 =
1. Fix Bounce issue

= 1.4 =
1. Fix total page views issue

= 1.3 =
1. Fix timezone issue

= 1.2 =
1. show alert if another stats plugin is enabled

= 1.1 =
1. Fix schedule cron job issue

= 1.0 =
1. Initial version

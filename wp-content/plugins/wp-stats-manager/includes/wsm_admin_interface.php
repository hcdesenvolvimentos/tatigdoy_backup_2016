<?php
class wsmAdminInterface{
    private $startWrapper;
    private $endWrapper;
    private $startMetaBox;
    private $endMetaBox;
    private $wsmClear;
    private $objDatabase;
    function __construct(){
        $this->startWrapper='<div class="wrap">';
        $this->endWrapper='</div>';
        $this->startMetaBoxWrapper='<div id="dashboard-widgets-wrap" class="wsmMetaboxContainer"><div id="dashboard-widgets" class="metabox-holder">';
        $this->endMetaBoxWrapper='</div></div>';
        $this->wsmClear='<div class="clear"></div>';
        $this->objDatabase=new wsmDatabase();
    }
    function fnPrintTitle($title){
        return '<h1 class="wsmHead">'.WSM_NAME.'</h1>'.$this->fnPrintHeader($title);
    }

	function wsm_upgrade_to_pro()
	{
		return ' style="color:gray" onclick="javascript: if(confirm(\'please upgrade to pro version to use this feature\') == true){window.location.href=\'https://visitor-statistics.com/product/wordpress-statistics/?attribute_license=Regular\';}"';
	}
	
    function fnPrintHeader($active=""){
        global $wsmRequestArray,$wsmAdminJavaScript;
        
        $current=isset($wsmRequestArray['subPage']) && $wsmRequestArray['subPage']!=''?$wsmRequestArray['subPage']:'';
         
        $header='<div class="wmsHorizontalTab">
                    <ul class="wmsTabList">';
                        $class=$active=='Traffic'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_traffic').'" '.$class.'>'.__('Traffic',P_PREFIX).'</a>';
                        $current=$current==''?'Summary':$current;
                        $class=$current=='Summary'?'class="active"':'';
                        $header.='<ul class="wmsSubList sublisthover"><li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_traffic&subPage=Summary').'" '.$class.'>Summary</a></li>';
                        
                        $current=$current==''?'UsersOnline':$current;
                        $class=$current=='UsersOnline'?'class="active"':'';
                        $header .='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_traffic&&subPage=UsersOnline&subTab=summary').'" href="#" '.$class.'>Users Online</a></li>';
                        
                        $current=$current==''?'TrafStats':$current;
                        $class=$current=='TrafStats'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#" '.$class.'>Traffic Stats</a></li></ul>';
                        $header.='</li>';
                        
                        $class=$active=='Traffic Sources'?'class="active"':'';
                        
                        $header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_trafficsrc').'" '.$class.'>'.__('Traffic Sources',P_PREFIX).'</a>';
                        
                        $current=$current==''?'RefSites':$current;
                        $class=$current=='RefSites'?'class="active"':'';
                        $header.='<ul class="wmsSubList sublisthover"><li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_trafficsrc&subPage=RefSites').'" '.$class.'>Refering Sites</a></li>';
                        
                        $current=$current==''?'SearchEngines':$current;
                        $class=$current=='SearchEngines'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#"'.$class.'>Search Engines</a></li>';
                        
                        $current=$current==''?'SearchKeywords':$current;
                        $class=$current=='SearchKeywords'?'class="active"':'';
                        $header.='<li><a  '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>Search Keywords</a></li></ul>';
                        
                        $header.='</li>';
							
                        $class=$active=='Visitors'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_visitors').'" '.$class.'>'.__('Visitors',P_PREFIX).'</a>';
                        
                        $current=$current==''?'bosl':$current;
                        $class=$current=='bosl'?'class="active"':'';
                        $header.='<ul class="wmsSubList sublisthover"><li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_visitors&subPage=bosl').'" '.$class.'>Browser/OS/Languages</a></li>';
                        
                        $current=$current==''?'GeoLocation':$current;
                        $class=$current=='GeoLocation'?'class="active"':'';
                        $header .='<li><a  '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>GeoLocation</a></li></ul>';
                        $header.='</li>';
                        
                        $class=$active=='Content'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_content').'" '.$class.'>'.__('Content',P_PREFIX).'</a>';
                        
                        $current=$current==''?'byURL':$current;
                        $class=$current=='byURL'?'class="active"':'';
                        $header.='<ul class="wmsSubList sublisthover"><li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_content&subPage=byURL').'" '.$class.'>Traffic By URL</a></li>';
                        $current=$current==''?'byTitle':$current;
                        $class=$current=='byTitle'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>Traffic By Title</a></li></ul>';
                        $header.='</li>';
                        
                        //$class=$active=='I.P. Exclusion'?'class="active"':'';
                        //$header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_ipexc').'" '.$class.'>'.__('I.P. Exclusion',P_PREFIX).'</a></li>';
                        
						$class=$active=='Settings'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_settings').'" '.$class.'>'.__('Settings',P_PREFIX).'</a>';
                        $header.='<ul class="sublist-section wmsSubList sublisthover" data-url="'.admin_url('admin.php?page='.P_PREFIX.'_settings').'"><li><a class="" href="#generalsettings">General settings</a></li><li><a class="" href="#ipexclusion">IP Exclusion</a></li><li><a class="" '.$this->wsm_upgrade_to_pro().' href="#" >Email Reports</a></li><li><a class="" '.$this->wsm_upgrade_to_pro().' href="#" >Admin dashboard</a></li><li><a class="" '.$this->wsm_upgrade_to_pro().' href="#" >Plugin Main Page (statistics dashboard)</a></li><li><a class="" '.$this->wsm_upgrade_to_pro().' href="#" >Short-Codes</a></li></ul>';
                        $header.='</li>';
                        
						
						$header.='<li><a href="http://visitor-statistics.com/product/wordpress-statistics/?attribute_license=Regular" '.$class.' target="_blank" style="background-color:green; color:white">'.__('Upgrade to Pro',P_PREFIX).'</a>';
                        $header.='</li>';
						
                        
                    $header.='</ul>'.$this->wsmClear;
                   
                    if($active=='Traffic'){
                        $header.='<ul class="wmsSubList">';
                        $current=$current==''?'Summary':$current;
                        $class=$current=='Summary'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_traffic').'&subPage=Summary" '.$class.'>'.__('Summary',P_PREFIX).'</a></li>';
                        $class=$current=='UsersOnline'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=summary" '.$class.'>'.__('Users Online',P_PREFIX).'</a></li>';
                        $class=$current=='TrafStats'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#" '.$class.'>'.__('Traffic Stats',P_PREFIX).'</a></li>';
                        $header.='</ul>'.$this->wsmClear;
                        if($current=='UsersOnline'){
                            $wsmAdminJavaScript.='arrLiveStats.push("wsmTopTitle");';
                            $onlineVisitors=$this->objDatabase->fnGetTotalVisitorsCount('Online');
                            $browsingPages=$this->objDatabase->fnGetTotalBrowsingPages();
                            $subTab=isset($wsmRequestArray['subTab']) && $wsmRequestArray['subTab']!=''?$wsmRequestArray['subTab']:'';
                           
                            $header.= '<div class="wsmTopTitle"><span class="wsmOnline">'.__('Users Online',P_PREFIX).'&nbsp;:&nbsp;<b>'.$onlineVisitors.'</b></span><span class="wsmBrowsing">'.__('Browing',P_PREFIX).':&nbsp;<b>'.$browsingPages.'</b>&nbsp;'.__('pages',P_PREFIX).'</span></div>';
                            $subClass=$subTab=='summary'?'class="active"':'';
                            $header.= '<ul class="wmsTabList wsmSubTabList">
                            <li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=summary" '.$subClass.'>'.__('Summary',P_PREFIX).'</a></li>';
                            $subClass=$subTab=='recent'?'class="active"':'';
                            $header.= '<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=recent" '.$subClass.'>'.__('Recent',P_PREFIX).'</a></li>';
                            $subClass=$subTab=='mavis'?'class="active"':'';
                            $header.= '<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=mavis" '.$subClass.'>'.__('Most Active Visitors',P_PREFIX).'</a></li>';
                            $subClass=$subTab=='popPages'?'class="active"':'';
                            $header.= '<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=popPages" '.$subClass.'>'.__('Popular Pages',P_PREFIX).'</a></li>';
                            $subClass=$subTab=='popReferrer'?'class="active"':'';
                            $header.= '<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=popReferrer" '.$subClass.'>'.__('Popular Referrers',P_PREFIX).'</a></li>';
                            $subClass=$subTab=='geoLocation'?'class="active"':'';
                            $header.= '<li><a  '.$this->wsm_upgrade_to_pro().' href="#" '.$subClass.'>'.__('Geo Location',P_PREFIX).'</a></li>';
                            $header.= '</ul>'.$this->wsmClear;
                        }
                    }
                    if($active=='Traffic Sources'){
                        $header.='<ul class="wmsSubList">';
                        $current=$current==''?'RefSites':$current;
                        $class=$current=='RefSites'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_trafficsrc').'&subPage=RefSites" '.$class.'>'.__('Refering Sites',P_PREFIX).'</a></li>';
                        $class=$current=='SearchEngines'?'class="active"':'';
                        $header.='<li><a  '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>'.__('Search Engines',P_PREFIX).'</a></li>';
                        $class=$current=='SearchKeywords'?'class="active"':'';
                        $header.='<li><a  '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>'.__('Search Keywords',P_PREFIX).'</a></li>';
                        $header.='</ul>';
                    }
                    if($active=='Visitors'){
                        $header.='<ul class="wmsSubList">';
                        $current=$current==''?'bosl':$current;
                        $class=$current=='bosl'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_visitors').'&subPage=bosl" '.$class.'>'.__('Browser/OS/Languages',P_PREFIX).'</a></li>';
                        $class=$current=='GeoLocation'?'class="active"':'';
                        $header.='<li><a  '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>'.__('GeoLocation',P_PREFIX).'</a></li>';
                        $header.='</ul>';
                    }
                    if($active=='Content'){
                        $header.='<ul class="wmsSubList">';
                        $current=$current==''?'byURL':$current;
                        $class=$current=='byURL'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.P_PREFIX.'_content').'&subPage=byURL" '.$class.'>'.__('Traffic By URL',P_PREFIX).'</a></li>';
                        $class=$current=='byTitle'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>'.__('Traffic By Title',P_PREFIX).'</a></li>';
                        $header.='</ul>';
                    }
                  $header.='</div>'.$this->wsmClear;
        return $header;
                  //$color = get_user_meta(get_current_user_id(), 'admin_color', true);
    }
    function fnShowTodayStats(){
        echo  do_shortcode("[".P_PREFIX."_showDayStats]");
        echo  do_shortcode("[".P_PREFIX."_showCurrentStats]");
        echo  do_shortcode("[".P_PREFIX."_showForeCast]");
        //echo $html;
    }
    
    function fnShowDailyStatBox($post, $arrParam){
        echo do_shortcode("[".P_PREFIX."_showDayStatBox]");       
    }
    function fnStatFilterBox(){
        echo  do_shortcode("[".P_PREFIX."_showStatFilterBox]"); 
    }
    function fnStatFilterBox4Referral(){
        echo  do_shortcode("[".P_PREFIX."_showStatFilterBox hide='Monthly' source='Referral']"); 
    }
    function fnShowTopReferrerSites(){
        echo  do_shortcode("[".P_PREFIX."_showTopReferrerList searchengine='".( isset($_REQUEST['subPage']) && $_REQUEST['subPage'] == 'SearchEngines' ? 1 : '' )."' ]"); 
    }
	function fnStatsSearchKeywords(){
		echo do_shortcode("[".P_PREFIX."_showStatKeywords]");
	}
	function fnShowOsStatBox(){
		echo do_shortcode("[".P_PREFIX."_showVisitorsDetailGraph]");
	}
	function fnShowVisitorsDetails(){
		echo do_shortcode("[".P_PREFIX."_showVisitorsDetail]");
	}
	function fnShowGeoLocationStats(){
		echo do_shortcode("[".P_PREFIX."_showGeoLocationGraph]");
	}
	function fnShowGeoLocationDetails(){
		echo do_shortcode("[".P_PREFIX."_showGeoLocationDetails]");
	}
	
    function fnShowDaysStatsGraph(){
        $arrPostData=wsmSanitizeFilteredPostData();
        if($arrPostData['filterWay']!='Range'){
            echo  do_shortcode("[".P_PREFIX."_showDayStatsGraph]");
        }
        echo  do_shortcode("[".P_PREFIX."_showTrafficStatsList]");
    }
    function fnShowGenStats(){
        echo  do_shortcode("[".P_PREFIX."_showGenStats]");
        //echo $html;
    }
    function fnShowLastDaysStats(){
        echo  do_shortcode("[".P_PREFIX."_showLastDaysStats]");
        echo  do_shortcode("[".P_PREFIX."_showLastDaysStatsChart id='LastDaysChart2']");
        //echo $html;
    }
    function fnShowGeoLocationChart(){
        echo  do_shortcode("[".P_PREFIX."_showGeoLocation]");
    }
    function fnShowRecentVisitedPages(){
        echo  do_shortcode("[".P_PREFIX."_showRecentVisitedPages]");
    }
    function fnShowRecentVisitedPagesDetails(){
        echo  do_shortcode("[".P_PREFIX."_showRecentVisitedPagesDetails]");
    }
    function fnShowPopularPages(){
        echo  do_shortcode("[".P_PREFIX."_showPopularPages]");
    }
    function fnShowPopularReferrers(){
        echo  do_shortcode("[".P_PREFIX."_showPopularReferrers]");
    }
    function fnShowMostActiveVisitors(){
        echo  do_shortcode("[".P_PREFIX."_showMostActiveVisitors]");
    }
    function fnShowMostActiveVisitorsDetails(){
        echo  do_shortcode("[".P_PREFIX."_showMostActiveVisitorsDetails]");
    }
    function fnShowMostActiveVisitorsGeo(){
        echo  do_shortcode("[".P_PREFIX."_showMostActiveVisitorsGeo]");
    }
    function fnShowMostActiveVisitorsGeoDetails(){
        echo  do_shortcode("[".P_PREFIX."_showMostActiveVisitorsGeo height='450px' zoom='2']");       
    }
    function fnShowActiveVistiorsCountByCountry(){
         echo  do_shortcode("[".P_PREFIX."_showActiveVisitorsByCountry]");
    }
    function fnShowActiveVistiorsCountByCity(){
         echo  do_shortcode("[".P_PREFIX."_showActiveVisitorsByCity]");
    }
    function fnShowReffererStatBox(){
         echo  do_shortcode("[".P_PREFIX."_showRefferStatsBox searchengine='".( isset($_REQUEST['subPage']) && $_REQUEST['subPage'] == 'SearchEngines' ? 1 : '' )."' ]");
    }
    function fnShowReffererSearchEngineStatBox(){
         echo  do_shortcode("[".P_PREFIX."_showRefferStatsBox searchengine='1']");
    }
	function fnShowSearchEngineSummary(){
		echo do_shortcode('['.P_PREFIX.'_showSearchEngineSummary]');
	}
	function fnShowContentByURL(){
		echo do_shortcode("[".P_PREFIX."_showContentByURL]");
	}
	function fnShowContentURLStats(){
		echo do_shortcode("[".P_PREFIX."_showContentByURLStats]");
	}
	function fnIPExclusion(){
		echo do_shortcode("[".P_PREFIX."_showIPExclustion]");
	}
	function fnShowTitleCloud(){
		echo do_shortcode("[".P_PREFIX."_showTitleCloud]");
	}
    function wsmSavePluginSettings($arrPostData){
        $tzstring = get_option('wsmTimezoneString');
        if(isset($_POST[P_PREFIX.'TimezoneString']) && $_POST[P_PREFIX.'TimezoneString']!=''){
            //if($tzstring!==$_POST[P_PREFIX.'TimezoneString']){
                update_option(P_PREFIX.'TimezoneString',$_POST[P_PREFIX.'TimezoneString']);
                wsmInitPlugin::wsm_fnCreateImportantViews();
                wsmInitPlugin::wsm_createMonthWiseViews();
				//}
        }
        if(isset($_POST[P_PREFIX.'ChartDays']) && $_POST[P_PREFIX.'ChartDays']!=''){
            update_option(P_PREFIX.'ChartDays',$_POST[P_PREFIX.'ChartDays']);
        }
        if(isset($_POST[P_PREFIX.'Country']) && $_POST[P_PREFIX.'Country']!=''){
            update_option(P_PREFIX.'Country',$_POST[P_PREFIX.'Country']);
        }
        if(isset($_POST[P_PREFIX.'GoogleMapAPI']) && $_POST[P_PREFIX.'GoogleMapAPI']!=''){
            update_option(P_PREFIX.'GoogleMapAPI',$_POST[P_PREFIX.'GoogleMapAPI']);
        }
        if(isset($_POST[P_PREFIX.'ArchiveDays']) && $_POST[P_PREFIX.'ArchiveDays']!=''){
            update_option(P_PREFIX.'ArchiveDays',$_POST[P_PREFIX.'ArchiveDays']);
        }
        if(isset($_POST[P_PREFIX.'KeepData']) && $_POST[P_PREFIX.'KeepData']!=''){
            update_option(P_PREFIX.'KeepData',"1");
        }
        else
			update_option(P_PREFIX.'KeepData',"0");
			 
		update_option(P_PREFIX.'ReportScheduleTime',$_POST[P_PREFIX.'ReportScheduleTime']);
		update_option(P_PREFIX.'ReportStats',$_POST[P_PREFIX.'ReportStats']);
		update_option(P_PREFIX.'ReportEmails',$_POST[P_PREFIX.'ReportEmails']);
		update_option(P_PREFIX.'SiteDashboardNormalWidgets',$_POST[P_PREFIX.'SiteDashboardNormalWidgets']);
		update_option(P_PREFIX.'SiteDashboardSideWidgets',$_POST[P_PREFIX.'SiteDashboardSideWidgets']);
		update_option(P_PREFIX.'Dashboard_widget',$_POST[P_PREFIX.'Dashboard_widget']);
		update_option(P_PREFIX.'SitePluginNormalWidgets',$_POST[P_PREFIX.'SitePluginNormalWidgets']);
		update_option(P_PREFIX.'SitePluginSideWidgets',$_POST[P_PREFIX.'SitePluginSideWidgets']);
		update_option(P_PREFIX.'Plugin_widget',$_POST[P_PREFIX.'Plugin_widget']);
    }
    function wsmViewSettings(){
        global $wsmAdminJavaScript,$wsmAdminPageHooks;
        if(isset($_POST[P_PREFIX.'_form']) && $_POST[P_PREFIX.'_form']==P_PREFIX.'_frmSettings'){
            $this->wsmSavePluginSettings($_POST);
        }
        $html=$this->startWrapper;
        $html.=$this->fnPrintTitle('Settings');
        $current_offset = get_option('gmt_offset');
        $tzstring = get_option(P_PREFIX.'TimezoneString');
        $chartDays = get_option(P_PREFIX.'ChartDays');
        $country = get_option(P_PREFIX.'Country');
        $googleMapAPI = get_option(P_PREFIX.'GoogleMapAPI');
        $ArchiveDays = get_option(P_PREFIX.'ArchiveDays');
        $KeepData = get_option(P_PREFIX.'KeepData');
        $KeepDataChk="";
        if($KeepData=="1")
			$KeepDataChk = "checked='checked'";
		
        $reportScheduleTime = get_option(P_PREFIX.'ReportScheduleTime');
        $reportStats = get_option(P_PREFIX.'ReportStats');
        $reportEmails = get_option(P_PREFIX.'ReportEmails');
		$siteDashboardNormalWidgets = get_option(P_PREFIX.'SiteDashboardNormalWidgets');
		$siteDashboardSideWidgets = get_option(P_PREFIX.'SiteDashboardSideWidgets');
		$dashboard_widget = get_option(P_PREFIX.'Dashboard_widget');
		$sitePluginNormalWidgets = get_option(P_PREFIX.'SitePluginNormalWidgets');
		$sitePluginSideWidgets = get_option(P_PREFIX.'SitePluginSideWidgets');
		$plugin_widget = get_option(P_PREFIX.'Plugin_widget');
		
        $check_zone_info = true;
        // Remove old Etc mappings. Fallback to gmt_offset.
        if ( false !== strpos($tzstring,'Etc/GMT') )
            $tzstring = '';
        if ( empty($tzstring) ) { // Create a UTC+- zone if no timezone string exists
            $check_zone_info = false;
            if ( 0 == $current_offset )
                $tzstring = 'UTC+0';
            elseif ($current_offset < 0)
                $tzstring = 'UTC' . $current_offset;
            else
                $tzstring = 'UTC+' . $current_offset;
        }
		$mailTiming = '<select id="'.P_PREFIX.'ReportScheduleTime" name="'.P_PREFIX.'ReportScheduleTime"><option value=""></option>';
		$scheduleArray = array( 1 => 'Every Day', 3 => 'Every 3 Days', 7 => 'Every Week', 30 => 'Every Month' );
		foreach( $scheduleArray as $key => $value ){
			$mailTiming .= '<option '.($reportScheduleTime == $key ? 'selected' : '').' value="'.$key.'">'.$value.'</option>';
		}
		$mailTiming .= '</select>';
		
		$report_stats_list = array('general_stats_new' => 'General Stats',
									'daily_stats'	=>	'Daily Stats',
									'referral_website_stats'	=>	'Referral Website Stats',
									'search_engine_stats'	=>	'Top Search Engine Stats',
									'traffic_by_title_stats' => 'Title Stats',
									'top_search_engine_stats' => 'Top Search Engine Stats',
									'os_wise_visitor_stats'	=>	'OS base Visitor Stats',
									'browser_wise_visitor_stats'	=>	'Browser base Visitor Stats',
									'screen_wise_visitor_stats'	=>	'Screen base Visitor Stats',
									'country_wise_visitor_stats'	=>	'Today Countries Stats',
									'city_wise_visitor_stats'	=>	'Today Cities Stats',
									/*'recent_visit_pages' => 'Traffic By Title',*/
									'recent_active_visitors' => 'Users Online'
							);
		
		//$reportStatsHTML = '<select id="'.P_PREFIX.'ReportStats" multiple name="'.P_PREFIX.'ReportStats[]">';
		$reportStatsHTML = '<table class="report_list_table">';
		$reportWidget = $dashboardNormalWidget = $dashboardSideWidget = $pluginNormalWidget = $pluginSideWidget = '';
		
		$dashboardNormalWidgetList = $siteDashboardNormalWidgets ? explode(',', $siteDashboardNormalWidgets) : array();
		$dashboardSideWidgetList = $siteDashboardSideWidgets ? explode(',', $siteDashboardSideWidgets) : array();
		
		$pluginNormalWidgetList = $sitePluginNormalWidgets ? explode(',', $sitePluginNormalWidgets) : array();
		$pluginSideWidgetList = $sitePluginSideWidgets ? explode(',', $sitePluginSideWidgets) : array();
		
		foreach( $report_stats_list as $key => $value ){
			//$reportStatsHTML .= '<option '.( (is_array($reportStats) && in_array( $key, $reportStats)) ? 'selected' : '' ).' value="'.$key.'">'.$value.'</option>';
			$status =(is_array($reportStats) && in_array( $key, $reportStats)) ? 1 : 0;
			if( !in_array($key, array( 'recent_visit_pages', 'traffic_by_title_stats', 'recent_active_visitors' ) ) ){
				$reportStatsHTML .= sprintf( '<tr><td>%s</td><td><label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></td></tr>', $value, P_PREFIX.'ReportStats[]', ($status?'checked':''), $key );
			}
			$reportWidget .= sprintf( '<div data-id="%s">%s</div>', $key, $value );
			$dashboardNormalWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $value, P_PREFIX.'Dashboard_widget[normal][]', ($status?'checked':''), $key );
			$pluginNormalWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $value, P_PREFIX.'Plugin_widget[normal][]', ($status?'checked':''), $key );
		}
		if( count( $dashboardNormalWidgetList ) ){
			$dashboardNormalWidget = '';
			foreach( $dashboardNormalWidgetList as $widget ){
				$status = 0;
				$widget = trim($widget);
				if( is_array( $dashboard_widget ) && isset( $dashboard_widget['normal'] ) && in_array( $widget, $dashboard_widget['normal'] ) ){
					$status = 1;
				}
				$dashboardNormalWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $report_stats_list[$widget], P_PREFIX.'Dashboard_widget[normal][]', ($status?'checked':''), $widget );
			}
		}
		if( count( $dashboardSideWidgetList ) ){
			foreach( $dashboardSideWidgetList as $widget ){
				$status = 0;
				$widget = trim($widget);
				if( is_array( $dashboard_widget ) && isset( $dashboard_widget['side'] ) && in_array( $widget, $dashboard_widget['side'] ) ){
					$status = 1;
				}
				$dashboardSideWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $report_stats_list[$widget], P_PREFIX.'Dashboard_widget[side][]', ($status?'checked':''), $widget );
			}
		}
		if( count( $pluginNormalWidgetList ) ){
			$pluginNormalWidget = '';
			foreach( $pluginNormalWidgetList as $widget ){
				$status = 0;
				$widget = trim($widget);
				if( is_array( $plugin_widget ) && isset( $plugin_widget['normal'] ) && in_array( $widget, $plugin_widget['normal'] ) ){
					$status = 1;
				}
				$pluginNormalWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $report_stats_list[$widget], P_PREFIX.'Plugin_widget[normal][]', ($status?'checked':''), $widget );
			}
		}
		if( count( $pluginSideWidgetList ) ){
			foreach( $pluginSideWidgetList as $widget ){
				$status = 0;
				$widget = trim($widget);
				if( is_array( $plugin_widget ) && isset( $plugin_widget['side'] ) && in_array( $widget, $plugin_widget['side'] ) ){
					$status = 1;
				}
				$pluginSideWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $report_stats_list[$widget], P_PREFIX.'Plugin_widget[side][]', ($status?'checked':''), $widget );
			}
		}
		//$reportStatsHTML .= '</select>';
		$reportStatsHTML .= '</table>';
		$wsmStatistics=new wsmStatistics;
		//print_r($_POST);
		//echo 'active'.$_POST['tab-li-active'];
        $html.='<form name="'.P_PREFIX.'_frmSettings" method="post">';
        $html.='<input type="hidden" name="'.P_PREFIX.'_form" value="'.P_PREFIX.'_frmSettings">';
        $generalsettings="active";
        $ipexclusion=$sitedashboard=$report=$summarywidget=$shortcodelist='';
		$ipexclusion1=$generalsettings1=$sitedashboard1=$report1=$summarywidget1=$shortcodelist1='';
        if($_POST['tab-li-active'] && $_POST['tab-li-active']!=='')
        {
			if($_POST['tab-li-active']=="#generalsettings")
			{
					$generalsettings="active";
					$ipexclusion=$sitedashboard=$report=$summarywidget=$shortcodelist='';
					
					$generalsettings1='style="display: table;"';
					$ipexclusion1=$sitedashboard1=$report1=$summarywidget1=$shortcodelist1='style="display: none;"';
			}
			else if($_POST['tab-li-active']=="#ipexclusion")
			{
					$ipexclusion="active";
					$generalsettings=$report=$summarywidget=$shortcodelist='';
					
					$ipexclusion1='style="display: table;"';
					$generalsettings1=$report1=$summarywidget1=$shortcodelist1='style="display: none;"';
			}			
			else if($_POST['tab-li-active']=="#sitedashboard")
			{
					$sitedashboard="active";
					$ipexclusion=$generalsettings=$report=$summarywidget=$shortcodelist='';
					
					$sitedashboard1='style="display: table;"';
					$ipexclusion1=$generalsettings1=$report1=$summarywidget1=$shortcodelist1='style="display: none;"';
			}
			else if($_POST['tab-li-active']=="#report")
			{
					$report="active";
					$ipexclusion=$generalsettings=$sitedashboard=$summarywidget=$shortcodelist='';
					
					$report1='style="display: table;"';
					$ipexclusion1=$generalsettings1=$sitedashboard1=$summarywidget1=$shortcodelist1='style="display: none;"';
			}
			else if($_POST['tab-li-active']=="#summarywidget")
			{
					$summarywidget="active";
					$ipexclusion=$generalsettings=$sitedashboard=$report=$shortcodelist='';
					
					$summarywidget1='style="display: table;"';
					$ipexclusion1=$generalsettings1=$sitedashboard1=$report1=$shortcodelist1='style="display: none;"';
			}
			else if($_POST['tab-li-active']=="#shortcodelist")
			{
					$shortcodelist="active";
					$ipexclusion=$generalsettings=$sitedashboard=$report=$summarywidget='';
					
					$shortcodelist1='style="display: table;"';
					$ipexclusion1=$generalsettings1=$sitedashboard1=$report1=$summarywidget1='style="display: none;"';
			}
			
		}
		ob_start(); 
		include WSM_DIR."includes/wsm_shortcodeTable.php"; 
		$shortCodeData=ob_get_contents(); 
		ob_clean();
        $html.='<ul class="li-section">
					<li><a class="'.$generalsettings.'" href="#generalsettings">General settings</a></li>
					<li><a class="'.$ipexclusion.'" href="#ipexclusion">IP Exclusion</a></li>
					<li><a class="'.$report.'" '.$this->wsm_upgrade_to_pro().' href="#" >Email Reports</a></li>
					<li><a class="'.$sitedashboard.'" '.$this->wsm_upgrade_to_pro().' href="#" >Admin dashboard</a></li>
					<li><a class="'.$summarywidget.'" '.$this->wsm_upgrade_to_pro().' href="#" >Plugin Main Page (statistics dashboard)</a></li>
					<li><a class="'.$shortcodelist.'" '.$this->wsm_upgrade_to_pro().' href="#" >Short-Codes</a></li>
				</ul>';
        $html.='<div class="li-section-table"><table class="form-table" id="generalsettings" '.$generalsettings1.'><tbody>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'TimezoneString">'.__('Timezone',P_PREFIX).'</label></th>
                    <td>'.$this->wsmGetCountryDropDown($country).$this->wsmGetTimeZoneDropDown($tzstring).'
                    <p class="description" id="timezone-description">'.__( 'Choose either a city in the same timezone as you or a UTC timezone offset.',P_PREFIX ).'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'ChartDays">'.__('Chart Days',P_PREFIX).'</label></th>
                    <td>'.$this->wsmGetChartDaysDropDown($chartDays).'
                    <p class="description">'.__( 'Choose number of days to show statistics on the summary page.',P_PREFIX ).'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'GoogleMapAPI">'.__('Google Map API',P_PREFIX).'</label></th>
                    <td><input type="text" id="'.P_PREFIX.'GoogleMapAPI" name="'.P_PREFIX.'GoogleMapAPI" value="'.$googleMapAPI.'"/>
                    <p class="description">'.__( 'Enter Google Map API key. You can find how to get the key from this url - <a href="https://developers.google.com/maps/documentation/javascript/get-api-key">https://developers.google.com/maps/documentation/javascript/get-api-key</a>. This key is used to verify from google map.',P_PREFIX ).'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'ArchiveDays">'.__('Archive Data',P_PREFIX).'</label></th>
                    <td>'.$this->wsmGetArchiteDaysDropDown($ArchiveDays).'
						<p class="description">'.__('You can set archive data setting for 30 days or 60 days.').'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'KeepData1">'.__('Keep Data',P_PREFIX).'</label></th>
                    <td>
						<input type="checkbox" name="'.P_PREFIX.'KeepData" id="'.P_PREFIX.'KeepData" value="1" '.$KeepDataChk.'/> <label for="'.P_PREFIX.'KeepData">Keep data after unistalling the plugin?</label>
						<p class="description">'.__('Check to keep data and uncheck to remove data.').'</p>
                    </td>
                </tr>
                </tbody></table>
                <table class="form-table" id="report" '.$report1.'><tbody>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'ReportScheduleTime">'.__('Scheduled Report Time',P_PREFIX).'</label></th>
                    <td>'.$mailTiming.'
                    <p class="description">'.__( 'Select time for receiving report mail.',P_PREFIX ).'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'ReportStats">'.__('Reports',P_PREFIX).'</label></th>
                    <td>'.$reportStatsHTML.'
                    <p class="description">'.__( 'Select stats type which you want to receive as report in mail.',P_PREFIX ).'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'ReportEmails">'.__('Report Notification',P_PREFIX).'</label></th>
                    <td><textarea cols="50" id="'.P_PREFIX.'ReportEmails" name="'.P_PREFIX.'ReportEmails">'.$reportEmails.'</textarea>&nbsp;&nbsp;<a href="#" class="button button-primary send_test_mail">Send Test Mail</a>
                    <p class="description">'.__( 'Add more than one email by comma seperator.',P_PREFIX ).'</p>
                    </td>
                </tr></tbody></table>
                <table class="form-table" id="sitedashboard" '.$sitedashboard1.'><tbody>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'SiteDashboardWidgets">'.__('Site Dashboard Widgets',P_PREFIX).'</label></th>
                    <td>
					<div class="site_dashboard_widget_panel"></div>
					<table><tr><th>Normal</th><th>Side</th></tr><tr><td><ul class="site_dashboard_widget_handler" id="site_dashboard_widget_handler_1">'.$dashboardNormalWidget.'</ul></td><td><ul id="site_dashboard_widget_handler_2" class="site_dashboard_widget_handler">'.$dashboardSideWidget.'</ul></td></tr></table>
                    <p class="description">'.__( 'You can drag and drop widget here.',P_PREFIX ).'</p>
					<input type="hidden" name="'.P_PREFIX.'SiteDashboardNormalWidgets"  value="" />
					<input type="hidden" name="'.P_PREFIX.'SiteDashboardSideWidgets" value="" />
                    </td>
                </tr></tbody></table>
                <table class="form-table" id="summarywidget" '.$summarywidget1.'><tbody>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'SitePluginWidgets">'.__('Plugin Summary Widgets',P_PREFIX).'</label></th>
                    <td>
					<div class="site_dashboard_widget_panel"></div>
					<table><tr><th>Normal</th><th>Side</th></tr><tr><td><ul class="site_plugin_widget_handler" id="site_plugin_widget_handler_1">'.$pluginNormalWidget.'</ul></td><td><ul id="site_plugin_widget_handler_2" class="site_plugin_widget_handler">'.$pluginSideWidget.'</ul></td></tr></table>
                    <p class="description">'.__( 'You can drag and drop widget here.',P_PREFIX ).'</p>
					<input type="hidden" name="'.P_PREFIX.'SitePluginNormalWidgets"  value="" />
					<input type="hidden" name="'.P_PREFIX.'SitePluginSideWidgets" value="" />
                    </td>
                </tr>
                </tbody></table>
                 <table class="form-table myshortcodelist" id="shortcodelist" '.$shortcodelist1.'><tbody>
                <tr>
                    <th scope="row"><label for="'.P_PREFIX.'">'.__('Shortcodes',P_PREFIX).'</label></th>
                </tr>
                <tr>
					<td>
					<div class="shortcode_panel">';
						$html.=$shortCodeData;
			$html.='</div>
                    <p class="description">'.__( 'Shortcode lists are going to display here.',P_PREFIX ).'</p>
					</td>
                </tr>
                
                </tbody></table>
                </div>
                
                <p class="submit"><input type="hidden" name="tab-li-active" id="tab-li-active" value=""><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>';
        $html.='</form>';
        
        $html .= '
                <table class="form-table" style="display:none" id="ipexclusion" '.$ipexclusion1.'><tbody>
                <tr>
					
                    <td>
						<div id="dashboard-widgets-wrap" class="wsmMetaboxContainer">
							<div id="dashboard-widgets" class="metabox-holder">
								<form name="wsmmainMetboxForm" id="wsmmainMetboxForm" method="post">
									<input id="_wpnonce" name="_wpnonce" value="100250cbd1" type="hidden"><input type="hidden" name="_wp_http_referer" value="/wp-admin/admin.php?page=wsm_ipexc" />
									<div id="wsm-postbox-container-1" class="postbox-containe">
										<div id="bottom-sortables" class="meta-box-sortables ui-sortable">
											<div id="wsm_ipexc" class="postbox">
													<h2 class="hndle ui-sortable-handle test"><span>I.P. Exclution</span></h2>					 
													<div class="inside">'.$wsmStatistics->wsm_showIPExclustion('').'</div>
											</div>
										</div>
									</div>
									<input type="hidden" id="meta-box-order-nonce" name="meta-box-order-nonce" value="9c03ae7329" /><input type="hidden" id="closedpostboxesnonce" name="closedpostboxesnonce" value="4f4cab2048" />
								</form>
							</div>
						</div>
					</td>
                </tr>
               </tbody></table>';
        $wsmAdminJavaScript.='
            jQuery("#'.P_PREFIX.'Country").on(\'change\', function() {
              jQuery.ajax({
                   type: "POST",
                   url: wsm_ajaxObject.ajax_url,
                   data: { action: \'timezoneByCountry\', r: Math.random(),code:this.value }
               }).done(function( timezone ) {
                    jQuery("#'.P_PREFIX.'TimezoneString").val(timezone);
                   //jQuery("#'.P_PREFIX.'Country option[value=\'"+timezone+"\']").attr("selected", "selected");
               });
            })
            ';
        echo $html.=$this->endWrapper;
    }
    function wsmGetTimeZoneDropDown($tzstring){
        $html='<select id="'.P_PREFIX.'TimezoneString" name="'.P_PREFIX.'TimezoneString" aria-describedby="timezone-description">'.     wp_timezone_choice( $tzstring, get_user_locale() ).'</select>';
        return $html;
    }
    function wsmGetCountryDropDown($code=''){
        $arrCountries=$this->objDatabase->fnGetAllCountries();
        $html='<select id="'.P_PREFIX.'Country" name="'.P_PREFIX.'Country" >';
        foreach($arrCountries as $country){
            $selected="";
            if($country['alpha2Code']==$code){
                $selected='selected="selected"';
            }
            $html.='<option value="'.$country['alpha2Code'].'" '.$selected.'>'.__($country['name'],P_PREFIX).'</option>';
        }
        return $html.='</select>';
    }
    function wsmGetArchiteDaysDropDown($days=30)
	{
		$html='<select id="'.P_PREFIX.'ArchiveDays" name="'.P_PREFIX.'ArchiveDays" >';
		if($days==30){
            $html.='<option value="30" selected="selected">'.__('Last 30 Days',P_PREFIX).'</option>';
        }else{
            $html.='<option value="30">'.__('Last 30 Days',P_PREFIX).'</option>';
        }
        if($days==60){
            $html.='<option value="60" selected="selected">'.__('Last 60 Days',P_PREFIX).'</option>';
        }else{
            $html.='<option value="60">'.__('Last 60 Days',P_PREFIX).'</option>';
        }
		$html.='</select>';
		return $html;
	}
    
    function wsmGetChartDaysDropDown($days=30){
        $html='<select id="'.P_PREFIX.'ChartDays" name="'.P_PREFIX.'ChartDays" >';
        if($days==15){
            $html.='<option value="15" selected="selected">'.__('Last 15 Days',P_PREFIX).'</option>';
        }else{
            $html.='<option value="15">'.__('Last 15 Days',P_PREFIX).'</option>';
        }
        if($days==30 || $days=='' ){
            $html.='<option value="30" selected="selected">'.__('Last 30 Days',P_PREFIX).'</option>';
        }else{
            $html.='<option value="30">'.__('Last 30 Days',P_PREFIX).'</option>';
        }
        if($days==45){
            $html.='<option value="45" selected="selected">'.__('Last 45 Days',P_PREFIX).'</option>';
        }else{
            $html.='<option value="45">'.__('Last 45 Days',P_PREFIX).'</option>';
        }
        if($days==60){
            $html.='<option value="60" selected="selected">'.__('Last 60 Days',P_PREFIX).'</option>';
        }else{
            $html.='<option value="60">'.__('Last 60 Days',P_PREFIX).'</option>';
        }
        return $html.='</select>';
    }
    function wsmCreateSubLayout($layout){
        global $wsmAdminPageHooks,$wsmRequestArray,$wp_meta_boxes;        
        switch($layout){
            case 'Summary':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-4" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'bottom', null );
                echo '</div>';
            break;
            case 'UsersOnline':
                $tab=isset($wsmRequestArray['subTab'])&&$wsmRequestArray['subTab']!=""?$wsmRequestArray['subTab']:'';
                if($tab!=''){
                    switch($tab){
                        case 'summary':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                            echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                            do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'left', null );
                            echo '</div>';
                            echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                            do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'right', null );
                            echo '</div>';
                        break;
                        case 'recent':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                        break;
                        case 'mavis':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                        break;
                        case 'popPages':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                        break;
                        case 'popReferrer':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                        break;
                        case 'geoLocation':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                            echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                            do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'left', null );
                            echo '</div>';
                            echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                            do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'right', null );
                            echo '</div>';
                        break;
                    }
                }
            break;
            case 'TrafStats':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_traffic'], 'bottom', null );
                echo '</div>';
            break;
           case 'RefSites':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_trafficsrc'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_trafficsrc'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_trafficsrc'], 'bottom', null );
                echo '</div>';  
				break;          
           case 'SearchEngines':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_trafficsrc'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_trafficsrc'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_trafficsrc'], 'bottom', null );
                echo '</div>';   
				break;          
           case 'SearchKeywords':
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_trafficsrc'], 'bottom', null );
                echo '</div>';   
				break;
           case 'bosl':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_bosl'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_bosl'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_bosl'], 'bottom', null );
                echo '</div>';            
 			    break;
           case 'GeoLocation':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_visitors'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_visitors'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_visitors'], 'bottom', null );
                echo '</div>';            
 			    break; 
           case 'byURL':
           case 'byTitle':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_content'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_content'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_content'], 'bottom', null );
                echo '</div>';            
 			    break; 
           case 'ipexc':
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                do_meta_boxes( $wsmAdminPageHooks[P_PREFIX.'_ipexc'], 'bottom', null );
                echo '</div>';   
				break;
        }
    }
    function wsmShowMainPageLayout($page){
        global $wsmAdminPageHooks,$wsmAdminJavaScript,$wsmRequestArray;
        echo $this->startMetaBoxWrapper;
        echo '<form name="'.P_PREFIX.'mainMetboxForm" id="'.P_PREFIX.'mainMetboxForm" method="post">';
        wp_nonce_field( 'some-action-nonce' );        
        $subPage=isset($wsmRequestArray['subPage']) && $wsmRequestArray['subPage']!=''?$wsmRequestArray['subPage']:'bosl';
        if($subPage!=''){
            $this->wsmCreateSubLayout($subPage);
        }
        /* Used to save closed meta boxes and their order */
        wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false );
        wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false );
        echo '</form>';
        echo $this->endMetaBoxWrapper;        
        $wsmAdminJavaScript.='if(jQuery(".'.$wsmAdminPageHooks[$page].'").length){
                    postboxes.add_postbox_toggles("'.$wsmAdminPageHooks[$page].'");
                }';
    }
    function wsmViewTraffic(){
        global $wsmAdminPageHooks,$wsmAdminJavaScript,$wsmRequestArray;
        echo $this->startWrapper;
        echo $this->fnPrintTitle('Traffic');        
        $this->wsmShowMainPageLayout(P_PREFIX.'_traffic');
     /*   echo $html.=$this->startMetaBoxWrapper;
        echo '<form name="'.P_PREFIX.'mainMetboxForm" id="'.P_PREFIX.'mainMetboxForm" method="post">';
        wp_nonce_field( 'some-action-nonce' );        
        $subPage=isset($wsmRequestArray['subPage']) && $wsmRequestArray['subPage']!=''?$wsmRequestArray['subPage']:'';
        if($subPage!=''){
            $this->wsmCreateLayout($subPage);
        }
        /* Used to save closed meta boxes and their order 
        wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false );
        wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false );

        echo '</form>';
        echo $this->endMetaBoxWrapper;
        echo $this->endWrapper;
        $wsmAdminJavaScript.='if(jQuery(".'.$wsmAdminPageHooks[P_PREFIX.'_traffic'].'").length){
                    postboxes.add_postbox_toggles("'.$wsmAdminPageHooks[P_PREFIX.'_traffic'].'");
                }';*/
        echo $this->endWrapper;
    }

    function wsmViewTrafficSources(){
        global $wsmAdminPageHooks,$wsmAdminJavaScript,$wsmRequestArray;
        echo $this->startWrapper;
        echo $this->fnPrintTitle('Traffic Sources');
        $this->wsmShowMainPageLayout(P_PREFIX.'_trafficsrc');
        echo $this->endWrapper;
    }
    function wsmViewVisitors(){
        echo $this->startWrapper;
        echo $this->fnPrintTitle('Visitors');
        $this->wsmShowMainPageLayout(P_PREFIX.'_visitors');
        echo $this->endWrapper;
    }
    function wsmViewContent(){
        echo $this->startWrapper;
        echo $this->fnPrintTitle('Content');
        $this->wsmShowMainPageLayout(P_PREFIX.'_content');
        echo $this->endWrapper;
    }
    function wsmViewIPExclusion(){
        echo $this->startWrapper;
        echo $this->fnPrintTitle('I.P. Exclusion');
        $this->wsmShowMainPageLayout(P_PREFIX.'_ipexc');
        echo $this->endWrapper;
    }
}

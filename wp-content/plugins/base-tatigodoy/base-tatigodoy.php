<?php

/**
 * Plugin Name: Base Tatigodoy
 * Description: Controle base do tema Tatigodoy.
 * Version: 0.1
 * Author: Agência Pixele
 * Author URI: http://www.pixele.com.br
 * Licence: GPL2
 */

	function baseTatigodoy () {

		// TIPOS DE CONTEÚDO
		conteudosTatigodoy();

		// TAXONOMIA
		taxonomiaTatigodoy();

		// META BOXES
		metaboxesTatigodoy();

		// SHORTCODES
		shortcodesTatigodoy();

	    // ATALHOS VISUAL COMPOSER
	    //visualcomposerTatigodoy();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosTatigodoy (){

		// TIPOS DE CONTEÚDO
		//tipoEbooks();
		//tipoDownloads();
		tipoPOs();
		tipoVideos();
		tipoDepoimentos();

		// ALTERAÇÃO DO TÍTULO PADRÃO
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );

		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'pos':
					$titulo = 'Nome da personal organizer';
				break;

				default:
				break;
			}

		    return $titulo;

		}

	}

		function tipoEbooks() {

			$rotulosEbooks = array(
									'name'               => 'Ebooks',
									'singular_name'      => 'Ebook',
									'menu_name'          => 'Ebooks',
									'name_admin_bar'     => 'Ebooks',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo Ebook',
									'new_item'           => 'Novo Ebook',
									'edit_item'          => 'Editar Ebook',
									'view_item'          => 'Ver Ebook',
									'all_items'          => 'Todas os Ebooks',
									'search_items'       => 'Buscar Ebooks',
									'parent_item_colon'  => 'Dos Ebooks',
									'not_found'          => 'Nenhum Ebook cadastrado.',
									'not_found_in_trash' => 'Nenhum Ebook na lixeira.'
								);

			$argsEbooks 	= array(
									'labels'             => $rotulosEbooks,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-book-alt',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'ebooks' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('ebooks', $argsEbooks);

		}

		function tipoDownloads() {

			$rotulosDownloads 	= array(
									'name'               => 'Downloads',
									'singular_name'      => 'Download',
									'menu_name'          => 'Downloads',
									'name_admin_bar'     => 'Downloads',
									'add_new'            => 'Adicionar download',
									'add_new_item'       => 'Adicionar novo download',
									'new_item'           => 'Novo download',
									'edit_item'          => 'Editar download',
									'view_item'          => 'Ver download',
									'all_items'          => 'Todos downloads',
									'search_items'       => 'Buscar downloads',
									'parent_item_colon'  => 'Dos downloads',
									'not_found'          => 'Nenhum download cadatrado.',
									'not_found_in_trash' => 'Nenhum download na lixeira.'
								);

			$argsDownloads 		= array(
									'labels'             => $rotulosDownloads,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-media-text',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'downloads' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('downloads', $argsDownloads);

		}

		function tipoPOs() {

			$rotulosPOs = array(
									'name'               => 'POs',
									'singular_name'      => 'PO',
									'menu_name'          => 'POs',
									'name_admin_bar'     => 'POs',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova PO',
									'new_item'           => 'Nova PO',
									'edit_item'          => 'Editar PO',
									'view_item'          => 'Ver PO',
									'all_items'          => 'Todas as POs',
									'search_items'       => 'Buscar POs',
									'parent_item_colon'  => 'Das POs',
									'not_found'          => 'Nenhum PO cadastrada.',
									'not_found_in_trash' => 'Nenhuma PO na lixeira.'
								);

			$argsPOs 	= array(
									'labels'             => $rotulosPOs,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-groups',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'pos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('pos', $argsPOs);

		}

		function tipoVideos() {

			$rotulosVideos 	= array(
									'name'               => 'Vídeos',
									'singular_name'      => 'Vídeo',
									'menu_name'          => 'Vídeos',
									'name_admin_bar'     => 'Vídeos',
									'add_new'            => 'Adicionar vídeo',
									'add_new_item'       => 'Adicionar novo vídeo',
									'new_item'           => 'Novo vídeo',
									'edit_item'          => 'Editar vídeo',
									'view_item'          => 'Ver vídeo',
									'all_items'          => 'Todos vídeos',
									'search_items'       => 'Buscar vídeos',
									'parent_item_colon'  => 'Dos vídeos',
									'not_found'          => 'Nenhum vídeo cadatrado.',
									'not_found_in_trash' => 'Nenhum vídeo na lixeira.'
								);

			$argsVideos 		= array(
									'labels'             => $rotulosVideos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-video-alt3',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'videos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'editor', 'thumbnail', )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('videos', $argsVideos);

		}

		function tipoDepoimentos() {

			$rotulosDepoimentos 	= array(
										'name'               => 'Depoimentos',
										'singular_name'      => 'Depoimento',
										'menu_name'          => 'Depoimentos',
										'name_admin_bar'     => 'Depoimentos',
										'add_new'            => 'Adicionar depoimento',
										'add_new_item'       => 'Adicionar novo depoimento',
										'new_item'           => 'Novo depoimento',
										'edit_item'          => 'Editar depoimento',
										'view_item'          => 'Ver depoimento',
										'all_items'          => 'Todos depoimentos',
										'search_items'       => 'Buscar depoimentos',
										'parent_item_colon'  => 'Dos depoimentos',
										'not_found'          => 'Nenhum depoimento cadatrado.',
										'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
									);

			$argsDepoimentos 		= array(
									'labels'             => $rotulosDepoimentos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-format-chat',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'depoimentos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'editor', 'thumbnail', )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('depoimentos', $argsDepoimentos);

		}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaTatigodoy () {

	}

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesTatigodoy(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Tatigodoy_';

			// E-BOOKS
			$metaboxes[] = array(

				'id'			=> 'ebooksMetabox',
				'title'			=> 'Detalhes adicionais do E-book',
				'pages' 		=> array( 'ebooks' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'E-book',
						'id'    => "{$prefix}ebook_arquivo",
						'desc'  => '',
						'type'  => 'file_advanced',
						'max_file_uploads'	=> 1
					),
				),
				'validation' 	=> array()
			);

			// DOWNLOADS
			$metaboxes[] = array(

				'id'			=> 'downloadsMetabox',
				'title'			=> 'Detalhes adicionais do download',
				'pages' 		=> array( 'downloads' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Arquivo',
						'id'    => "{$prefix}download_arquivo",
						'desc'  => '',
						'type'  => 'file_advanced',
						'max_file_uploads'	=> 1
					)

				),
				'validation' 	=> array()
			);

			// POs
			$metaboxes[] = array(

				'id'			=> 'posMetabox',
				'title'			=> 'Detalhes PO',
				'pages' 		=> array( 'pos' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'E-mail',
						'id'    => "{$prefix}po_email",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Facebook',
						'id'    => "{$prefix}po_facebook",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Twitter',
						'id'    => "{$prefix}po_twitter",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Telefone',
						'id'    => "{$prefix}po_telefone",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'   => 'UF',
						'id'     => "{$prefix}po_uf",
						'options'=>  array("AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapá","BA"=>"Bahia","CE"=>"Ceará","DF"=>"Distrito Federal","ES"=>"Espírito Santo","GO"=>"Goiás","MA"=>"Maranhão","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Pará","PB"=>"Paraíba","PR"=>"Paraná","PE"=>"Pernambuco","PI"=>"Piauí","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondônia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"São Paulo","TO"=>"Tocantins"),
						'type'   => 'select'
					),
					array(
						'name'  => 'Cidade',
						'id'    => "{$prefix}po_cidade",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Onde atende?',
						'id'    => "{$prefix}po_regiao_atendimento",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Possui certificação?',
						'id'    => "{$prefix}po_certificacao",
						'options'  => array(
								'Sim' 	=> 'Sim',
								'Não' 	=> 'Não',
							),
						'type'  => 'select'
					)

				),
				'validation' 	=> array()
			);

			return $metaboxes;
		}

		// function metaboxjs(){

		// 	global $post;
		// 	$template = get_post_meta($post->ID, '_wp_page_template', true);
		// 	$template = explode('/', $template);
		// 	$template = explode('.', $template[1]);
		// 	$template = $template[0];

		// 	if($template != '' && $template != 'trabalhe' && $template != 'fornecedores'){
		// 		wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
		// 	}
		// }

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesTatigodoy(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerTatigodoy(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseTatigodoy');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseTatigodoy();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );
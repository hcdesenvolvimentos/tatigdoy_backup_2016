<?php get_header(); ?>

                   <div id="cont-col">
                        	<div class="corner-top-left">
                            	<div class="corner-top-right">
                                	<div class="border-top"></div>
                                </div>
                            </div>
							<div class="border-right">
                            	<div class="bg-cont">
									<?php if (have_posts()) : ?>

										<?php while (have_posts()) : the_post(); ?>
											
																				<div class="node">
																					<h1 class="title"><?php the_title(); ?></h1>
																					<div class="submit"><span class="submitted">Postado por: <?php the_author_nickname() ?> em <?php the_time('d') ?> de <?php the_time('F') ?>  <?php the_time('Y') ?></span>
																					</div>  
																					<!-- Início Redes Sociais -->
																					<script type="text/javascript">trackFacebook();</script><fb:like href="<?php echo get_permalink() ?>" send="true" show_faces="true" layout="button_count" width="150"></fb:like>
																					<g:plusone size="medium" href="<?php echo get_permalink() ?>"></g:plusone> 
																					<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-url="<?php echo get_permalink() ?>" data-counturl="<?php echo get_permalink() ?>" data-text="<?php the_title(); ?>" data-via=""></a>
																					<script type="IN/Share" data-url="<?php echo get_permalink() ?>" data-counter="right" data-success="LinkedInShare"></script> <script type="text/javascript"> function LinkedInShare() { _gaq.push(['_trackSocial', 'LinkedIn', 'Share']); } </script>
																					<!-- FIM Redes Sociais -->
											<div class="content">
												<?php the_content('Read the rest of this entry &raquo;'); ?>
												<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
												<div class="clear"></div>
											</div>

<div class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Postado em <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></div>
												<?php  edit_post_link('Edit this entry','','.'); ?>
<br />
												<?php comments_template('', true); ?>
											<div class="clear"></div>
																				</div>
										<?php endwhile; ?>
										<div class="navigation">
											<?php
												if(!function_exists('wp_pagenavi')) { 
													include('wp-pagenavi.php');
												}
												wp_pagenavi(); 
											?>
										</div>
									<?php else : ?>
										<h2 class="center">Não encontrado =(</h2>
										<p class="center">Não foi possível encontrar esse item.</p>
										<?php get_search_form(); ?>

									<?php endif; ?>
	
                                </div>
                            </div>
							
                            <div class="corner-bot-left">
                            	<div class="corner-bot-right">
                                	<div class="border-bot">
									</div>
                                </div>
                            </div>
                           
                    </div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
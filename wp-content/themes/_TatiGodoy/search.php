<?php get_header(); ?>

                   <div id="cont-col">
                        	<div class="corner-top-left">
                            	<div class="corner-top-right">
                                	<div class="border-top"></div>
                                </div>
                            </div>
							<div class="border-right">
                            	<div class="bg-cont">
									<?php if (have_posts()) : ?>
										  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
										  <?php /* If this is a category archive */ if (is_category()) { ?>
										  <h2>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h2>
										  <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
											<h2>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2>
										  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
											<h2>Archive for <?php the_time('F jS, Y'); ?></h2>
										  <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
											<h2>Archive for <?php the_time('F, Y'); ?></h2>
										  <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
											<h2>Archive for <?php the_time('Y'); ?></h2>
										  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
											<h2>Author Archive</h2>
										  <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
											<h2>Blog Archives</h2>
										  <?php } ?>

										<?php while (have_posts()) : the_post(); ?>
											<div class="node">
												<h1 class="title"><a href="<?php the_permalink() ?>" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
												<div class="submit"><span class="submitted">Postado por: <?php the_author_nickname() ?> em <?php the_time('d') ?> de <?php the_time('F') ?>  <?php the_time('Y') ?></span>
												</div>
												<div class="taxonomy">
												</div>
												<!-- Início Redes Sociais -->
												<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><script type="text/javascript">trackFacebook();</script><fb:like href="<?php echo get_permalink() ?>" send="true" show_faces="true" layout="button_count" width="150"></fb:like> <script> (function() { var e = document.createElement('script'); e.async = true; e.src = (document.location.protocol == 'file:' ? 'http:' : document.location.protocol) + '//connect.facebook.net/en_US/all.js'; document.getElementById('fb-root').appendChild(e); }()); window.fbAsyncInit = function() { FB.init({xfbml: true}); FB.Event.subscribe('edge.create', function(targetUrl){ _gaq.push(['_trackSocial', 'facebook', 'like', targetUrl]); }); FB.Event.subscribe('edge.remove', function(targetUrl){ _gaq.push(['_trackSocial', 'facebook', 'unlike', targetUrl]); }); FB.Event.subscribe('message.send', function(targetUrl) { _gaq.push(['_trackSocial', 'facebook', 'send', targetUrl]); }); FB.Event.subscribe('comment.create', function(targetUrl) { _gaq.push(['_trackSocial', 'facebook', 'comment', targetUrl]); }); FB.Event.subscribe('comment.remove', function(targetUrl) { _gaq.push(['_trackSocial', 'facebook', 'uncomment', targetUrl]); }); }; </script> 
												<g:plusone size="medium" href="<?php echo get_permalink() ?>"></g:plusone> <!-- Place this tag after the last plusone tag --> <script type="text/javascript"> (function() { var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.src = 'https://apis.google.com/js/plusone.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s); })(); </script>
												<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-url="<?php echo get_permalink() ?>" data-counturl="<?php echo get_permalink() ?>" data-text="<?php the_title(); ?>" data-via=""></a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script> <script type="text/javascript"> twttr.events.bind('tweet', function(event) { if (event) { var targetUrl; if (event.target && event.target.nodeName == 'IFRAME') { targetUrl = extractParamFromUri(event.target.src, 'url'); } _gaq.push(['_trackSocial', 'twitter', 'tweet', targetUrl]); } }); twttr.events.bind('follow', function(){ var targetUrl; if (event.target && event.target.nodeName == 'IFRAME') { targetUrl = extractParamFromUri(event.target.src, 'url'); } _gaq.push(['_trackSocial', 'twitter', 'follow', targetUrl]); }); twttr.events.bind('retweet', function(){ var targetUrl; if (event.target && event.target.nodeName == 'IFRAME') { targetUrl = extractParamFromUri(event.target.src, 'url'); } _gaq.push(['_trackSocial', 'twitter', 'retweet', targetUrl]); }); twttr.events.bind('favorite', function(){ var targetUrl; if (event.target && event.target.nodeName == 'IFRAME') { targetUrl = extractParamFromUri(event.target.src, 'url'); } _gaq.push(['_trackSocial', 'twitter', 'favorite', targetUrl]); }); twttr.events.bind('click', function(){ var targetUrl; if (event.target && event.target.nodeName == 'IFRAME') { targetUrl = extractParamFromUri(event.target.src, 'url'); } _gaq.push(['_trackSocial', 'twitter', 'click', targetUrl]); }); function extractParamFromUri(uri, paramName){ if(!uri){ return; } var uri = uri.split('#')[0]; //remove anchor var parts = uri.split('?'); //check for query params if(parts.length == 1){ return; //no params } var query = decodeURI(parts[1]); //find the url param paramName += '='; var params = query.split('&'); for(var i = 0, param; param = params[i]; ++i){ if(param.indexOf(paramName) === 0){ return unescape(param.split('=')[1]); } } } </script>
												<script src="http://platform.linkedin.com/in.js" type="text/javascript"></script> <script type="IN/Share" data-url="<?php echo get_permalink() ?>" data-counter="right" data-success="LinkedInShare"></script> <script type="text/javascript"> function LinkedInShare() { _gaq.push(['_trackSocial', 'LinkedIn', 'Share']); } </script>
												<!-- FIM Redes Sociais -->	  
												<div class="content">
													<?php if (has_post_thumbnail()){?>
														<?php if (!get_option('timthumb')) { ?><img class="bordered main-image" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo PostThumbURL(); ?>&w=125&h=125&zc=1" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/imagethumb.png'" style="width:125px; height:125px"><?php } else { ?><img class="bordered main-image" src="<?php echo PostThumbURL(); ?>" style="width:125px; height:125px" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/imagethumb.png'"/><?php } ?>
													<?php }  else if (catch_that_image()) {?>
														<?php if (!get_option('timthumb')) { ?><img class="bordered main-image" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo catch_that_image(); ?>&w=125&h=125&zc=1" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/imagethumb.png'" style="width:125px; height:125px"><?php } else { ?><img class="bordered main-image" src="<?php echo catch_that_image(); ?>" style="width:125px; height:125px" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/imagethumb.png'" /><?php }?>
													<?php } ?>

													<?php the_excerpt(); ?>
													<div class="clear"></div>
												</div>

													<div class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Postado em <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></div>
					
												<div class="clear"></div>
											</div>
										<?php endwhile; ?>
										<div class="navigation">
											<?php
												if(!function_exists('wp_pagenavi')) { 
													include('wp-pagenavi.php');
												}
												wp_pagenavi(); 
											?>
										</div>
									<?php else : ?>

										<h2 class="center">Não encontrado =(</h2>
										<p class="center">Não foi possível encontrar esse item.</p>
										<?php get_search_form(); ?>

									<?php  endif; ?>
	
                                </div>
                            </div>
							
                            <div class="corner-bot-left">
                            	<div class="corner-bot-right">
                                	<div class="border-bot">
									</div>
                                </div>
                            </div>
                           
                    </div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
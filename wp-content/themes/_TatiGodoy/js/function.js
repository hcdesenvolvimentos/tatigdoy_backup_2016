jQuery( function() {
	jQuery( '.category' ).click( function() {
		jQuery( this ).toggleClass( 'active' )
		jQuery( this ).siblings( '.dropdown' ).toggle();
		jQuery( this ).find( '.indicator' ).toggleClass( 'indicator-active' );
		return false;
	} );
			
	jQuery( '#dock > li' ).hover( function() {
		jQuery( '.latest' ).fadeOut( 'fast' );
		jQuery( this ).addClass( 'dock-active' );
		jQuery( this ).children( 'span' ).fadeIn( 200 );
	}).bind( "mouseleave", function() {		
		jQuery( this ).removeClass( 'dock-active' );	
		jQuery( this ).children( 'span' ).fadeOut( 200 );
	} );
			
	jQuery( '#dock' ).bind( "mouseleave", function() {
		jQuery( '.latest' ).fadeIn( 1000 );
	} );
} );
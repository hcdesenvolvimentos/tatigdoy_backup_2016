<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>


<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    

<?php wp_enqueue_script('jquery'); ?>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>	
<?php wp_head(); ?>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/superfis.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/scrollTo.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/serialSc.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/init0000.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/custom00.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/function.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jqueryslidemenu.js"></script>
</head>
  
<body>
<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
<script src="http://platform.linkedin.com/in.js" type="text/javascript"></script> 																		

        <div id="main">
            <div id="header">
                <div class="head-row1">
                	<div class="col1">
                    	<a href="<?php echo get_option('home'); ?>" title="Home"><img src="<?php if (get_option('logo')) : echo get_option('logo'); else: bloginfo('stylesheet_directory');?>/images/logo.png<?php endif; ?>" alt="Tati Godoy" class="logo" /></a>
                    </div>
                    
                </div>
                <div class="head-row2">
                	<div class="col1">
						<?php wp_nav_menu( array( 'container_class' => 'wrapper-top-menu', 'menu_class' => 'menu-nav', 'container_id' => 'wrapper-top-menu', 'theme_location' => 'primary' ) ); ?>
						<script type="text/javascript">
							jqueryslidemenu.buildmenu("wrapper-top-menu", arrowimages);
						</script>						
                    </div>
                    <div class="col2">
                    	<div class="search-box"><?php include(TEMPLATEPATH . '/searchform.php' ); ?>
                        </div>
                    </div>
                </div>
				<?php if (is_home() && !is_paged()) : ?>
               	<div class="clear"></div>
<div class="featured">
    <div id="sections">
        <ul> 
				<div class="featured-posts-box">
			<?php
            $recentPosts = new WP_Query();
            $recentPosts->query('showposts=3'); ?>
            <ul class="top-featured-image">
            <?php while ($recentPosts->have_posts()) : $recentPosts->the_post(); ?>
            <li class="top-featured-image">
            <span><a href="<?php the_permalink(); ?>"></a></span> 
            <?php if ( has_post_thumbnail()) : the_post_thumbnail(array(655,500)); endif; ?>
            </li>
            <?php endwhile; ?>
</ul>
</div>
	    </ul>
    </div> <!-- -end sections -->
    <div id="featured-right">
        <div id="sections2">
            <ul>
				<?php
					$topfeatured = new WP_Query();
					$topfeatured->query('showposts=3');
					if($topfeatured->have_posts()) : while($topfeatured->have_posts()): $topfeatured->the_post();
				?>
					<li>
						<h2 class="featured-title"><a href="<?php the_permalink(); ?>" title="Permanent Link to <?php the_title();?>"><?php the_title();?></a></h2>
						<div class="featured-info"></div>
						<p><?php excerpt_link_modify2() ?></p>
						<div class="clear"></div>
						<a href="<?php the_permalink(); ?>" class="featured-readmore">Leia mais</a> 
						<div class="clear"></div>
					</li>
				<?php endwhile; ?>
				<?php endif; ?>
				
               </ul>
        </div> <!-- -end sections2 -->
        <div class="clear"></div>
        <div id="featured-button">
            <div class="prev">
                <div class="prev-hover"> </div>
            </div>
            <div class="next">
                <div class="next-hover"></div>
            </div>
        </div> <!-- -end featured-bottom -->
    </div> <!-- -end featured-right -->
</div> 
                <div class="head-row4" id="custom">
						<div class="block" id="block-block-14">
							<div class="content">
									<?php
										$featured = new WP_Query();
										$featured->query('showposts=3&offset=3');
										if($featured->have_posts()) : $featured->the_post();
										$featured_ID = $post->ID;
									?>
										<div class="title">
											<h3><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title();?></a></h3>
										</div>
										<div class="block-content">
											
											<?php if (has_post_thumbnail()){?>
												<?php if (!get_option('timthumb')) { ?><img class="bordered" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo PostThumbURL(); ?>&w=269&h=126&zc=1" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'"><?php } else { ?><img class="bordered" src="<?php echo PostThumbURL(); ?>" style="width:269px; height:126px" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'" /><?php }?>
											<?php } else if (catch_that_image()){?>
												<?php if (!get_option('timthumb')) { ?><img class="bordered" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo catch_that_image(); ?>&w=269&h=126&zc=1" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'"><?php } else { ?><img class="bordered" src="<?php echo catch_that_image(); ?>" style="width:269px; height:126px" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'" /><?php }?>
											<?php } else { ?>
												<img class="bordered" src="<?php bloginfo('template_directory'); ?>/images/image.jpg" />
											<?php }?>
											
											<a href="<?php the_permalink(); ?>" ><?php excerpt_link_modify2(); ?></a>
										</div>

									<?php endif; ?>                                  
							</div>
						</div>
						<div class="block" id="block-block-15">
							<div class="content">
									<?php										
										if($featured->have_posts()) : $featured->the_post();
										$featured2_ID = $post->ID;
									?>
										<div class="title">
											<h3><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title();?></a></h3>
										</div>
										<div class="block-content">
											<?php if (has_post_thumbnail()){?>
												<?php if (!get_option('timthumb')) { ?><img class="bordered" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo PostThumbURL(); ?>&w=269&h=126&zc=1" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'"><?php } else { ?><img class="bordered" src="<?php echo PostThumbURL(); ?>" style="width:269px; height:126px" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'" /><?php }?>
											<?php } else if (catch_that_image()){?>
												<?php if (!get_option('timthumb')) { ?><img class="bordered" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo catch_that_image(); ?>&w=269&h=126&zc=1" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'"><?php } else { ?><img class="bordered" src="<?php echo catch_that_image(); ?>" style="width:269px; height:126px" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'" /><?php }?>
											<?php } else { ?>
												<img class="bordered" src="<?php bloginfo('template_directory'); ?>/images/image.jpg" /> 
											<?php }?>
											<a href="<?php the_permalink(); ?>" ><?php excerpt_link_modify2(); ?></a>
										</div>

									<?php endif; ?>		
																
							</div>
						</div>
						<div class="block" id="block-block-16">
							<div class="content">
									<?php
										if($featured->have_posts()) : $featured->the_post();
										$featured3_ID = $post->ID;
									?>
										<div class="title">
											<h3><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title();?></a></h3>
										</div>
										<div class="block-content">
											<?php if (has_post_thumbnail()){?>
												<?php if (!get_option('timthumb')) { ?><img class="bordered" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo PostThumbURL(); ?>&w=269&h=126&zc=1" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'"><?php } else { ?><img class="bordered" src="<?php echo PostThumbURL(); ?>" style="width:269px; height:126px" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'" /><?php }?>
											<?php } else if (catch_that_image()){?>
												<?php if (!get_option('timthumb')) { ?><img class="bordered" src="<?php bloginfo('template_directory'); ?>/timthumb.php?src=<?php echo catch_that_image(); ?>&w=269&h=126&zc=1" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'"><?php } else { ?><img class="bordered" src="<?php echo catch_that_image(); ?>" style="width:269px; height:126px" onerror="this.src='<?php bloginfo('template_directory'); ?>/images/image.jpg'" /><?php }?>
											<?php } else { ?>
												<img class="bordered" src="<?php bloginfo('template_directory'); ?>/images/image.jpg" />
											<?php }?>
											
											<a href="<?php the_permalink(); ?>" ><?php excerpt_link_modify2(); ?></a>
										</div>

									<?php endif; ?>	
							</div>


						</div>              
                </div>
				<?php endif; ?>
            </div>
            <div class="clear"></div>
                            
            <div id="cont">
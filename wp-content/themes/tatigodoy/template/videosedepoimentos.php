<?php
/**
 * Template Name: Videos e Depoimentos
 * Description:
 *
 * @package Tatigodoy
 */

get_header();

?>
	<!-- PÁGINA VIDEOS E DEPOIMENTOS -->
	<div class="pg-depoimentos container correcao-xy">

         <!-- TÍTULO -->
        <div class="row correcao-y">
           <div class="titulo-pagina col-md-offset-2 col-md-8">
               <div class="titulo text-center">
                   <span><strong>depoimentos</strong> - Confira abaixo <img src="<?php echo get_template_directory_uri(); ?>/img/balao.png"></span>
               </div>
           </div>
        </div>

        <!-- VÍDEO -->
        <div class="row">

            <?php
            $videosCont = 0;
            $loopVideos = new WP_Query( array( 'post_type' => 'videos', 'posts_per_page' => -1) );
            while ( $loopVideos->have_posts() ) : $loopVideos->the_post();
                if ($videosCont == 0 ):
                    $videoUrl = get_the_content();
                    $videoMatches = array();
                    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $videoUrl, $videoMatches);
                    $videoYoutubeId = (count($videoMatches) > 0) ? $videoMatches[1] : '';
            ?>

            <div class="dash col-md-12">
                <div class="video">
                    <iframe width="560" height="349" src="https://www.youtube.com/embed/<?php echo $videoYoutubeId; ?>" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <?php $videosCont++; ?>

            <?php else:
                $videoUrl = get_the_content();
                $videoMatches = array();
                preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $videoUrl, $videoMatches);
                $videoYoutubeId = (count($videoMatches) > 0) ? $videoMatches[1] : '';

            ?>

            <div class="dash col-md-6">
                <div class="video">
                    <iframe width="560" height="349" src="https://www.youtube.com/embed/<?php echo $videoYoutubeId; ?>" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <?php $videosCont++; ?>

            <?php endif; endwhile; wp_reset_query(); ?>

        </div>

         <!-- VÍDEO -->
        <!-- <div class="row">
            <div class="col-md-12">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/HfMxaU9iE24" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

         <div class="row">
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/qBnxL2KYkpA" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/9GOrKXRBRag" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/MybNFN0eBZ0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/dVrPTWRjuuU" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/9lwXH7d0CfM" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/FcEWIiK4wck" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/TFiJsUI3mbo" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/Lmz7S8bhtUA" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/EmEO4bpiW14" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/vBhgy3f3mMQ" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/CDyhbNdiIWI" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="dash">
                    <div class="video">
                        <iframe width="560" height="349" src="https://www.youtube.com/embed/J6CsOlCMVqI" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div> -->

        <!-- DEPOIMENTOS -->
        <div class="conteudo row correcao-y">

            <?php
            $loopDepoimentos = new WP_Query( array( 'post_type' => 'depoimentos', 'posts_per_page' => -1) );
            while ( $loopDepoimentos->have_posts() ) : $loopDepoimentos->the_post();
            ?>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span><?php echo the_title(); ?></span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <p><?php echo get_the_content(); ?></p>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data"><?php echo get_the_date(); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php endwhile; wp_reset_query(); ?>

<!--
            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Monica Hetzel Gofferjé</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Muito obrigada... nada acontece por acaso. Acredito que seu curso foi uma forma de Deus me mostrar que a força está dentro de mim. Gratidão!.</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Melissa Pisacco Bidegain Witzel</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Eu amei fazer o curso em SP esse find. Além de aprender dicas maravilhosas,conheci pessoas geniais,principalmente a Tati ,uma mulher vencedora e que não nos nega nenhuma informação.</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Cassia Borges Barbosa</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Amei conhecer! É tudo de bom! Aprendendo organização, passamos a ter uma vida mais livre, aproveitamos mais nosso tempo, que é tão curtinho. . gostei das dicas! Cassia Borges Barbosa.</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 as 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Giselle Hamaue</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Fiz o curso de organização com a Tati Godoy e consegui organizar minha vida.Obrigada,Tati,bjs</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 as 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Ana Cristina Escobar Marques</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Sensacional ...mudou a minha vida e agora quero ajudar pessoas a mudar suas vidas tb ...apaixonante ...a Taty é massaaaa</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 as 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Débora Fabrini</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Foi muito bom, amei o curso, me trouxe grandes conhecimento de como organizar uma casa com plasticidade, e teve bastante novidade. Espero mais conhecimento..breve.</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 as 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Eudilene Almeida</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Tati amei curso sai apaixonada com varias ideais,estou muito feliz ter participado .</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 as 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Ediane Tramujas Resende</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Amei o curso, quero mais! Vontade de sair organizando tudo!!</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 as 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Pamela Raquel Wagnitz Nepomuceno</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Participei do curso em Florianópolis e super recomendo. Valeu muito a pena.</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 ás 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Fernanda Brandes Hoffmann</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Recomendo. A Taty é 10!!!</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 ás 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Fernanda Brandes Hoffmann</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Perfeito! Curso maravilhoso, equipe atenciosa, gentil. Vale a pena conferir!</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 ás 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Maria Florbela Vcs Pinto</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Sabe quando algo acaba e você quer mais? É assim q me sinto.... Acabou e eu já quero outro curso, outras dicas,outras sugestões... Tati, você é querida, iluminada e "organizadissima". Parabéns. Entrou na minha vida pela porta da frente. Nao vai sair mais. Bjs nesse coração de luz. Sua aluna portuguesa que se chama Florbela!</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 ás 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Claudinei Costa</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Dicas muito bem vindas para facilitar e melhorar nosso dia a dia. Parabéns, tornando melhor a cada dia a organização das pessoas.</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 ás 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="depoimento">
                    <div class="nome">
                        <span>Débora Fabrini</span>
                    </div>
                    <div class="dash">
                        <div class="comentario">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/aspas.png">
                            <div class="texto">
                                <span>Foi muito bom, amei o curso. Me trouxe grande conhecimento de como organizar uma casa com praticidade.</span>
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6 text-right">
                                      <span class="data">21/07/2015 ás 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

        </div>
    </div>

<?php get_footer(); ?>
<?php
/**
 * Template Name: Estante Virtual
 * Description:
 *
 * @package Tatigodoy
 */

get_header();

?>

    <!-- SCRIPTS MAILPOET -->
    <script type="text/javascript" src="http://www.tatigodoy.com.br/wp-includes/js/jquery/jquery.js?ver=2.6.19"></script>
    <script type="text/javascript" src="http://www.tatigodoy.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.6.19"></script>
    <script type="text/javascript" src="http://www.tatigodoy.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.6.19"></script>
    <script type="text/javascript" src="http://www.tatigodoy.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.6.19"></script>
    <script type="text/javascript">
    /* <![CDATA[ */
    var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://www.tatigodoy.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
    /* ]]> */
    </script>
    <script type="text/javascript" src="http://www.tatigodoy.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.6.19"></script>

    <style type="text/css">

        /* PÁGINA ESTANTE VIRTUAL */
        .pg-estante .titulo-pagina{
            padding: 0 50px;
        }

        @media screen and (max-width: 992px) {

        .pg-estante .titulo-pagina{
            padding: 0 5px;
        }

        }
            .pg-estante .titulo{
                background: #84d1d4;
                padding: 6px 0;
                border-bottom-right-radius: 8px;
                border-bottom-left-radius: 8px;
                min-height: 50px;
                margin-bottom: 40px;
                max-width: 606px;
                clear: both;
                float: none;
                margin: 0 auto 40px auto;
            }

                @media screen and (max-width: 992px) {

                .pg-estante .titulo{
                    border-radius: 8px;
                }
                .navegacaoCarrosselTras,
                .navegacaoCarrosselFrente{
                    display: none;
                }

                }

                .pg-estante .titulo span{
                    color: #d7597d;
                    font-size: 18px;
                    vertical-align: -webkit-baseline-middle;
                    padding: 0 5px;
                }
                    .pg-estante .titulo span strong{
                        font-size: 24px;
                        color: #fff;
                        text-transform: uppercase;
                        font-weight: normal;
                        vertical-align: middle;
                        display: inline-block;
                            transform: scale(0.8,1.3);
                            -webkit-transform:scale(0.8,1.3);
                        -moz-transform:scale(0.8,1.3);
                        -ms-transform:scale(0.8,1.3);
                        -o-transform:scale(0.8,1.3);
                    }
                    .pg-estante .titulo span i{
                        font-size: 34px;
                        vertical-align: middle;
                        margin: 0 10px;
                    }

                .texto-slide{
                    font-size: 20px;
                    font-weight: 400;
                    color: #fff;
                    margin-top: -5px;
                }

        .fundo-area{
            min-height: 480px;
            background: url(<?php echo get_template_directory_uri(); ?>/img/fundo-slide.png) center center no-repeat;
            background-size: cover;
            position: relative;
        }
            .titulo-area{
                background: url(<?php echo get_template_directory_uri(); ?>/img/fundo-titulo.png) center center no-repeat;
                width: 336px;
                height: 44px;
                margin: 40px auto 0 auto;
            }
                .titulo-area span{
                    color: #fff;
                    font-size: 18px;
                    line-height: 36px;
                }

        .navegacaoCarrosselTras{
            background: url(<?php echo get_template_directory_uri(); ?>/img/prev.png) center center no-repeat;
            height: 35px;
            width: 35px;
            outline: 0;
            border: none;
            position: absolute;
            top: 230px;

        }
        .navegacaoCarrosselTras:hover{
            background: url(<?php echo get_template_directory_uri(); ?>/img/prev-hover.png) center center no-repeat;
        }
        .navegacaoCarrosselFrente{
            background: url(<?php echo get_template_directory_uri(); ?>/img/next.png) center center no-repeat;
            height: 35px;
            width: 35px;
            outline: 0;
            border: none;
            position: absolute;
            top: 230px;
            margin-left: 10px;
        }
        .navegacaoCarrosselFrente:hover{
            background: url(<?php echo get_template_directory_uri(); ?>/img/next-hover.png) center center no-repeat;
        }

        .area-slide{
            background: #479ca0;
            min-height: 325px;
            margin-top: 20px;
            border-radius: 5px;
            border: 5px solid rgba(255, 255, 255, .5);
            -webkit-background-clip: padding-box; /* for Safari */
            background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */
            padding: 0 30px 10px 30px;
        }
            .texto-slide{
                font-size: 20px;
                font-weight: 400;
                color: #fff;
                margin-top: -5px;
            }
            .slide-livros{
                height: 215px;
                background: #82bcbf;
                border-radius: 5px;
                margin: 10px 0 0 0;
                padding: 10px;
            }
                 .slide-livros h3{
                    margin:0;
                    padding:0;
                    color:#fff;
                    font-size: 15px;
                 }
                        .slide-livros h3 a{
                            text-decoration: none;
                            color:#fff;
                            font-size: 15px;
                        }
                .slide-livros .owl-carousel .owl-item img{
                    width: 109px;
                    height: 154px;
                    border: 1px solid #888b95;
                    cursor: pointer;
                }
                .slide-arq .owl-carousel .owl-item img{
                    width: 128px;
                    height: 128px;
                    background: #fff;
                    border: 2px solid #fff;
                    -webkit-background-clip: padding-box; /* for Safari */
                    background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */
                    border-radius: 100px;
                    overflow: hidden;
                    display: block;
                    margin: 0 auto;
                    cursor: pointer;
                }
                .slide-livros .item span{
                    height: 50px;
                    width: 50px;
                    display: block;
                    background: url(<?php echo get_template_directory_uri(); ?>/img/novo.png) center center no-repeat !important;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                }
                .owl-nav{
                    display: none;
                }

        .dashed{
            margin: 5px auto;
            border-bottom: 2px dashed #6cb0b3;
        }

        .fundo-arq{
            min-height: 200px;
            background: #84d1d4;
        }
            .slide-arq{
                margin: 20px 0;
            }
                .slide-arq .item span{
                    font-size: 22px;
                    font-weight: 400;
                    color: #fff;
                }
                .item strong.doc{
                    height: 34px;
                    width: 36px;
                    display: block;
                    background: url(<?php echo get_template_directory_uri(); ?>/img/doc.png) center center no-repeat !important;
                    position: absolute;
                    bottom: 30px;
                    right: 30px;
                }
                .slide-arq .owl-controls{
                    display: none;
                }
            .navegacaoCarrosselT{
                background: url(<?php echo get_template_directory_uri(); ?>/img/prev-a.png) center center no-repeat;
                height: 35px;
                width: 35px;
                outline: 0;
                border: none;
                position: absolute;
                top: 80px;
            }
            .navegacaoCarrosselT:hover{
                background: url(<?php echo get_template_directory_uri(); ?>/img/prev-a-hover.png) center center no-repeat;
            }
            .navegacaoCarrosselF{
                background: url(<?php echo get_template_directory_uri(); ?>/img/next-a.png) center center no-repeat;
                height: 35px;
                width: 35px;
                outline: 0;
                border: none;
                position: absolute;
                top: 80px;
                margin-left: 10px;
            }
            .navegacaoCarrosselF:hover{
                background: url(<?php echo get_template_directory_uri(); ?>/img/next-a-hover.png) center center no-repeat;
            }

        /* POSIÇÃO DA MODAL */
        .modal-dialog {
          position:absolute;
          top:50% !important;
          transform: translate(40%, -50%) !important;
          -ms-transform: translate(40%, -50%) !important;
          -webkit-transform: translate(40%, -50%) !important;
          margin:auto 5%;
          width:    50%;
          height:   260px;
        }
        .modal-content {
          min-height:100%;
          position:absolute;
          top:0;
          bottom:0;
          left:0;
          right:0;
        }
        .modal-body {
          position:absolute;
          top:45px;
          bottom:45px;
          left:0;
          right:0;
          overflow-y:auto;
        }
        .modal-footer {
          position:absolute;
          bottom:0;
          left:0;
          right:0;
        }

    </style>

    <!-- MODAL -->
    <div id="modalCadastro" class="modal fade" role="dialog">

      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Você fará o download em alguns instantes...</h4>
          </div>

          <div class="modal-body">

            <p>Para fazer o download informe um endereço de e-mail para receber gratuitamente novidades exclusivas!</p>

            <!-- FORMULÁRIO -->
            <div class="widget_wysija_cont html_wysija">
                <div id="msg-form-wysija-html563a7bd2869e3-1" class="wysija-msg ajax"></div>

                <form id="form-wysija-html563a7bd2869e3-1" method="post" action="#wysija" class="widget_wysija html_wysija">
                    <p class="wysija-paragraph">
                        <label>Informe o seu melhor e-mail: <span class="wysija-required">*</span></label>
                        <input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email"  value="" />

                        <span class="abs-req">
                            <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
                        </span>
                    </p>

<!--                     <p class="wysija-paragraph" style="display: none;">
                        <label>Observação</label>

                            <input type="text" id="observacao" name="wysija[field][cf_1]" class="wysija-input " title="observacao"  value="" />



                        <span class="abs-req">
                            <input type="text" name="wysija[field][abs][cf_1]" class="wysija-input validated[abs][cf_1]" value="" />
                        </span>

                    </p>
 -->
                    <p class="wysija-paragraph" style="display: none;">
                        <label>Baixou</label>

                        <input type="text" id="observacao" name="wysija[field][cf_2]" class="wysija-input " title="Baixou"  value="" />



                        <span class="abs-req">
                            <input type="text" name="wysija[field][abs][cf_2]" class="wysija-input validated[abs][cf_2]" value="" />
                        </span>

                    </p>

                    <input class="wysija-submit wysija-submit-field btn btn-primary" type="submit" value="Cadastrar" />

                    <input type="hidden" name="form_id" value="1" />
                    <input type="hidden" name="action" value="save" />
                    <input type="hidden" name="controller" value="subscribers" />
                    <input type="hidden" value="1" name="wysija-page" />
                    <input type="hidden" name="wysija[user_list][list_ids]" value="1" />
                    <input type="hidden" value="" id="download-arquivo" />

                </form>
            </div>

          </div>

          <div class="modal-footer">
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button> -->
          </div>

        </div>

      </div>
    </div>

    <!-- PÁGINA ESTANTE VIRTUAL -->
    <div class="pg-estante">

        <div class="row correcao-y">
           <div class="titulo-pagina col-md-offset-2 col-md-8 col-sm-12">
               <div class="titulo text-center">
                   <span><strong>Estante virtual</strong> - Confira nossa área de downloads <i class="fa fa-cloud-download"></i></span>
               </div>
           </div>
        </div>

        <!-- E-BOOKS -->
        <div class="row">

            <div class="fundo-area container correcao-x">

                <div class="titulo-area text-center">
                    <span>Download de E-books</span>
                </div>

                <div class="col-md-1 text-center">
                    <button class="navegacaoCarrosselTras"></i></button>
                </div>
                <div class="area-slide col-md-10">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/ebookdown.png">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 texto-slide text-center">
                            <span>Baixe agora mesmo <strong>gratuitamente</strong> e-books exclusivos!</span>
                        </div>
                        <div class="slide-livros col-md-12">
                            <div id="carrossel" class="owl-carousel">

                                <?php

                                    $downloadsEbooks = new WP_Query( array( 'post_type' => 'dlm_download', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1, 'tax_query'       => array(
                                                        array(
                                                            'taxonomy' => 'dlm_download_category',
                                                            'field'    => 'slug',
                                                            'terms'    => 'ebooks',
                                                        )
                                                    ) ) );

                                    while ( $downloadsEbooks->have_posts() ) : $downloadsEbooks->the_post();

                                    $capa = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                                    $capa = $capa[0];

                                    foreach (rwmb_meta('Tatigodoy_ebook_arquivo', 'type=file') as $arquivo) {
                                        $arquivoEbook = $arquivo['url'];
                                    }
                                    global $post;

                                $downloadEbook = new DLM_Download( $post->ID );
                                $linkEbook = $downloadEbook->get_the_download_link();

                                ?>

                                <div class="item text-center">
                                    <a style="display: block;" data-arquivo="<?php echo $linkEbook; ?>" data-nome="<?php echo get_the_title(); ?>" title="Clique para baixar <?php echo get_the_title(); ?>" class="acaoDownload"><img src="<?php echo $capa; ?>"  class="center-block"/></a>
                                    <h3><?php echo get_the_title(); ?></h3>
                                </div>

                                <?php endwhile; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <button class="navegacaoCarrosselFrente"></button>
                </div>

            </div>

            <div class="dashed container"></div>

        </div>

        <!-- DOWNLOADS -->
        <div class="row correcao-y">

            <div class="titulo-area text-center">
                <span>Downloads diversos</span>
            </div>

            <div class="dashed container top10"></div>

            <div class="fundo-arq container">

                <div class="row">

                    <div class="col-md-1 text-center">
                        <button class="navegacaoCarrosselT hidden-xs"></button>
                    </div>

                    <div class="slide-arq col-md-10">

                        <div id="carrossel-2" class="owl-carousel">

                            <?php

                                $downloadsDiversos = new WP_Query( array( 'post_type' => 'dlm_download', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1, 'tax_query'       => array(
                                                        array(
                                                            'taxonomy' => 'dlm_download_category',
                                                            'field'    => 'slug',
                                                            'terms'    => 'diversos',
                                                        )
                                                    ) ) );

                                while ( $downloadsDiversos->have_posts() ) : $downloadsDiversos->the_post();

                                $capa = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                                $capa = $capa[0];

                                foreach (rwmb_meta('Tatigodoy_download_arquivo', 'type=file') as $arquivo) {
                                    $arquivoDownload = $arquivo['url'];

                                }

                                global $post;

                                $downloadArquivo = new DLM_Download( $post->ID );
                                $link = $downloadArquivo->get_the_download_link();
                            ?>

                            <div class="item text-center">
                                <a style="display: block;" data-arquivo="<?php echo $link; ?>" data-nome="<?php echo get_the_title(); ?>" title="Clique para baixar <?php echo get_the_title(); ?>" class="acaoDownload"><img src="<?php echo $capa; ?>" class="acenter-block"></a>
                                <strong class="pdf"></strong>
                                <span><?php echo get_the_title(); ?></span>
                            </div>

                            <?php endwhile; ?>

                        </div>

                    </div>

                    <div class="col-md-1">
                        <button class="navegacaoCarrosselF hidden-xs"></button>
                    </div>

                </div>

            </div>

            <div class="dashed container"></div>

        </div>

    </div>

    <!-- SCRIPT ADICIONAL MAILPOET -->
    <script type="text/javascript">

        (function($) {

            // AÇÃO DE DOWNLOAD
            $('.acaoDownload').click(function(e){

                e.preventDefault();

                var arquivo = $(this).attr('data-arquivo');

                $('#download-arquivo').val(arquivo);

                var nomeArquivo = $(this).attr('data-nome');

                $('#observacao').val(nomeArquivo);

                function writeCookie(name,value,days) {
                   var date, expires;
                   if (days) {
                       date = new Date();
                       date.setTime(date.getTime()+(days*24*60*60*1000));
                       expires = "; expires=" + date.toGMTString();
                           }else{
                       expires = "";
                   }
                   document.cookie = name + "=" + value + expires + "; path=/";
                }

                function readCookie(name) {
                   var i, c, ca, nameEQ = name + "=";
                   ca = document.cookie.split(';');
                   for(i=0;i < ca.length;i++) {
                       c = ca[i];
                       while (c.charAt(0)==' ') {
                           c = c.substring(1,c.length);
                       }
                       if (c.indexOf(nameEQ) == 0) {
                           return c.substring(nameEQ.length,c.length);
                       }
                   }
                   return '';
                }

                $('#form-wysija-html563a7bd2869e3-1').submit(function(){

                    setTimeout(function(){

                        if($('#form-wysija-html563a7bd2869e3-1 .formErrorContent').length > 0){

                        }else{

                            var checkModal = false;
                            writeCookie('modal', checkModal, 1);

                            window.open(
                              $('#download-arquivo').val(),
                              '_blank'
                            );
                        }

                    }, 1000);

                });


                if (!readCookie('modal')) {
                    $('#modalCadastro').modal('show');
                }else{
                    window.open(
                        $('#download-arquivo').val(),
                        '_blank'
                    );
                }

                // if(localStorage.getItem('download-email') == '' || !localStorage.getItem('download-email')){

                //     $('#modalCadastro').modal('show');

                // }else{

                //     window.open(
                //       $('#download-arquivo').val(),
                //       '_blank'
                //     );

                // }

            });

            // NO CADASTRO
            // $('#form-wysija-html563a7bd2869e3-1').submit(function(){

            //     setTimeout(function(){

            //         if($('#form-wysija-html563a7bd2869e3-1 .formErrorContent').length > 0){

            //         }else{

            //             localStorage.setItem('download-email',$('#form-validation-field-0').val());

            //             window.open(
            //               $('#download-arquivo').val(),
            //               '_blank'
            //             );
            //         }

            //     }, 1000);

            // });

        })(jQuery);

    </script>

<?php get_footer(); ?>
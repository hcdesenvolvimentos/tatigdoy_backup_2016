<?php
/**
 * Template Name: Estante Virtual
 * Description: 
 *
 * @package Tatigodoy
 */

get_header();

?>

    <style type="text/css">

        /* PÁGINA ESTANTE VIRTUAL */
        .pg-estante .titulo-pagina{
            padding: 0 50px;
        }

        @media screen and (max-width: 992px) {

        .pg-estante .titulo-pagina{
            padding: 0 5px;
        }

        }
            .pg-estante .titulo{
                background: #84d1d4;
                padding: 6px 0;
                border-bottom-right-radius: 8px; 
                border-bottom-left-radius: 8px; 
                min-height: 50px;
                margin-bottom: 40px;
                max-width: 606px;
                clear: both;
                float: none;
                margin: 0 auto 40px auto;
            }

                @media screen and (max-width: 992px) {

                .pg-estante .titulo{
                    border-radius: 8px;
                }
                .navegacaoCarrosselTras,
                .navegacaoCarrosselFrente{
                    display: none;
                }

                }   

                .pg-estante .titulo span{
                    color: #d7597d;
                    font-size: 18px;
                    vertical-align: -webkit-baseline-middle;
                    padding: 0 5px;         
                }
                    .pg-estante .titulo span strong{
                        font-size: 24px;
                        color: #fff;
                        text-transform: uppercase;
                        font-weight: normal;
                        vertical-align: middle;
                        display: inline-block;
                            transform: scale(0.8,1.3);
                            -webkit-transform:scale(0.8,1.3); 
                        -moz-transform:scale(0.8,1.3); 
                        -ms-transform:scale(0.8,1.3); 
                        -o-transform:scale(0.8,1.3);
                    }
                    .pg-estante .titulo span i{
                        font-size: 34px;
                        vertical-align: middle;
                        margin: 0 10px;
                    }

                .texto-slide{
                    font-size: 20px;
                    font-weight: 400;
                    color: #fff;
                    margin-top: -5px;
                }
        
        .fundo-area{
            min-height: 480px;
            background: url(<?php echo get_template_directory_uri(); ?>/img/fundo-slide.png) center center no-repeat;
            background-size: cover;
            position: relative; 
        }
            .titulo-area{
                background: url(<?php echo get_template_directory_uri(); ?>/img/fundo-titulo.png) center center no-repeat;
                width: 336px;
                height: 44px;
                margin: 40px auto 0 auto;
            }
                .titulo-area span{
                    color: #fff;
                    font-size: 18px;
                    line-height: 36px;
                }

        .navegacaoCarrosselTras{
            background: url(<?php echo get_template_directory_uri(); ?>/img/prev.png) center center no-repeat;
            height: 35px;
            width: 35px;
            outline: 0;
            border: none;
            position: absolute;
            top: 230px;

        }
        .navegacaoCarrosselTras:hover{
            background: url(<?php echo get_template_directory_uri(); ?>/img/prev-hover.png) center center no-repeat;
        }
        .navegacaoCarrosselFrente{
            background: url(<?php echo get_template_directory_uri(); ?>/img/next.png) center center no-repeat;
            height: 35px;
            width: 35px;
            outline: 0;
            border: none;
            position: absolute;
            top: 230px;
            margin-left: 10px;
        }
        .navegacaoCarrosselFrente:hover{
            background: url(<?php echo get_template_directory_uri(); ?>/img/next-hover.png) center center no-repeat;
        }

        .area-slide{
            background: #479ca0;
            min-height: 325px;
            margin-top: 20px;
            border-radius: 5px;
            border: 5px solid rgba(255, 255, 255, .5);
            -webkit-background-clip: padding-box; /* for Safari */
            background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */
            padding: 0 30px 10px 30px;
        }
            .texto-slide{
                font-size: 20px;
                font-weight: 400;
                color: #fff;
                margin-top: -5px;
            }
            .slide-livros{
                height: 175px;
                background: #82bcbf;
                border-radius: 5px;
                margin: 10px 0 0 0;
                padding: 10px;
            }
                .slide-livros .owl-carousel .owl-item img{
                    width: 109px;
                    height: 154px;
                    border: 1px solid #888b95;
                }
                .slide-arq .owl-carousel .owl-item img{
                    width: 128px;
                    height: 128px;
                    background: #fff;
                    border: 2px solid #fff;
                    -webkit-background-clip: padding-box; /* for Safari */
                    background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */
                    border-radius: 100px;
                    overflow: hidden;
                }
                .slide-livros .item span{
                    height: 50px;
                    width: 50px;
                    display: block;
                    background: url(<?php echo get_template_directory_uri(); ?>/img/novo.png) center center no-repeat !important;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                }
                .owl-nav{
                    display: none;
                }

        .dashed{
            margin: 5px auto;
            border-bottom: 2px dashed #6cb0b3;
        }

        .fundo-arq{
            min-height: 200px;
            background: #84d1d4;
        }
            .slide-arq{
                margin: 20px 0;
            }
                .slide-arq .item span{
                    font-size: 22px;
                    font-weight: 400;
                    color: #fff;
                }
                .item strong.doc{
                    height: 34px;
                    width: 36px;
                    display: block;
                    background: url(<?php echo get_template_directory_uri(); ?>/img/doc.png) center center no-repeat !important;
                    position: absolute;
                    bottom: 30px;
                    right: 30px;
                }
                .slide-arq .owl-controls{
                    display: none;
                }
            .navegacaoCarrosselT{
                background: url(<?php echo get_template_directory_uri(); ?>/img/prev-a.png) center center no-repeat;
                height: 35px;
                width: 35px;
                outline: 0;
                border: none;
                position: absolute;
                top: 80px;
            }
            .navegacaoCarrosselT:hover{
                background: url(<?php echo get_template_directory_uri(); ?>/img/prev-a-hover.png) center center no-repeat;
            }
            .navegacaoCarrosselF{
                background: url(<?php echo get_template_directory_uri(); ?>/img/next-a.png) center center no-repeat;
                height: 35px;
                width: 35px;
                outline: 0;
                border: none;
                position: absolute;
                top: 80px;
                margin-left: 10px;
            }
            .navegacaoCarrosselF:hover{
                background: url(<?php echo get_template_directory_uri(); ?>/img/next-a-hover.png) center center no-repeat;
            }

    </style>

    <!-- PÁGINA ESTANTE VIRTUAL -->
    <div class="pg-estante">

        <div class="row correcao-y">
           <div class="titulo-pagina col-md-offset-2 col-md-8 col-sm-12">
               <div class="titulo text-center">
                   <span><strong>Estante virtual</strong> - Confira nossa área de downloads <i class="fa fa-cloud-download"></i></span>
               </div>
           </div>
        </div>

        <!-- E-BOOKS -->
        <div class="row">
            
            <div class="fundo-area container correcao-x">
                
                <div class="titulo-area text-center">
                    <span>Download de E-books</span>
                </div>

                <div class="col-md-1 text-center">
                    <button class="navegacaoCarrosselTras"></i></button>
                </div>
                <div class="area-slide col-md-10">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/ebookdown.png">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 texto-slide text-center">
                            <span>Baixe agora mesmo <strong>gratuitamente</strong> e-books exclusivos!</span>
                        </div>
                        <div class="slide-livros col-md-12">                            
                            <div id="carrossel" class="owl-carousel">

                                <?php
                                    
                                    $ebooks = new WP_Query( array( 'post_type' => 'ebooks', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
                                    
                                    while ( $ebooks->have_posts() ) : $ebooks->the_post(); 
                                    
                                    $capa = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                                    $capa = $capa[0]; 

                                    foreach (rwmb_meta('Tatigodoy_ebook_arquivo', 'type=file') as $arquivo) {
                                        $arquivoEbook = $arquivo['url'];
                                    }  

                                ?>

                                <div class="item text-center">
                                    <a href="<?php echo $arquivoEbook; ?>" target="_blank" title="Clique para baixar <?php echo get_the_title(); ?>"><img src="<?php echo $capa; ?>"  class="center-block"/></a>
                                </div>

                                <?php endwhile; ?>

                            </div>                            
                        </div>
                    </div>                    
                </div>
                <div class="col-md-1">
                    <button class="navegacaoCarrosselFrente"></button>
                </div>

            </div>

            <div class="dashed container"></div>

        </div>

        <!-- DOWNLOADS -->
        <div class="row correcao-y">
            
            <div class="titulo-area text-center">
                <span>Downloads diversos</span>
            </div>

            <div class="dashed container top10"></div>

            <div class="fundo-arq container">
                
                <div class="row">

                    <div class="col-md-1 text-center">
                        <button class="navegacaoCarrosselT hidden-xs"></button>
                    </div>

                    <div class="slide-arq col-md-10">
                        
                        <div id="carrossel-2" class="owl-carousel">

                            <?php
                                
                                $downloads = new WP_Query( array( 'post_type' => 'downloads', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
                                
                                while ( $downloads->have_posts() ) : $downloads->the_post(); 
                                
                                $capa = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
                                $capa = $capa[0]; 

                                foreach (rwmb_meta('Tatigodoy_download_arquivo', 'type=file') as $arquivo) {
                                    $arquivoDownload = $arquivo['url'];
                                }  

                            ?>

                            <div class="item text-center">
                                <a href="<?php echo $arquivoDownload; ?>" target="_blank" title="Clique para baixar <?php echo get_the_title(); ?>"><img src="<?php echo $capa; ?>" class="center-block"></a>
                                <strong class="pdf"></strong>
                                <span><?php echo get_the_title(); ?></span>
                            </div>

                            <?php endwhile; ?>
     
                        </div>

                    </div>

                    <div class="col-md-1">
                        <button class="navegacaoCarrosselF hidden-xs"></button>
                    </div>

                </div>

            </div>

            <div class="dashed container"></div>

        </div>

    </div>

<?php get_footer(); ?>
<?php
/**
 * Template Name: Encontre uma PO
 * Description: 
 *
 * @package Tatigodoy
 */

get_header();

?>
    <!-- PÁGINA ENCONTRE UMA PERSONAL ORGANIZER -->
    <div class="pg-po container correcao-xy">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="imagem text-center">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/ferramentas.png" class="img-responsive">
                    
                </div>
                <div class="row">
                    <div class="fundo col-md-offset-1 col-md-10">
                        <div class="titulo text-center">
                            <span>Página em Construção</span>
                        </div>
                    </div>
                </div>
                
                <div class="texto text-center">
                    <span>Obrigado por visitar nosso site.<br>Nós estamos construindo essa página.<br>Aguarde novidades em breve....</span>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
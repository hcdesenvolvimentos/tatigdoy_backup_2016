<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tatigodoy
 */

get_header(); ?>

	<div class="pg pg-blog" style="display: block;">
		<div class="row">
			
			<div class="container">	

				<div class="col-md-9">

				<div class="cab-informacoes">
					<?php the_archive_title( '<span>', '</span>' ); ?>
				</div>

				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); 

					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
					$url = $thumb['0'];

					?>

						<article class="publicacoes">
							<div class="row informacoes">
								<div class="col-md-6">
									<div class="titulo">
										<a href="<?php the_permalink(); ?>"><h2><?php echo get_the_title(); ?></h2></a>
									</div>
								</div>
								<div class="col-md-6">		
									<div class="data">
										<span><?php echo  mysql2date('d', $post->post_date); ?> <?php echo  strtoupper(mysql2date('M', $post->post_date)); ?> <strong><?php echo mysql2date('Y', $post->post_date); ?></strong></span>
									</div>
								</div>
							</div>
							<div class="imagem-detaque">
								<a href="<?php the_permalink(); ?>"><figure style="background: url(<?php echo $url; ?>); background-size: cover; background-position: center">
								</figure></a>
							</div>
							<p><?php echo get_the_excerpt(); ?></p>
							<div class="row rodape-post">
								<div class="col-md-6">
									<div class="ir-postagem">	
										<a href="<?php the_permalink(); ?>">continuar lendo <b> ></b> </a>
									</div>	
								</div>
								<div class="col-md-6">
									<div class="comentario">	
										<!-- <a href="<?php the_permalink(); ?>"><?php $comentarios = wp_count_comments($post->ID); echo $comentarios->total_comments; ?> comentários deixe seu comentário</a> -->
									</div>
								</div>
							</div>
						</article>

					<?php endwhile; ?>

					<?php if (function_exists("pagination")) {
					    pagination($additional_loop->max_num_pages);
					} ?>

				<?php else : ?>

					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; ?>

				</div>

				<?php get_sidebar(); ?>

			</div>
		</div>
	</div>

<?php get_footer(); ?>

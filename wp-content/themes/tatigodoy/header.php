<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Tatigodoy
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '667705660008323', {
em: 'insert_email_variable,'
});
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=667705660008323&ev=PageView&noscript=1"
/></noscript>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!-- TOPO -->
	<div class="row topo">

		<div class="container">
		
			<!-- REDES SOCIAIS -->
			<div class="col-md-1"></div>
				<div class="col-md-3">
					<div class="redes-sociais-cima">
						<ul>
							<li style="background: #e4e4e4; border-radius: 40px; padding-top: 13px; text-transform: uppercase; font-size: .8em;"><a href="/" title="Voltar a página inicial" style="text-indent: -35px; color: #0;">home »</a></li>
							<li class="facebook"><a href="https://www.facebook.com/FacilitaOrganizacao" title="Curta nossa fanpage no Facebook"></a></li>
							
							<li class="instagram"><a href="https://instagram.com/tatigodoyfacilita/" title="Siga-nos no Instagram"></a></li>
							
							<li class="youtube"><a href="https://www.youtube.com/channel/UCBlBXUL1EuSFQX_u1LJ_yNw/feed" title="Acompanhe-nos no Youtube"></a></li>
							
							<li class="linkedin"><a href="https://www.linkedin.com/in/tatigodoy" title="Acompanhe nosso Linkedin"></a></li>
							
							<li class="pinterest"><a href="https://www.pinterest.com/tatifacilita/" title="Acompanhe nosso Pinterest"></a></li>
						</ul>	
					</div>
				</div>
			
			<!-- LOGOTIPO -->
			<div class="col-md-4">
				<div class="logo">
					<h1><a href="<?php echo home_url('/'); ?>" title="Nome do projeto">blog tati godoy</a></h1>
				</div>
			</div>
			
			<!--BUSCAR-->
			<div class="col-md-4">
				<div class="buscar">

					<form method="get" id="searchform" action="<?php echo home_url('/'); ?>" style="padding: 10px;">
						<input type="text" class="field" name="s" id="s" placeholder="O que você procura?" style="padding: 5px;">
						<input type="submit" class="btn" name="submit" id="searchsubmit" value="Buscar" style="display: none;">
					</form>

				</div>
			</div>
		</div>
	
	</div>	

	<!-- MENU PRINCIPAL -->
	<div class="row">
		<div class="container">
			<div class="col-md-12">
			
				<nav class="menu-principal hidden-sm hidden-xs">
					<ul>

						<li class="menu-pai menu-roxo">
							<a href="<?php echo home_url('/tati'); ?>" title="Ir para página inicial" class="link-ativo">Tati Godoy</a>
						</li>
						
						<li class="menu-pai menu-rosa">
							<a href="<?php echo home_url('/organizacoes'); ?>" title="">Organização é Vida </a>
							<ul>
								<li><b><a href="<?php echo home_url('/estante-virtual/'); ?>" title="Download de E-books e Outros">Estante Virtual</a></b></li>
								<li><a href="<?php echo home_url('/organizacoes/organizacao-pessoal/'); ?>" title="">Organização Pessoal</a></li>
								<li><a href="<?php echo home_url('/organizacoes/rotina-domestica-organizacao/'); ?>" title="">Rotina Doméstica</a></li>
								<li><a href="<?php echo home_url('/organizacoes/organizando-a-mente/'); ?>" title="">Organizando a Mente</a></li>
								<li><a href="<?php echo home_url('/organizacoes/organizando-os-ambientes/'); ?>" title="">Organizando os Ambientes</a></li>
								<li><a href="<?php echo home_url('/organizacoes/organizando-seu-tempo/'); ?>" title="">Organizando seu Tempo</a></li>
								<li><a href="<?php echo home_url('/organizacoes/organizando-a-vida-financeira/'); ?>" title="">Organizando a Vida Financeira</a></li>
								<li><a href="<?php echo home_url('/organizacoes/personal-organizer-organizacao/'); ?>" title="">Personal Organizer</a></li>
							</ul>
						</li>
						
						<li class="menu-pai menu-laranja">
							<a href="<?php echo home_url('/limpezas'); ?>" title="">Limpeza Fácil</a>
							<ul>
								<li><a href="<?php echo home_url('/limpezas/limpeza-pos-obra/'); ?>" title="">Limpeza Pós-Obra</a></li>
								<li><a href="<?php echo home_url('/limpezas/limpeza-residencial/'); ?>" title="">Limpeza Residencial</a></li>
							</ul>
						</li>

						<li class="menu-pai menu-amarelo">
							<a href="<?php echo home_url('/cursos'); ?>" title="">Curso de Personal Organizer</a>
							<ul>
								<li><a href="<?php echo home_url('cursos/agenda/'); ?>" title="">Agenda de Cursos</a></li>
								<?php /*?><li><a href="<?php echo home_url('/cursos/conheca-nossos-cursos/'); ?>" title="">Conheça nossos cursos</a></li><?php */?>
								<li><a href="<?php echo home_url('/cursos/cursos-realizados/'); ?>" title="">Cursos já realizados</a></li>
							</ul>
						</li>

						<li class="menu-pai menu-verde">
							<a href="<?php echo home_url('/bem-estar'); ?>" title="">Bem Estar</a>
							<ul>
								<li><a href="<?php echo home_url('/bem-estar/vida-saudavel/'); ?>" title="">Vida Saudável</a></li>
								<li><a href="<?php echo home_url('/bem-estar/equilibrio-espiritual/'); ?>" title="">Equilíbrio Espiritual</a></li>
								<li><a href="<?php echo home_url('/bem-estar/bem-estar-em-familia/'); ?>" title="">Bem-estar em Família</a></li>
							</ul>
						</li>

						<li class="menu-azul"><a href="<?php echo home_url('/pos'); ?>" title="">Encontre uma P. O.</a></li>
						
						<li class="menu-azul-escuro"><a href="<?php echo home_url('/videos-e-depoimentos'); ?>" title="">Vídeos e Depoimentos</a></li>
					
					</ul>
				</nav>
			</div>
		</div>
	</div>

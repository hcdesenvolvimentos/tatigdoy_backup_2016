<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tatigodoy
 */

get_header(); ?>

	<!-- SLIDER -->
	<div class="row carrossel" style =" margin-top: 3px; background-image: url(<?php echo get_template_directory_uri(); ?>/img/fundo2.png); background-size: cover; background-position: center">
		<div class="container">
			<div class="col-md-7">
				<div class="hidden-sm hidden-xs">
					<span>Não existe falta de espaço, o que existe é falta de criatividade.</span>
					<strong>by Tati Godoy</strong>
				</div>
			</div>
			<div class="col-md-5">
				<img src="<?php echo get_template_directory_uri(); ?>/img/tati_foto.png" />
			</div>
		</div>
	</div>

	<div class="pg pg-blog" style="display: block;">
		<div class="row">
			
			<div class="container">	

				<div class="col-md-9">

				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); 

					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
					$url = $thumb['0'];

					?>

						<article class="publicacoes">
							<div class="row informacoes">
								<div class="col-md-6">
									<div class="titulo">
										<a href="<?php the_permalink(); ?>"><h2><?php echo get_the_title(); ?></h2></a>
									</div>
								</div>
								<div class="col-md-6">		
									<div class="data">
										<span><?php echo  mysql2date('d', $post->post_date); ?> <?php echo  strtoupper(mysql2date('M', $post->post_date)); ?> <strong><?php echo mysql2date('Y', $post->post_date); ?></strong></span>
									</div>
								</div>
							</div>
							<div class="imagem-detaque">
								<a href="<?php the_permalink(); ?>"><figure style="background: url(<?php echo $url; ?>); background-size: cover; background-position: center">
								</figure></a>
							</div>
							<p><?php echo get_the_excerpt(); ?></p>
							<div class="row rodape-post">
								<div class="col-md-6">
									<div class="ir-postagem">	
										<a href="<?php the_permalink(); ?>">continuar lendo <b> ></b> </a>
									</div>	
								</div>
								<div class="col-md-6">
									<div class="comentario">	
										<!-- <a href="<?php the_permalink(); ?>"><?php $comentarios = wp_count_comments($post->ID); echo $comentarios->total_comments; ?> comentários deixe seu comentário</a> -->
									</div>
								</div>
							</div>
						</article>

					<?php endwhile; ?>

					<?php if (function_exists("pagination")) {
					    pagination($additional_loop->max_num_pages);
					} ?>

				<?php else : ?>

					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; ?>

				</div>

				<?php get_sidebar(); ?>

			</div>
		</div>
	</div>

<?php get_footer(); ?>

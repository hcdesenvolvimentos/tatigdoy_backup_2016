<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Tatigodoy
 */

?>

	<!-- RODAPÉ -->
	<footer class="rodape">
		
		<div class="container">
		
				
					<div class="col-md-3">
						<div class="logo">
							<img src="<?php echo get_template_directory_uri(); ?>/img/img_logo.png"/>
						</div>
					</div>
					<div class="col-md-2">
						<div class="contato">
							<a href="<?php echo home_url('/tati'); ?>"><span>entre em contato</span></a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="redes_sociais-baixo">
							<ul class="redes-sociais">
								<li class="facebook"><a href="https://www.facebook.com/FacilitaOrganizacao" title="Curta nossa fanpage no Facebook"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_facebook_baixo.png"></a></li>
								<li class="instagram"><a href="https://instagram.com/tatigodoyfacilita/" title="Siga-nos no Instagram"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_instagram_baixo.png"></a></li>
								<li class="youtube"><a href="https://www.youtube.com/channel/UCBlBXUL1EuSFQX_u1LJ_yNw/feed" title="Acompanhe-nos no Youtube"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_youtube_baixo.png"></a></li>
								<li class="linkedin"><a href="//linkedin.com" title="Acompanhe nosso Linkedin"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_linkedin_baixo.png"></a></li>
								<li class="pinterest"><a href="https://pt.pinterest.com/tatigodoy/" title="Acompanhe nosso Pinterest"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_pinterest_baixo.png"></a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3">
						<!-- <div class="newsletter">
							<label class="naoalinhado">
								<input type="text" placeholder="receba nossa newsletter">
							</label>
							<a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/enviar_rodape.png"></a>
						</div> -->
						<div class="curtir-facebook">
							<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FFacilitaOrganizacao&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=267318246713012" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:220px; background: #fff;" allowtransparency="true"></iframe>
						</div>
					</div>
				</div>
		</footer>
	
	<!-- COPYRIGHT -->
	<div class="row copyright">
		<div class="container">
			<div class="texto">
				<small>© Copyright Blog Tati Godoy - Todos os direitos reservados 2015</small>
			</div>
		</div>
	
	</div>

<?php wp_footer(); ?>

</body>
</html>

<?php
/**
 * The template for displaying all single posts.
 *
 * @package Tatigodoy
 */

get_header(); ?>
<script>
fbq('track', 'ViewContent', {
value: 3.50,
currency: 'USD'
});
</script>
	<div class="pg pg-blog" style="display: block;">
		<div class="row">
			
			<div class="container">	

				<div class="col-md-9">

					<?php while ( have_posts() ) : the_post(); 

					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
					$url = $thumb['0'];

					?>

						<article class="publicacoes">
							<div class="row informacoes">
								<div class="col-md-6">
									<div class="titulo">
										<h2><?php echo get_the_title(); ?></h2>
									</div>
								</div>
								<div class="col-md-6">		
									<div class="data">
										<span><?php echo  mysql2date('d', $post->post_date); ?> <?php echo  strtoupper(mysql2date('M', $post->post_date)); ?> <strong><?php echo mysql2date('Y', $post->post_date); ?></strong></span>
									</div>
								</div>
							</div>
							<div class="imagem-detaque">
								<figure style="background: url(<?php echo $url; ?>); background-size: cover; background-position: center">
								</figure>
							</div>
							<?php echo the_content(); ?>
						</article>

					<?php endwhile; ?>

					<?php the_posts_navigation(); ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>

				</div>

				<?php get_sidebar(); ?>

			</div>
		</div>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

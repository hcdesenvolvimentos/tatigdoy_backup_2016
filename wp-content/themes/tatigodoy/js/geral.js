
	(function($) {

		var isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};

		// MENU MOBILE
		$('.menu-principal').slicknav({
			'label' : 'MENU'
		});

		// MENU SUPERIOR
		$( ".menu-pai" )
		  .mouseenter(function() {
		    $('ul',this).fadeIn();
		  })
		  .mouseleave(function() {
		    $('ul',this).hide();
		});

		/* GALERIA DE FOTOS */
		/*$('.galeria').iLightBox({
			skin: 'metro-white',
			path: 'horizontal',
			controls: {
				arrows: 1,
				slideshow: 1
			}
		});*/

 		// PÁGINA: ESTANTE VIRTUAL

 			/*****************************************
            *  CARROSSEL DE E-BOOKS
            *****************************************/
            $("#carrossel").owlCarousel({
                dots: false,
                nav: false,
                responsiveClass:true,
                responsive:{
                        0:{
                            loop: false,
                            items:1
                        },
                        600:{
                            items:3
                        },
                        1000:{
                            items:5
                        }
                    }

            });

            var carrossel = $("#carrossel").data('owlCarousel');

            $('.navegacaoCarrosselTras').click(function(){ carrossel.prev(); });
            $('.navegacaoCarrosselFrente').click(function(){ carrossel.next(); });

            /*****************************************
            *  CARROSSEL DE DOWNLOADS
            *****************************************/
            $("#carrossel-2").owlCarousel({
                dots: false,
                nav: false,
                responsiveClass:true,
                responsive:{
                        0:{
                            loop: false,
                            items:1
                        },
                        600:{
                            items:3
                        },
                        1000:{
                            items:5
                        }
                    },
                loop: true,
		        mouseDrag: true,
		        autoplay:true,
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    animateOut: 'fadeOut',
			    smartSpeed: 450,
			    autoplaySpeed: 2000,
            });

            var carrossel2 = $("#carrossel-2").data('owlCarousel');

            $('.navegacaoCarrosselT').click(function(){ carrossel2.prev(); });
            $('.navegacaoCarrosselF').click(function(){ carrossel2.next(); });

            // PÁGINA: ENCONTRE UMA PO

                /*****************************************
                *  DINÂMICA DE ABAS
                *****************************************/
                $('#nav').on('click', 'li > a', function() {

                    if (!$(this).hasClass('active')) {

                        $('#nav .is-open').removeClass('is-open').hide();

                        $(this).next().toggleClass('is-open').toggle();

                        $('#nav').find('.active').removeClass('active');
                        $(this).addClass('active');

                    }else {

                        $('#nav .is-open').removeClass('is-open').hide();
                        $(this).removeClass('active');

                    }

                });

                // SIMULA CLIQUE NA PRIMEIRA ABA
                $('#nav li:first-child a').trigger('click');

                // EXIBE CONTEÚDO DA ABA 1
                $('#aba-encontre').click(function(e){

                    e.preventDefault();

                    $('.lista').show();
                    $('.cadastre-se').hide();

                });

                // EXIBE CONTEÚDO DA ABA 2
                $('#aba-cadastre').click(function(e){

                    e.preventDefault();

                    $('.cadastre-se').show();
                    $('.lista').hide();

                });

            // FORMULÁRIO DE CADASTRO PO
            $('#poTelefone').mask('(99) 9999-9999');

            $('#formulario-cadastro-po input[type=submit]').click(function(e){

                e.preventDefault();

                // VALIDAÇÃO DO FORMULÁRIO DE CADASTRO
                $('#formulario-cadastro-po').validate({
                    rules:{
                        poNome: {
                            required: true
                        },
                        poEmail: {
                            required: true
                        },
                        poUf: {
                            required: true
                        },
                        poCidade: {
                            required: true
                        },
                        poRegiao: {
                            required: true
                        }
                    },
                    messages:{
                        poNome: {
                            required: ""
                        },
                        poEmail: {
                            required: ""
                        },
                        poUf: {
                            required: ""
                        },
                        poCidade: {
                            required: ""
                        },
                        poRegiao: {
                            required: ""
                        }
                    }
                });

                if($("#formulario-cadastro-po").valid()){

                    $("#formulario-cadastro-po").submit();

                }

            });

	})(jQuery);

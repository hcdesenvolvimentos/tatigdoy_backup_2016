<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Tatigodoy
 */

// if ( ! is_active_sidebar( 'sidebar-1' ) ) {
// 	return;
// }
?>

	<!-- MENU LATERAL -->
	<div class="col-md-3">
		<div class="menu-lateral">
			<!-- <div class="newsletter">
				<label class="naoalinhado">
					<input type="text" placeholder="receba nossa newsletter">
				</label>
				<a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/enviar_cima.png"></a>
			</div> -->
			<div class="curso">
				<a href="http://www.tatigodoy.com.br/formulario-de-cadastro/"><img src="<?php echo get_template_directory_uri(); ?>/img/bt_curso.png"></a>
			</div>
			<div class="youtube">
				<a href="https://www.youtube.com/channel/UCBlBXUL1EuSFQX_u1LJ_yNw/feed" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/bt_youtube.png"></a>
			</div>
			<div class="youtube">
				<a href="http://www.tatigodoy.com.br/estante-virtual"><img src="<?php echo get_template_directory_uri(); ?>/img/bt_estante.png"></a>
			</div>
			<div class="instagram">
				<img src="<?php echo get_template_directory_uri(); ?>/img/ico_instagram_lateral.png">
				<span>instagram</span>
				<strong>@tatigodoyfacilita</strong>
                <iframe src="//widgets-code.websta.me/w/df1409d7c8c5?ck=MjAxNy0wOS0yOVQwMjo1MDozNC4zMjNa" allowtransparency="true" frameborder="0" scrolling="no" style="border:none;overflow:hidden;width:246px; height: 246px"></iframe>
			</div>
			<div class="anuncios">
				<div class="icone">
					<img src="<?php echo get_template_directory_uri(); ?>/img/ico_anuncio_lateral.png">
					<span>anúncios</span>
				</div>
				<div class="banners">

					<?php if(function_exists('drawAdsPlace')) drawAdsPlace(array('id' => 1), true); ?>
					
					<!-- <a href="//www.multiusoype.com.br" target="_blank"><img src="http://www.tatigodoy.com.br/wp-content/uploads/2015/07/230x250.png" /></a>
					<?php echo do_shortcode('[cm_ad_changer campaign_id="1"]'); ?> -->
					<br /><br />
					<a href="http://www.imobiliariacolmeia.com.br/" target="_blank"><img src="http://tatigodoy.com.br/wp-content/uploads/2013/09/ANUNCIO_BLOG.png" /></a>
					<br /><br />
					<a href="http://www.napo.net/" target="_blank"><img src="http://www.tatigodoy.com.br/wp-content/uploads/2016/07/Logotipo-Napo-JPG-e1467749260642.jpg" /></a>
					<br /><br />
					<a href="http://www.facilitalimpeza.com.br/" target="_blank"><img src="http://www.tatigodoy.com.br/wp-content/uploads/2015/07/ANUNCIO_BLOG_facilita.png" /></a>
					<br /><br />
					<a href="http://www.anpop.com.br/" target="_blank"><img src="http://www.tatigodoy.com.br/wp-content/uploads/2015/04/anpopselo.png" /></a>
					<br /><br /><br />
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- Lateral do Blog -->
					<ins class="adsbygoogle"
					     style="display:inline-block;width:250px;height:250px"
					     data-ad-client="ca-pub-3226668559752033"
					     data-ad-slot="7122694684"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
			</div>
			
		</div>
	
	</div>

<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tatigodoy
 */

	// CADASTRO DE PERSONAL ORGANIZER
	if($_SERVER['REQUEST_METHOD'] == 'POST'){

		if(isset($_POST['poCadastro'])){

			$poNome				= $_POST['poNome'];		
			$poEmail			= $_POST['poEmail'];		
			$poFacebook			= $_POST['poFacebook'];	
			$poTwitter			= $_POST['poTwitter'];	 	
			$poTelefone			= $_POST['poTelefone'];		
			$poUf				= $_POST['poUf'];				
			$poCidade			= $_POST['poCidade'];
			$poRegiao			= $_POST['poRegiao'];		
			$poCertificacao		= $_POST['poCertificacao'];

			$cadastroPo = array(
		        'post_title'    => $poNome,
		        'post_content'  => '',
		        'post_status'   => 'draft',
		        'post_type' 	=> 'pos'
		    );

	    	$pid = wp_insert_post($cadastroPo);

		    // METABOXES
		    add_post_meta($pid, 'Tatigodoy_po_nome',    			$poNome,      	true);
		    add_post_meta($pid, 'Tatigodoy_po_email',    			$poEmail,       true);
		    add_post_meta($pid, 'Tatigodoy_po_facebook',    		$poFacebook,    true);
		    add_post_meta($pid, 'Tatigodoy_po_twitter',    			$poTwitter,     true);
		    add_post_meta($pid, 'Tatigodoy_po_telefone',    		$poTelefone,    true);
		    add_post_meta($pid, 'Tatigodoy_po_uf',    				$poUf,      	true);
		    add_post_meta($pid, 'Tatigodoy_po_cidade',    			$poCidade,      true);
		    add_post_meta($pid, 'Tatigodoy_po_regiao_atendimento',	$poRegiao,      true);
		    add_post_meta($pid, 'Tatigodoy_po_certificacao',    	$poCertificacao,true);

		    move_uploaded_file($_FILES["poFoto"]["tmp_name"],WP_CONTENT_DIR .'/uploads/pos/'.basename($_FILES['poFoto']['name']));
			
			$arquivoNome  = WP_CONTENT_DIR .'/uploads/pos/' . basename($_FILES['poFoto']['name']);
			$arquivoTipo  = wp_check_filetype( basename( $arquivoNome ), null );
			$arquivoTipos = array('png','jpg','jpeg','gif');

			if(in_array($arquivoTipo['ext'], $arquivoTipos)){

				require_once( ABSPATH . 'wp-admin/includes/image.php' );

				$diretorioUploads = wp_upload_dir();

				$arquivoDetalhes = array(
					'guid'           => $diretorioUploads['url'] . '/' . basename( $arquivoNome ), 
					'post_mime_type' => $arquivoTipo['type'],
					'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $arquivoNome ) ),
					'post_content'   => '',
					'post_status'    => 'inherit'
				);

				$arquivoCarregadoId 	= wp_insert_attachment($arquivoDetalhes, $arquivoNome, $pid);
				$arquivoCarregadoMeta	= wp_generate_attachment_metadata($arquivoCarregadoId,$arquivoNome);

				wp_update_attachment_metadata($arquivoCarregadoId, $arquivoCarregadoMeta);

				add_post_meta($pid, '_thumbnail_id', $arquivoCarregadoId, true);

			}

		    if($pid > 0){ $cadastroRealizado = true; }

		}

	}

	get_header(); 

	?>

	<style type="text/css">
	
		/* ABAS */
		#nav {
			margin: 24px auto;
			width: 100%;
			padding: 0;
			border-radius: 10px;
			overflow: hidden;
		}
			#nav li {
				list-style: none;
				clear: both;
			}
			#nav li:first {
			  border-radius: 2px 2px 0 0;
			  outline: 3px solid red;
			}
				#nav li > a {
					display: block;
					padding: 10px 15px;		  
					background-size: 100%;
					background: #c14668;
					text-decoration: none;
					color: #fff;
				}
				#nav li > a:hover, #nav li > a.active {		  
					background-size: 100%;
					background: #84d1d4;
					color: #fff;
				}
				#nav li > a:hover{
					background: #309498;
				}
				#nav li > a.active {
					color: #fff;
					background: #84d1d4;
				}

				#nav section {
					width: 100%;
					color: #8298a0;
					display: none;
					float: left;		 
					background-size: 100%;
					background: #96e0e3;
					overflow: hidden;
					padding: 1% 3% 0 3%;
				}
				.no-js #nav section {
					display: block;
				}
					/* ABA DE REFINAMENTOS */
					.filtro select {
					    padding:3px;
					    margin: 0;
					    -webkit-border-radius:4px;
					    -moz-border-radius:4px;
					    border-radius:4px;
					    -webkit-box-shadow: 0 2px 0 #c14668, 0 -1px #fff inset;
					    -moz-box-shadow: 0 2px 0 #c14668, 0 -1px #fff inset;
					    box-shadow: 0 2px 0 #c14668, 0 -1px #fff inset;
					    background: #fff;
					    color:#c14668;
					    outline:none;
					    display: inline-block;
					    -webkit-appearance:none;
					    -moz-appearance:none;
					    appearance:none;
					    cursor:pointer;
					    border: 2px solid #c14668;
					}
					
					@media screen and (-webkit-min-device-pixel-ratio:0) {
						.filtro select {padding-right:18px}
					}

					.filtro label.estado {position:relative}
					.filtro label.estado:after {
					    content:'\f078';
					    font-family: FontAwesome;
					    font-size: 11px;
					    color:#c14668;
					    right:8px; 
					    top:8px;
					    padding:0 0 2px;
					    position:absolute;
					    pointer-events:none;
					}
					.filtro label.cidade{
						text-indent: -9999px;
					}
					.filtro input{
						padding: 4px 8px;
						margin: 0;
						-webkit-border-radius:4px;
					    -moz-border-radius:4px;
					    border-radius:4px;
					    background: #fff;
					    color:#c14668;
					    outline:none;
					    display: inline-block;
					    -webkit-appearance:none;
					    -moz-appearance:none;
					    appearance:none;
					    cursor:pointer;
					    border: 2px solid #c14668;
					}
					.filtro ::-webkit-input-placeholder { color: #b86581; }
					.filtro input[type=submit]{
						padding: 4px 20px;
						color: #fff;
						background: #c14668;
						position: relative;
						outline:none;
						display: inline-block;
						-webkit-appearance:none;
						-moz-appearance:none;
						appearance:none;
						cursor:pointer;
						border: 2px solid #c14668 !important;
						margin: 0!important;
					}
					.fa-input {
						font-family: FontAwesome, 'Roboto Condensed', sans-serif;
					}

					.resultado span{
						color: #c14668;
						font-size: 20px;
						text-transform: uppercase;
						line-height: 38px;
					} 

					/* ABA SOU UMA PO */
					.sou-po{
						background: #c54a6c!important;			
					}
						.sou-po span{
							color: #fff;
							font-size: 20px;
							line-height: 38px;
							font-weight: 300;
						}

					#nav section a {
						color: #cdd5d9;
						text-decoration: none;
					}

		@media only screen and (min-width: 30em) {

			#nav {
				width: 100%;
				position: relative;
				border: none;
				overflow: visible;
			}

			#nav li {
				display: inline;
			}
				#nav li a {
					display: inline-block;
					border-radius: 7px 7px 0 0;
					padding: 5px 15px;
				}
				#nav section {
					padding: 2% 2%;
					position: absolute;
					margin-top: -1px;
					left: 0;
				}

		}

		@media screen and (max-width: 479px){
		
			#nav {
				border-radius:0;
				text-align: center;
			}
				#nav li {
					border-bottom: 2px solid #fff;
				}

		}
			/* LISTAGEM DE POs */
			.container.lista{
				margin-top: 100px;
				padding: 0 40px 5px 40px;
			}

				.localidade{
					border-bottom: 1px dashed #847979;
				}
					.localidade span{
						color: #c14668;
						font-size: 20px;
						font-weight: 300;
					}

				.po{
					margin-top: 20px;
					padding: 20px 0 40px 0;
					border-bottom: 1px dashed #847979;
				}
					.po .foto{
						min-height: 167px;
						padding: 10px 0;
						border: 2px solid #c14668;
						border-radius: 5px;
						background-image: url(../img/gio.png) center center no-repeat;
						background-size: cover !important;
					}		
					.po .info{
						border: 2px solid #ecc7d1;
						border-radius: 5px;
						position: relative;
						padding: 5px 20px;
					}
						.po .nome{
							color: #309498;
							text-transform: uppercase;
							font-weight: 700;
							font-size: 22px;
						}
						.po .funcao{
							color: #c14668;
							text-transform: uppercase;
							font-weight: 700;
						}
						.po .contato{
							margin-top: 25px;
							color: #c14668;
							font-weight: 300;
							font-size: 18px;
						}
							.po .contato span{
								display: block;
							}
						.po .msg{
							padding: 40px 0;
						}
							.po .msg span{
								display: block;
								color: #c14668;
								font-weight: 300;
								font-size: 20px;
							}
							.po .msg a.btn {
								color: #fff;
								font-weight: 300;
								font-size: 16px;
								background: #c14668;
								text-transform: uppercase;
								border-radius: 5px;
								outline: 0!important;
								-webkit-box-shadow: -3px 3px 5px 0px rgba(50, 50, 50, 0.2);
								-moz-box-shadow:    -3px 3px 5px 0px rgba(50, 50, 50, 0.2);
								box-shadow:         -3px 3px 5px 0px rgba(50, 50, 50, 0.2);
							}
						.po .social{
							position: absolute;
						    right: -20px;
						    top: 36px;
						}
							.po .social a{
								display: block;
								width: 40px;
								height: 40px;
								background: #c14668;
								border-radius: 100px;
								margin: 5px 0;
								padding: 4px 0;
								font-size: 24px;
								color: #fff;
							}
								.po .social a:hover{
									background: #309498;					
								}
									.po .social a:hover i{
										-webkit-animation-name: thumb;
										-webkit-animation-duration: 500ms;
										-webkit-transform-origin:50% 50%;
										-webkit-animation-iteration-count: infinite;
										-webkit-animation-timing-function: linear;
									}
									@-webkit-keyframes thumb {
										0% { -webkit-transform: scale(1); }
										50% { -webkit-transform: scale(0.9); }
										100% { -webkit-transform: scale(1); }
									}

					/* PAGINAÇÂO */
					nav.paginacao{
						margin: 40px 0;
					}
						nav.paginacao a{
							display: inline-block;
							width: 32px;
							height: 47px;
							border: 1px solid #d7597d;
							border-radius: 3px;
							padding: 10.5px 0;
						}
							nav.paginacao a i{					
								color: #309498;
								font-size: 38px;
								vertical-align: middle;			
							}
							nav.paginacao #prev,
							nav.paginacao #next{
								border: none !important;
							}
						nav.paginacao ul{
							margin: 0;
							padding: 0 20px;
							display: inline-block;
						}
							nav.paginacao li{
								display: inline-block;
							}

								nav.paginacao li a{
									color: #309498;
									text-decoration: none;
									font-size: 18px;
								}
									nav.paginacao li a:hover{
										background: #309498;
										color: #fff;
									}
								nav.paginacao li.ativo a{
									background: #309498;
									color: #fff;
								}

			/* FORMULÁRIO DE CADASTRO DE PO */		
			.form fieldset {
				padding-top: 80px;
				border: none;
				margin: 0;
			}
			.form label{
				font-size: 22px;
				color: #c14668;
			}
			.form input,
			.form select {
				width: 100%;
				font-size: 14px;
				border: none;
				font-family: inherit;
				font-size: inherit;
				margin: 0;					
			}
			.form input{
				-webkit-appearance: none;
			}
			.form input:focus,
			.form select:focus {
			  outline: none;
			}
			.form input[type="text"],
			.form select,
			.form input[type="email"],
			.form input[type="tel"] {
				border: 1px solid #dcdcdc;
				padding: 12px 10px;
				border-radius: 3px;
				width: 100%;
				color: #b86581;
			}			
			.form input[type="submit"] {
				background: #c14668;
				border-radius: 3px;
				color: #fff;
				float: right;
				margin-top: 20px;
				padding: 10px 20px;
				text-transform: uppercase;
				cursor: pointer; 
			}
			.form input[type="submit"]:hover { background: #309498; }
			
			/*EXTRAS*/
			.correcao-x { 	padding: 0 20px; 	}
			.correcao-y { 	padding: 20px 0; 	}
			.correcao-xy{	padding: 20px; 		}
			.noPadding	{	padding: 0;			}
			.top10		{ 	padding-top: 10px;	}

	</style>

    <!-- ENCONTRE UMA PO -->
    <div class="pg-po" style="display: ;">
        
        <div class="enc-po container">
            
            <!-- FILTROS -->
            <div class="filtro rowcorrecao-xy">
                
                <!-- ABAS -->
                <ul id="nav" class="text-right">

                    <!-- ABA: ENCONTRE UMA PO -->
                    <li>
                        <a href="#" id="aba-encontre">ENCONTRE UMA P.O.</a>
                        <section>
                            <div class="col-md-6 text-left">
                                
                                <!-- FORMULÁRIO DE REFINAMENTO -->
                                <form method="post">

                                    <label class="estado">
                                        <select name="poFiltroUf">
                                            <option value=""> UF </option>
                                            <?php 

                                            	$ufs = array("AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapá","BA"=>"Bahia","CE"=>"Ceará","DF"=>"Distrito Federal","ES"=>"Espírito Santo","GO"=>"Goiás","MA"=>"Maranhão","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Pará","PB"=>"Paraíba","PR"=>"Paraná","PE"=>"Pernambuco","PI"=>"Piauí","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondônia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"São Paulo","TO"=>"Tocantins");

                                            	foreach ($ufs as $uf => $estado) {
                                            		
                                            		if(isset($_POST['poFiltroUf']) && $_POST['poFiltroUf'] == $uf){
                                            			echo '<option value="' . $uf . '" selected>' . $estado . '</option>';
                                            		}else {
                                            			echo '<option value="' . $uf . '">' . $estado . '</option>';
                                            		}
                                            	}

                                            ?>
                                        </select>
                                    </label>

                                    <input type="text" name="poFiltroCidade" placeholder="Cidade" value="<?php echo ($_POST['poFiltroCidade']) ? $_POST['poFiltroCidade'] : ''; ?>">

                                    <input type="submit" class="fa-input" value="Buscar &#xf002;">

                                </form>

                            </div>
                            <div class="col-md-6">

                               <div class="resultado text-center">

                               		<?php if($wp_query->post_count > 0): ?>
                                	<span>Resultados encontrados: <?php echo $wp_query->post_count; ?> resultados</span>
                               		<?php endif; ?>

                               </div>

                            </div>
                        </section>
                    </li>

                    <!-- ABA: SOU UMA PO -->
                    <li><a href="#" id="aba-cadastre">SOU UMA P.O.</a>
                        <section class="sou-po text-left">
                            <span><strong>Preencha</strong> o formulário abaixo e <strong>apareça aqui</strong>.</span>
                        </section>
                    </li>

                </ul>
            </div>

        </div>

        <!-- LISTAGEM DE POs -->
        <div class="container lista">

        	<?php if($cadastroRealizado): ?>
        	<div class="alert alert-success">
        		<h4>Obrigado por cadastrar-se, logo você aparecerá aqui!</h4>
        	</div>
        	<?php endif; ?>
            
            <div class="localidade">
                <span>Listagem de personal organizers</span>
            </div>

            <div class="alert alert-warning" style="margin-top: 10px; margin-bottom: 0;"><b>Atenção:</b> Esse é um espaço livre para personal organizers cadastrarem seus perfis. Nenhum profissional tem qualquer ligação profissional com a Facilita Limpeza ou com a Tati Godoy.
Não somos responsáveis pelos trabalhos executados.</div>

            <div class="row">

				<?php if ( have_posts() ) : ?>

				<div class="po col-md-12">

					<?php while ( have_posts() ) : the_post(); ?>

						<!-- PO -->
	                    <div class="row" style="margin: 10px 0;">

                    		<?php
							
							$thumb 	= wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
							$img 	= ($thumb['0']) != '' ? $thumb['0'] : 'http://www.tatigodoy.com.br/wp-content/themes/tatigodoy/img/img-padrao.png';
							
							?>

	                        <div class="col-md-2 foto" style="background: url('<?php echo $img; ?>') center center no-repeat;">
	                            
	                        </div>

	                        <div class="info col-md-10">
	                            
	                            <div class="row">
	                                <div class="col-md-8">
	                                    <div class="nome">
	                                        <span><?php echo get_the_title(); ?></span>
	                                    </div>
	                                    <div class="funcao">
	                                        <span>Personal Organizer</span>
	                                    </div>
	                                    <div class="contato">
	                                    	
	                                    	<?php if(!empty(rwmb_meta('Tatigodoy_po_telefone'))):?>
	                                        <span><i class="fa fa-phone"></i> <?php echo rwmb_meta('Tatigodoy_po_telefone'); ?></span>
	                                       	<?php endif; ?>

	                                       	<?php if(!empty(rwmb_meta('Tatigodoy_po_email'))):?>
	                                        <span><i class="fa fa-envelope"></i> <?php echo rwmb_meta('Tatigodoy_po_email'); ?></span>
	                                        <?php endif; ?>

	                                        <?php if(!empty(rwmb_meta('Tatigodoy_po_regiao_atendimento'))):?>
	                                        <span><i class="fa fa-map-marker"></i> Onde atende: <?php echo rwmb_meta('Tatigodoy_po_regiao_atendimento'); ?></span>
	                                    	<?php endif; ?>

	                                    </div>
	                                </div>
	                                <div class="msg col-md-4 text-center">
	                                    <span>entrar em contato com a P.O.</span>
	                                    <a href="mailto:<?php echo rwmb_meta('Tatigodoy_po_email'); ?>" target="_blank" class="btn">enviar mensagem</a>
	                                </div>
	                            </div>

	                            <div class="social">
	                            	
	                            	<?php if(!empty(rwmb_meta('Tatigodoy_po_facebook'))):?>
	                                <a href="//facebook.com/<?php echo rwmb_meta('Tatigodoy_po_facebook'); ?>" target="_blank" class="text-center"><i class="fa fa-facebook"></i></a>
	                                <?php endif; ?>

	                                <?php if(!empty(rwmb_meta('Tatigodoy_po_twitter'))):?>
	                                <a href="//twitter.com/<?php echo str_replace('@','',rwmb_meta('Tatigodoy_po_twitter')); ?>" target="_blank" class="text-center"><i class="fa fa-twitter"></i></a>
	                            	<?php endif; ?>	

	                            </div>

	                        </div>

	                    </div>

					<?php endwhile; ?>

				</div>
			
				<?php else: ?>

				<h4>Que pena, não encontramos nenhuma PO na localização buscada! :(</h4>

				<?php endif; ?>

                <!-- PAGINAÇÃO -->
                <nav class="col-md-12 text-center">
                	<?php pagination(); ?>
                </nav>

            </div>

        </div>

        <!-- CADASTRE-SE -->
        <div class="container cadastre-se" style="display: none;">
            
            <div class="form info">

                <!-- FORMULÁRIO DE CADASTRO DE PO -->
                <fieldset>
                    <form id="formulario-cadastro-po" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6 correcao-xy">                        
                                
                                <label for="poNome">Nome</label>                                                
                                <input type="text" id="poNome" name="poNome">
                                
                                <label for="poEmail">E-mail</label>
                                <input type="email" id="email" name="poEmail">

                                <label for="poFacebook">Facebook</label>                                                
                                <input type="text" id="poFacebook" name="poFacebook" placeholder="/seuperfil">
                                
                                <label for="poTwitter">Twitter</label>
                                <input type="text" id="poTwitter" name="poTwitter" placeholder="@perfil">
                            
                            </div>
                            <div class="col-md-6 correcao-xy">

                    			<label for="poTelefone">Telefone</label>
                    			<input type="tel" id="poTelefone" name="poTelefone">                      
                            
                            	<label for="poUf">Estado</label>
                            	<select id="poUf" name="poUf">
                                    <option value=""> Selecione... </option>
                                    <?php 

                                    	$ufs = array("AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapá","BA"=>"Bahia","CE"=>"Ceará","DF"=>"Distrito Federal","ES"=>"Espírito Santo","GO"=>"Goiás","MA"=>"Maranhão","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Pará","PB"=>"Paraíba","PR"=>"Paraná","PE"=>"Pernambuco","PI"=>"Piauí","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondônia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"São Paulo","TO"=>"Tocantins");

                                    	foreach ($ufs as $uf => $estado) {
                                    		
                                    		if(isset($_POST['poUf']) && $_POST['poUf'] == $uf){
                                    			echo '<option value="' . $uf . '" selected>' . $estado . '</option>';
                                    		}else {
                                    			echo '<option value="' . $uf . '">' . $estado . '</option>';
                                    		}
                                    	}

                                    ?>
                                </select>

                                <label for="poCidade">Cidade</label>
                    			<input type="text" id="poCidade" name="poCidade">                      
                            
                            	<label for="poCidade">Onde atende?</label>
                    			<input type="text" id="poRegiao" name="poRegiao">                      

                            	<label for="poCertificacao">Possui certificação?</label>                                                
                                <select id="poCertificacao" name="poCertificacao">
                                    <option value="">Selecione...</option>
                                    <option value="Sim">Sim</option>
                                    <option value="Não">Não</option>                             
                                </select>

								<label for="foto">Foto</label>                                            
								<input type="file" id="poFoto" name="poFoto" />
								<i>.JPG, .PNG ou .GIF preferencialmente quadrada.</i>

								<input type="hidden" name="poCadastro" value="1">

                            </div>                       
                        </div>
                        <div class="col-md-2 correcao-x">
                            <input type="submit" class="btn" value="Cadastrar">
                        </div>               
                    </form>                
                </fieldset>

            </div>

        </div>

    </div>

<?php get_footer(); ?>

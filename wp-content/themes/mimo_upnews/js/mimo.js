 /**
 * @author       Rob W <gwnRob@gmail.com>
 * @website      http://stackoverflow.com/a/7513356/938089
 * @version      20120724
 * @description  Executes function on a framed YouTube video (see website link)
 *               For a full list of possible functions, see:
 *               https://developers.google.com/youtube/js_api_reference
 * @param String frame_id The id of (the div containing) the frame
 * @param String func     Desired function to call, eg. "playVideo"
 *        (Function)      Function to call when the player is ready.
 * @param Array  args     (optional) List of arguments to pass to function func*/
 
function callPlayer(frame_id, func, args) {
    if (window.jQuery && frame_id instanceof jQuery) frame_id = frame_id.get(0).id;
    var iframe = document.getElementById(frame_id);
    if (iframe && iframe.tagName.toUpperCase() != 'IFRAME') {
        iframe = iframe.getElementsByTagName('iframe')[0];
    }

    // When the player is not ready yet, add the event to a queue
    // Each frame_id is associated with an own queue.
    // Each queue has three possible states:
    //  undefined = uninitialised / array = queue / 0 = ready
    if (!callPlayer.queue) callPlayer.queue = {};
    var queue = callPlayer.queue[frame_id],
        domReady = document.readyState == 'complete';

    if (domReady && !iframe) {
        // DOM is ready and iframe does not exist. Log a message
        window.console && console.log('callPlayer: Frame not found; id=' + frame_id);
        if (queue) clearInterval(queue.poller);
    } else if (func === 'listening') {
        // Sending the "listener" message to the frame, to request status updates
        if (iframe && iframe.contentWindow) {
            func = '{"event":"listening","id":' + JSON.stringify(''+frame_id) + '}';
            iframe.contentWindow.postMessage(func, '*');
        }
    } else if (!domReady || iframe && (!iframe.contentWindow || queue && !queue.ready)) {
        if (!queue) queue = callPlayer.queue[frame_id] = [];
        queue.push([func, args]);
        if (!('poller' in queue)) {
            // keep polling until the document and frame is ready
            queue.poller = setInterval(function() {
                callPlayer(frame_id, 'listening');
            }, 250);
            // Add a global "message" event listener, to catch status updates:
            messageEvent(1, function runOnceReady(e) {
                var tmp = JSON.parse(e.data);
                if (tmp && tmp.id == frame_id && tmp.event == 'onReady') {
                    // YT Player says that they're ready, so mark the player as ready
                    clearInterval(queue.poller);
                    queue.ready = true;
                    messageEvent(0, runOnceReady);
                    // .. and release the queue:
                    while (tmp = queue.shift()) {
                        callPlayer(frame_id, tmp[0], tmp[1]);
                    }
                }
            }, false);
        }
    } else if (iframe && iframe.contentWindow) {
        // When a function is supplied, just call it (like "onYouTubePlayerReady")
        if (func.call) return func();
        // Frame exists, send message
        iframe.contentWindow.postMessage(JSON.stringify({
            "event": "command",
            "func": func,
            "args": args || [],
            "id": frame_id
        }), "*");
    }
    /* IE8 does not support addEventListener... */
    function messageEvent(add, listener) {
        var w3 = add ? window.addEventListener : window.removeEventListener;
        w3 ?
            w3('message', listener, !1)
        :
            (add ? window.attachEvent : window.detachEvent)('onmessage', listener);
    }
}


jQuery(document).ready(function(){

(function() {

  var root = (typeof exports == 'undefined' ? window : exports);

  var config = {
    // Ensure Content-Type is an image before trying to load @2x image
    // https://github.com/imulus/retinajs/pull/45)
    check_mime_type: true
  };



  root.Retina = Retina;

  function Retina() {}

  Retina.configure = function(options) {
    if (options == null) options = {};
    for (var prop in options) config[prop] = options[prop];
  };

  Retina.init = function(context) {
    if (context == null) context = root;

    var existing_onload = context.onload || new Function;

    context.onload = function() {
      var images = document.getElementsByTagName("img"), retinaImages = [], i, image;
      for (i = 0; i < images.length; i++) {
        image = images[i];
        retinaImages.push(new RetinaImage(image));
      }
      existing_onload();
    }
  };

  Retina.isRetina = function(){
    var mediaQuery = "(-webkit-min-device-pixel-ratio: 1.5),\
                      (min--moz-device-pixel-ratio: 1.5),\
                      (-o-min-device-pixel-ratio: 3/2),\
                      (min-resolution: 1.5dppx)";

    if (root.devicePixelRatio > 1)
      return true;

    if (root.matchMedia && root.matchMedia(mediaQuery).matches)
      return true;

    return false;
  };


  root.RetinaImagePath = RetinaImagePath;

  function RetinaImagePath(path) {
    this.path = path;
    this.at_2x_path = path.replace(/\.\w+$/, function(match) { return "@2x" + match; });
  }

  RetinaImagePath.confirmed_paths = [];

  RetinaImagePath.prototype.is_external = function() {
    return !!(this.path.match(/^https?\:/i) && !this.path.match('//' + document.domain) )
  }

  RetinaImagePath.prototype.check_2x_variant = function(callback) {
    var http, that = this;
    if (this.is_external()) {
      return callback(false);
    } else if (this.at_2x_path in RetinaImagePath.confirmed_paths) {
      return callback(true);
    } else {
      http = new XMLHttpRequest;
      http.open('HEAD', this.at_2x_path);
      http.onreadystatechange = function() {
        if (http.readyState != 4) {
          return callback(false);
        }

        if (http.status >= 200 && http.status <= 399) {
          if (config.check_mime_type) {
            var type = http.getResponseHeader('Content-Type');
            if (type == null || !type.match(/^image/i)) {
              return callback(false);
            }
          }

          RetinaImagePath.confirmed_paths.push(that.at_2x_path);
          return callback(true);
        } else {
          return callback(false);
        }
      }
      http.send();
    }
  }



  function RetinaImage(el) {
    this.el = el;
    this.path = new RetinaImagePath(this.el.getAttribute('src'));
    var that = this;
    this.path.check_2x_variant(function(hasVariant) {
      if (hasVariant) that.swap();
    });
  }

  root.RetinaImage = RetinaImage;

  RetinaImage.prototype.swap = function(path) {
    if (typeof path == 'undefined') path = this.path.at_2x_path;

    var that = this;
    function load() {
      if (! that.el.complete) {
        setTimeout(load, 5);
      } else {
        that.el.setAttribute('width', that.el.offsetWidth);
        that.el.setAttribute('height', that.el.offsetHeight);
        that.el.setAttribute('src', path);
      }
    }
    load();
  }




  if (Retina.isRetina()) {
    Retina.init(root);
  }

})();


		
       
    
	var vimeoPlayers = jQuery('.flexslider').find('iframe'), player;        

for (var i = 0, length = vimeoPlayers.length; i < length; i++) {            
        player = vimeoPlayers[i];           
        $f(player).addEvent('ready', ready);        
}       

function addEvent(element, eventName, callback) {           
    if (element.addEventListener) {                 
        element.addEventListener(eventName, callback, false)            
    } else {                
        element.attachEvent(eventName, callback, false);            
    }       
}       

function ready(player_id) {             
    var froogaloop = $f(player_id);             
    froogaloop.addEvent('play', function(data) {                
        jQuery('.flexslider').flexslider("pause");  
          
    });             
    froogaloop.addEvent('pause', function(data) {               
        jQuery('.flexslider').flexslider("play"); 
             
    });         
}  
	jQuery('.connections').css({'border-top':'0px'});
	jQuery('.fbConnectWidgetFooter').hide();
	

 
jQuery(window).scroll(function(){
            if (jQuery(this).scrollTop() > 100) {
                jQuery('#gantry-totop').fadeIn();
            } else {
                jQuery('#gantry-totop').fadeOut();
            }
        }); 
 
jQuery('.scrollup').click(function(){
            jQuery("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
 
  


	jQuery('div.ca-content-wrapper').show();
	jQuery('.loading').hide();
	var n = 1;	
	
/* fix Filter when scroll */



  var $sidebar   = jQuery("#rt-top"),
        $window    = jQuery(window),
        offset     = jQuery("#rt-hello").outerHeight(),
        topPadding = 15;
var $cache = jQuery('.allnewswallmimo-filter'); 

    $window.scroll(function() {
        if ($window.scrollTop() > offset) {
        	
				$sidebar.css('position','fixed');
				$cache.animate({'height':'show'},400);
			
            
        } else {
            $sidebar.css('position','relative');
            $cache.animate({'height':'hide'},40);
        }
    });
		
	  



/* Hovers

********************************************************************************** */
    
	
	
		jQuery('.button_link,button[type=submit],button,input[type=submit],input[type=button],input[type=reset]').hover(
		function() {
				jQuery(this).stop().animate({opacity:0.8},400);
			},
			function() {
				jQuery(this).stop().animate({opacity:1},400);
		});
		jQuery('.item2, .flexslider-single').hover(function(){
					jQuery(this).find('.all-article-social').stop().animate({'height': 'show'},'fast');
					}, function(){
					jQuery(this).find('.all-article-social').stop().animate({'height': 'hide'},'fast');
				});
				
		jQuery('.all_icon').hover(function(){
					jQuery(this).find('.socialhidden').stop().animate({'width': 'show'},'fast', function() {
    // Animation complete.
    					jQuery(this).find('.socialtext').stop().animate({'opacity': 1},'fast');
					});
					}, function(){
					jQuery(this).find('.socialhidden').stop().animate({'width': 'hide'},'fast', function() {
    // Animation complete.
    				jQuery(this).find('.socialtext').stop().animate({'opacity': 0},'fast');
					});
				});
				jQuery('#newswallmimo-filter li a').hover(function(){
					jQuery(this).stop().animate({padding: '20px 40px 20px 20px'});
					}, function(){
					jQuery(this).stop().animate({padding: '20px 10px'});
				});
				
								
				jQuery('.searchall').hover(function(){
				
					jQuery('#searchform input[type="text"]').stop().animate({width : 'show'},'fast');
					
					}, function(){
					jQuery('#searchform input[type="text"]').stop().animate({width : 'hide'},'fast');
					
				});
				jQuery('.flex-direction-nav a').hover(function(){
					jQuery(this).stop().animate({opacity : 1},'fast');
					}, function(){
					jQuery(this).stop().animate({opacity : 0.7},'fast');
				});
				jQuery('.flexslider').hover(function(){
					jQuery(this).children('.flex-direction-nav').stop().animate({height : 'show'},'fast');
					}, function(){
					jQuery(this).children('.flex-direction-nav').stop().animate({height : 'hide'},'fast');
				});
				jQuery('.blog_thumb_standard').hover(function(){
					jQuery(this).find('.rt-image').stop().animate({opacity : 0.5},'fast');
						
					}, function(){
					jQuery(this).find('.rt-image').stop().animate({opacity : 1},'fast');
				});


/* Tabs 

********************************************************************************** */

	jQuery('#tabvanilla > ul').tabs({ fx: { height: 'toggle', opacity: 'toggle' } });
	jQuery('#tabvanilla_widget > ul').tabs({ fx: { height: 'toggle', opacity: 'toggle' } });
	jQuery('#featuredvid > ul').tabs();
	
	
/* Code for left tabs */


	jQuery('#lefttabvanilla > ul').tabs({ fx: { height: 'toggle', opacity: 'toggle' } });
	
	
	
});


/* Menu 

********************************************************************************** */

jQuery(document).ready(

    function() {
        jQuery( ".select-menu" ).change(
            function() { 
                window.location = jQuery(this).find("option:selected").val();
            }
        );
    }
	
	
);


/* Walls 

********************************************************************************** */




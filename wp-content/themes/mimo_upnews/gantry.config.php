<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
defined( 'GANTRY_VERSION' ) or die();

$gantry_presets = array(
    'presets' => array(
      
	  'beta' => array(
            'name' => 'Beta',
            'linkcolor' => '#ec524b',
                    ),
	  'blue' => array(
            'name' => 'Blue',
            'linkcolor' => '#32b8eb',
            
        ),
	  'brown' => array(
            'name' => 'Brown',
            'linkcolor' => '#917700',
        ),
	  'cool' => array(
            'name' => 'Cool',
            'linkcolor' => '#adc42f',
         ),
	   'creative' => array(
            'name' => 'Creative',
            'linkcolor' => '#77adc4',
        ), 
	   'cyan' => array(
            'name' => 'Cyan',
            'linkcolor' => '#82e8ff',
        ),
	   'darkblue' => array(
            'name' => 'Dark blue',
            'linkcolor' => '#004578',
         ),
	   'darkgreen' => array(
            'name' => 'Dark green',
            'linkcolor' => '#66a14e',
        ),
	   'darkred' => array(
            'name' => 'Dark red',
            'linkcolor' => '#960311',
         ),
	   'egg' => array(
            'name' => 'Egg',
            'linkcolor' => '#f1b932',
          ),
	   'electricred' => array(
            'name' => 'Electric Red',
            'linkcolor' => '#e4470d',
         ),
	   'green' => array(
            'name' => 'Green',
            'linkcolor' => '#c2e100',
        ),
	   'greencake' => array(
            'name' => 'Greencake',
            'linkcolor' => '#26c39a',
        ),
	   'greeny' => array(
            'name' => 'Greeny',
            'linkcolor' => '#20c5ba',
        ),
	   'orange' => array(
            'name' => 'Orange',
            'linkcolor' => '#f78d51',
        ),
	   'pastel' => array(
            'name' => 'Pastel',
            'linkcolor' => '#f1ca6e',
         ),
	   'pink' => array(
            'name' => 'Pink',
            'linkcolor' => '#ff307f',
         ),
        'red' => array(
            'name' => 'Red',
            'linkcolor' => '#f2193a',
          ),
        'violet' => array(
            'name' => 'Violet',
            'linkcolor' => '#473951',
         )
    )
);

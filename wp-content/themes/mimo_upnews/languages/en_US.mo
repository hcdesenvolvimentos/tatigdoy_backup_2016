��    e      D  �   l      �     �     �     �  
   �     �     �     �     �     �     �     �     	     	     !	     4	     L	     Z	     h	     {	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	      
     
     
     $
     ,
     4
     ;
     G
     T
  	   f
     p
     
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
                      
        '     ,     9  	   K     U     \     a     o  
   �     �  	   �     �     �     �  
   �     �  
   �  
   �     �     �  	          	        '     6     ?     K     Y     l     �     �     �     �     �     �          $     5  	   K     U  
   d     o          �     �     �  �   �     �     �     �  
   �     �     �     �            4        N     \  ,   d     �  '   �     �     �  Q   �     :     G     O  )   \     �  %   �     �  I   �       	     $   !     F  (   N     w          �  0   �     �  E   �       E   !     g  !   s     �     �  #   �     �     �     �     �  -        5     :     @     E  4   K     �     �     �  	   �     �     �     �     �  
   �  #   �  	        '      .     O  
   [     f     l  
   �  >   �     �  	   �  1   �  	     G        e     n     z  =   �     �  F   �     !  (   2     [  6   n     �  G   �            	   6  &   @  
   g     r     �     �     �     �            L   5      $       Z      H   8   U          P           O   "       <   G       6   7   I       F   [      a                        d       @   #           b   '   ^   &       B                    >          +   .   K   1                     D   `   -   (   ;       
   Y              R   	   *       :   J      e       0   !   _   /       A   M       T       S                   =   E   9   4      ]          V                 c   Q   N          ,   2   W                 \   X      ?       C   )   3   %    ALWAYS ANY ARCHIVE_VIEW ATTACHMENT AUTHOR AUTHOR_META AUTHOR_META_DESC AUTO BLOG_CATEGORY BLOG_CATEGORY_DESC CATEGORY_VIEW COLUMNS COLUMNS_DESC COMMENTS_META_DATA COMMENTS_META_DATA_DESC COMMENT_COUNT CONTACT_EMAIL CONTACT_EMAIL_DESC CONTACT_FORM CONTENT CONTENT_TYPE CONTENT_TYPE_DESC CUSTOM_HEADING CUSTOM_HEADING_DESC CUSTOM_QUERY CUSTOM_QUERY_DESC DATE DATE_META DATE_META_DESC DISPLAY DISPLAY_DESC ENABLED EXCERPT FORMAT FORMAT_DESC HEADER_STYLE HEADER_STYLE_DESC HTML_TAGS HTML_TAGS_DESC INTRO_POSTS INTRO_POSTS_DESC LABEL LEADING_POSTS LEADING_POSTS_DESC LINK LINK_DESC MODIFIED MODIFIED_DATE_META MODIFIED_DATE_META_DESC NAME NEVER None ORDER ORDER_DESC PAGE PAGE_HEADING PAGE_HEADING_DESC PAGE_VIEW PARENT POST POSTS_DISPLAY POSTS_DISPLAY_DESC POST_COUNT POST_COUNT_DESC POST_VIEW PREFIX PREFIX_DESC PRIVATE_KEY PUBLIC_KEY QUERY QUERY_DESC QUERY_TYPE QUERY_TYPE_DESC RANDOM READ_MORE READ_MORE_DESC RECAPTCHA RECAPTCHA_DESC REVISION SEARCH_VIEW SHOW_COMMENTS SHOW_COMMENTS_DESC SHOW_FEATURED_IMAGE SHOW_FEATURED_IMAGE_DESC SHOW_FOOTER_NOTE SHOW_FOOTER_NOTE_DESC SHOW_POST_CATEGORY SHOW_POST_CATEGORY_DESC SHOW_POST_PARENT_CATEGORY SHOW_POST_PARENT_CATEGORY_DESC SHOW_POST_TITLES SHOW_POST_TITLES_DESC SHOW_TAGS SHOW_TAGS_DESC SHOW_TITLE SHOW_TITLE_DESC Search Results for: %s TAG_VIEW TEMPLATE_NAME_SETTINGS TITLE Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Jakub Baran <jakub@rockettheme.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
 Always Any Archive View Attachment Author Author Meta Shows the author of the post. Auto Blog Category Include or exclude category ID's for the Blog query. Category View Columns Number of columns to display Intro Posts in. Comments Meta Data Shows the number of comments in a post. Comment Count Contact Email Email address for mails to be sent on. If empty admin email address will be used. Contact Form Content Content Type Choose between posts content and excerpt. Custom Heading Type in your own custom heading text. Custom Query Create your own custom query by adding any WP_Query compatible arguments. Date Date Meta Shows the creation date of the post. Display Set when Read More button should appear. Enabled Excerpt Format Set the date format using the PHP's date format. Header Style Select an available style option for the header area of the template. HTML Tags Note Displays the footer with available HTML tags under the comments form. Intro Posts Number of Intro Posts to display. Label Leading Posts Number of Leading Posts to display. Link Adds a link to the post. Modified Modified Date Meta Shows the last modification date of the post. Name Never NONE Order Change in what order should the posts get displayed. Page Page Heading Shows the page heading. Page View Parent Post Posts Display Set number of posts to display. Post Count Number of posts to display on page. Post View Prefix Add your own custom prefix text. Private Key Public Key Query Blog view query settings. Query Type Set what type of content should be displayed on the Blog view. Random Read More Configure how the Read More button should behave. reCaptcha If enabled, user will have to solve a captcha before sending a message. Revision Search View Show Comments Displays comments and comments form in the full content view. Show Featured Image Should the posts Featured Image be displayed in the full content view. Show Footer Note Displays posts footer below the content. Show Post Category Show the first category that this post is assigned to. Show Post Parent Category Show the parent category of the category that this post is assigned to. Show Post Titles Shows individual Post titles. Show Tags Displays posts tags below the content. Show Title Shows title on the page. Resultados da busca por: Tag View Gantry Theme Settings Title 
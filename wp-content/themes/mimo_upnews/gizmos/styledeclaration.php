<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

defined( 'GANTRY_VERSION' ) or die();

gantry_import( 'core.gantrygizmo' );

/**
 * @package     gantry
 * @subpackage  features
 */
class GantryGizmoStyleDeclaration extends GantryGizmo {

	var $_name = 'styledeclaration';
	
	function isEnabled(){
		global $gantry;
		$menu_enabled = $this->get('enabled');

		if (1 == (int)$menu_enabled) return true;
		return false;
	}

	function query_parsed_init() {
		global $gantry, $mimo_css;
		$browser = $gantry->browser;
		// Menu Colors
		
		
		// Colors
		 
        $linkColor = new Color($gantry->get('linkcolor'));
        
		$mimo_css = 'a,.separator, .criteria_total h2, .criteria span,.criteria_total  span, .criteria,  .tweet_date, .colori {color:'.$gantry->get('linkcolor').';}';
		$mimo_css  .= '.twitter_icon, .facebook_icon, .google_icon, .stumbleupon_icon, .linkedin_icon, .youtube_icon, .rss_icon, .mail_icon, #searchform, #searchform input[type="text"] {color:'.$gantry->get('topbarlinkcolor').';}';
		
		$mimo_css .= '.gf-menu.l1 >  .menu1:hover {background:'.$gantry->get('menu1').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu2:hover {background:'.$gantry->get('menu2').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu3:hover {background:'.$gantry->get('menu3').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu4:hover {background:'.$gantry->get('menu4').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu5:hover {background:'.$gantry->get('menu5').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu6:hover {background:'.$gantry->get('menu6').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu7:hover {background:'.$gantry->get('menu7').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu8:hover {background:'.$gantry->get('menu8').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu9:hover {background:'.$gantry->get('menu9').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu10:hover {background:'.$gantry->get('menu10').';}';
		
		$mimo_css .= '.gf-menu.l1 >  .menu1:hover a {color:'.$gantry->get('menu1color').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu2:hover a {color:'.$gantry->get('menu2color').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu3:hover a {color:'.$gantry->get('menu3color').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu4:hover a {color:'.$gantry->get('menu4color').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu5:hover a {color:'.$gantry->get('menu5color').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu6:hover a {color:'.$gantry->get('menu6color').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu7:hover a {color:'.$gantry->get('menu7color').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu8:hover a {color:'.$gantry->get('menu8color').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu9:hover a {color:'.$gantry->get('menu9color').';}';
		$mimo_css .= '.gf-menu.l1 >  .menu10:hover a {color:'.$gantry->get('menu10color').';}';
		
		$mimo_css .= '.fraseblock, .mediumfraseblock, .smallfraseblock, #theaccordion ul li:hover,  .color .module-content, .linkcolorbg ,  .hover-images .icon, #breadcrumbs, .color, #infscr-loading, .lefttabnav li, .lefttabnav li.ui-tabs-selected, .tabnav-widget li,.widget_gantry_menu li:hover,.widget_gantry_menu li.active, .searchall:hover, .all_icon:hover, .navigation a, input[type="submit"], .gotopost{background:'.$gantry->get('linkcolor').';}';
		$mimo_css .= '#rt-hello{background:'.$gantry->get('hellobg').';} .icon-ticker i{color:'.$gantry->get('hellobg').' ;}.icon-ticker i{background:'.$gantry->get('hellocolor').' ;}#rt-hello, #rt-hello a{color:'.$gantry->get('hellocolor').' ;}';
		
		
        
		
		
		$mimo_css .= '.greysmallfraseblock, .greymediumfraseblock, .greyfraseblock, .readon_blog, .readon_recent, .prev-page a:hover, .next-page a:hover, .flickr-wall .page-number, .pagination a:hover, .recent-title-slider .article-info {background-color:'.$gantry->get('linkcolor').';}';
		
	  //Frases Colors
	    
	    $mimo_css .= '.format-standard .all-article-info,.format-standard .mimo-content,.format-standard .article-title, .standardpost, .fcfraseblock, .fcmediumfraseblock, .fcsmallfraseblock, .titlefc .module-title, .gf-menu .fcolor:hover, .fcolorsub {background:'.$gantry->get('standard-bgcolor').';}';
	    
		
		$mimo_css .= '.format-video .all-article-info,.format-video .mimo-content,.format-video .article-title, .videopost, .scfraseblock, .scmediumfraseblock, .scsmallfraseblock, .titlesc .module-title, .gf-menu .scolor:hover, .scolorsub{background-color:'.$gantry->get('video-bgcolor').';}';
		
		
		$mimo_css .= '.format-audio .all-article-info,.format-audio .mimo-content,.format-audio .article-title, .audiopost, .tcfraseblock, .tcmediumfraseblock, .tcsmallfraseblock, .titletc .module-title, .gf-menu .tcolor:hover, .tcolorsub {background-color:'.$gantry->get('audio-bgcolor').';}';
		

		
		$mimo_css .= '.format-link .all-article-info,.format-link .mimo-content,.format-link .article-title, .linkpost, .focfraseblock, .focmediumfraseblock, .focsmallfraseblock, .titlefoc .module-title, .gf-menu .focolor:hover, .focolorsub{background-color:'.$gantry->get('linkpost-bgcolor').';}';
		
		
		$mimo_css .= '.format-image .all-article-info,.format-image .mimo-content,.format-image .article-title, .imagepost, .ficfraseblock, .ficmediumfraseblock, .ficsmallfraseblock, .titlefic .module-title, .gf-menu .ficolor:hover, .ficolorsub{background-color:'.$gantry->get('image-bgcolor').';}';
		
		
		$mimo_css .= '.format-gallery .all-article-info,.format-gallery .mimo-content,.format-gallery .article-title{background-color:'.$gantry->get('gallery-bgcolor').';}';


		
		$mimo_css .= '.format-aside .all-article-info,.format-aside .mimo-content,.format-aside .article-title, .asidepost{background-color:'.$gantry->get('aside-bgcolor').';}';
		
		
		$mimo_css .= '.format-status .all-article-info,.format-status .mimo-content,.format-status .article-title, .statuspost{background-color:'.$gantry->get('status-bgcolor').';}';
		
		
		$mimo_css .= '.format-quote .all-article-info,.format-quote .mimo-content,.format-quote .article-title, .quotepost {background-color:'.$gantry->get('quote-bgcolor').';}';
		
		//Logo Image
		$mimo_css  .= '.logo-block {background:'.$gantry->get('logobg').';}';
		$mimo_css  .= '.flex-control-paging li a:hover, .twitter-title h2 a,.facebook-title h2 a,.rss-title h2 a {background:'.$gantry->get('linkcolor').';}';
		// Tabs
		
		//Container
		$mimo_css  .= '.rt-container {background:'.$gantry->get('containerbg').';}';
		// Top
		$mimo_css  .= '#rt-header {background:'.$gantry->get('headerbg').';}';
		
		
		
		$mimo_css  .= '#rt-top, #rt-top .rt-container{background:'.$gantry->get('topbarbg').'; color:'.$gantry->get('topbarlinkcolor').';}.widget_gantry_menu li  a, .all_icon a{color:'.$gantry->get('topbarlinkcolor').';}';
		$mimo_css  .= '.widget_search input::-webkit-input-placeholder,.widget_search textarea::-webkit-input-placeholder{color:'.$gantry->get('topbarlinkcolor').';}';
		//Filter
		$mimo_css  .= '.allnewswallmimo-filter{background:'.$gantry->get('filterbg').';}';
		$mimo_css  .= '.allnewswallmimo-filter a{color:'.$gantry->get('filterlinkcolor').';}';
		
		//Search
		$mimo_css  .= '#searchform input[type="text"], #searchform{border-color:'.$gantry->get('topbarbg').';}';
		
		
		
		$mimo_css  .= '#newswallmimo-filter li:active a{ color:#ffffff;}';
		$mimo_css  .= '.component-content .article-title-single h1{color:'.$gantry->get('singlecolor').';}';
		
		// Text color in post formats
		$mimo_css .= '.videoitem, .videoitem .article-title h2, .videoitem a, .videoitem .article-title i {color:'.$gantry->get('video-textcolor').';}';
		$mimo_css .= '.imageitem, .imageitem .article-title h2, .imageitem a, .videoitem .article-title i {color:'.$gantry->get('image-textcolor').';}';
		$mimo_css .= '.standarditem, .standarditem .article-title h2, .standarditem a {color:'.$gantry->get('standard-textcolor').';}';
		$mimo_css .= '.linkitem, .linkitem .article-title h2, .linkitem a {color:'.$gantry->get('linkpost-textcolor').';}';
		$mimo_css .= '.audioitem, .audioitem .article-title h2, .audioitem a {color:'.$gantry->get('audio-textcolor').';}';
		$mimo_css .= '.galleryitem, .galleryitem .article-title h2, .galleryitem a {color:'.$gantry->get('gallery-textcolor').';}';
		$mimo_css .= '.statusitem, .statusitem .article-title h2, .statusitem a {color:'.$gantry->get('status-textcolor').';}';
		$mimo_css .= '.quoteitem, .quoteitem .article-title h2, .quoteitem a {color:'.$gantry->get('quote-textcolor').';}';
		$mimo_css .= '.asideitem, .asideitem .article-title h2, .asideitem a {color:'.$gantry->get('aside-textcolor').';}';
		//Body
		$mimo_css  .= 'body {background:url('.get_site_url().'/wp-content/themes/mimo_upnews/images/backgrounds/'.$gantry->get('bgimage').') 0 0 repeat fixed ;color:'.$gantry->get('bodytext').';background-color:'.$gantry->get('bodybg').';}';
		
        // Gradients
        $mimo_css .= '.button, button.validate, #member-profile a, #member-registration a, .formelm-buttons button, .btn-primary {background-color: '.$linkColor->lighten('4%').'; '.$this->_createGradient('top', $linkColor->lighten('4%'), '1', '0%', $linkColor->darken('9%'), '1', '100%').'}'."\n";
        $mimo_css .= '.button:hover, button.validate:hover, #member-profile a:hover, #member-registration a:hover, .formelm-buttons button:hover, .btn-primary:hover {background-color: '.$linkColor->lighten('10%').'; '.$this->_createGradient('top', $linkColor->lighten('10%'), '1', '0%', $linkColor->darken('3%'), '1', '100%').'}'."\n";
        $mimo_css .= '.button:active, button.validate:active, #member-profile a:active, #member-registration a:active, .formelm-buttons button:active, .btn-primary:active {background-color: '.$linkColor->darken('2%').'; '.$this->_createGradient('top', $linkColor->darken('2%'), '1', '0%', $linkColor->lighten('8%'), '1', '100%').'}'."\n";

		//Icon Home Color
		if(is_home())
        $mimo_css .= '.searchhome a {background:'.$gantry->get('linkcolor').';}.searchhome a i {color:#ffffff;}';
       

     
       //Articles titles
       $mimo_css .= '.component-content .article-title h2 {font-size:'.$gantry->get('articletitlesize').';}';
        // Logo
        $mimo_css .= $this->buildLogo();

		$this->_disableRokBoxForiPhone();

		$gantry->addInlineStyle($mimo_css);      
        if ($gantry->get('layout-mode')=="responsive") $gantry->addLess('mediaqueries.less', 'mediaqueries.css', 9);
        if ($gantry->get('layout-mode')=="960fixed") $gantry->addLess('960fixed.less');
        if ($gantry->get('layout-mode')=="1200fixed") $gantry->addLess('1200fixed.less'); 

		// add inline css from the Custom CSS field
		$gantry->addInlineStyle($gantry->get('customcss'));

	}

	function buildLogo(){
		global $gantry;

		if ($gantry->get('logo')=="none") return "";

		$source = $width = $height = "";

		$logo = str_replace("&quot;", '"', str_replace("'", '"', $gantry->get('logo')));
		$data = json_decode($logo);

		if (!$data){
			if (strlen($logo)) $source = $logo;
			else return "";
		} else {
			$source = $data->path;
		}

		$baseUrl = trailingslashit(get_bloginfo('wpurl'));

		if (substr($baseUrl, 0, strlen($baseUrl)) == substr($source, 0, strlen($baseUrl))){
			$file = ABSPATH . substr($source, strlen($baseUrl));
		} else {
			$file = ABSPATH . $source;
		}

		if (isset($data->width) && isset($data->height)){
			$width = $data->width;
			$height = $data->height;
		} else {
			$size = @getimagesize($file);
			$width = $size[0];
			$height = $size[1];
		}

		$output = "";
		

		$file = preg_replace('/\//i', DS, $file);

		return (file_exists($file)) ?$output : '';
	}
	

	function _createGradient($direction, $from, $fromOpacity, $fromPercent, $to, $toOpacity, $toPercent){
		global $gantry;
		$browser = $gantry->browser;

		$fromColor = $this->_RGBA($from, $fromOpacity);
		$toColor = $this->_RGBA($to, $toOpacity);
		$gradient = $default_gradient = '';

		$default_gradient = 'background: linear-gradient('.$direction.', '.$fromColor.' '.$fromPercent.', '.$toColor.' '.$toPercent.');';

		switch ($browser->engine) {
			case 'gecko':
				$gradient = ' background: -moz-linear-gradient('.$direction.', '.$fromColor.' '.$fromPercent.', '.$toColor.' '.$toPercent.');';
				break;

			case 'webkit':
				if ($browser->shortversion < '5.1'){

					switch ($direction){
						case 'top':
							$from_dir = 'left top'; $to_dir = 'left bottom'; break;
						case 'bottom':
							$from_dir = 'left bottom'; $to_dir = 'left top'; break;
						case 'left':
							$from_dir = 'left top'; $to_dir = 'right top'; break;
						case 'right':
							$from_dir = 'right top'; $to_dir = 'left top'; break;
					}
					$gradient = ' background: -webkit-gradient(linear, '.$from_dir.', '.$to_dir.', color-stop('.$fromPercent.','.$fromColor.'), color-stop('.$toPercent.','.$toColor.'));';
				} else {
					$gradient = ' background: -webkit-linear-gradient('.$direction.', '.$fromColor.' '.$fromPercent.', '.$toColor.' '.$toPercent.');';
				}
				break;

			case 'presto':
				$gradient = ' background: -o-linear-gradient('.$direction.', '.$fromColor.' '.$fromPercent.', '.$toColor.' '.$toPercent.');';
				break;

			case 'trident':
				if ($browser->shortversion >= '10'){
					$gradient = ' background: -ms-linear-gradient('.$direction.', '.$fromColor.' '.$fromPercent.', '.$toColor.' '.$toPercent.');';
				} else if ($browser->shortversion <= '6'){
					$gradient = $from;
					$default_gradient = '';
				} else {

					$gradient_type = ($direction == 'left' || $direction == 'right') ? 1 : 0;
					$from_nohash = str_replace('#', '', $from);
					$to_nohash = str_replace('#', '', $to);

					if (strlen($from_nohash) == 3) $from_nohash = str_repeat(substr($from_nohash, 0, 1), 6);
					if (strlen($to_nohash) == 3) $to_nohash = str_repeat(substr($to_nohash, 0, 1), 6);

					if ($fromOpacity == 0 || $fromOpacity == '0' || $fromOpacity == '0%') $from_nohash = '00' . $from_nohash;
					if ($toOpacity == 0 || $toOpacity == '0' || $toOpacity == '0%') $to_nohash = '00' . $to_nohash;

					$gradient = " filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#".$to_nohash."', endColorstr='#".$from_nohash."',GradientType=".$gradient_type." );";

					$default_gradient = '';

				}
				break;

			default:
				$gradient = $from;
				$default_gradient = '';
				break;
		}

		return  $default_gradient . $gradient;
	}

	function _HEX2RGB($hexStr, $returnAsString = false, $seperator = ','){
		$hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr);
		$rgbArray = array();
	
		if (strlen($hexStr) == 6){
			$colorVal = hexdec($hexStr);
			$rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
			$rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
			$rgbArray['blue'] = 0xFF & $colorVal;
		} elseif (strlen($hexStr) == 3){
			$rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
			$rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
			$rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
		} else {
			return false;
		}
	
		return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray;
	}
	
	function _RGBA($hex, $opacity){
		return 'rgba(' . $this->_HEX2RGB($hex, true) . ','.$opacity.')';
	}

	function _disableRokBoxForiPhone() {
		global $gantry;

		if ($gantry->browser->platform == 'iphone' || $gantry->browser->platform == 'android') {
			$gantry->addInlineScript("window.addEvent('domready', function() {\$\$('a[rel^=rokbox]').removeEvents('click');});");
		}
	}
}
<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined( 'ABSPATH' ) or die( 'Restricted access' );
?>

<?php global $gantry, $post, $posts, $query_string, $wp_query, $mimo_post_type; ?>

	<?php /** Begin Query Setup **/ ?>
	
	<?php

	// Page Type used as a prefix for gantry options ie. archive-content
	$mimo_page_type = basename( __FILE__, '.php' );

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$query = $wp_query->query;
	if ( !is_array( $query ) ) parse_str( $query, $query ); 
	
	$custom_query = new WP_Query( array_merge( $query, array( 'posts_per_page' => get_option( 'posts_per_page' ), 'paged' => $paged ) ) ); ?>

	<?php /** End Query Setup **/ ?>

	<?php if( $custom_query->have_posts() ) : ?>
	
		<?php /** Begin Page Heading **/ ?>
		
		<?php if( $gantry->get( $mimo_page_type . '-page-heading-enabled', '1' ) ) : ?>
		
			<?php if( $gantry->get( $mimo_page_type . '-page-heading-text' ) != '' ) : ?>
			<div class="archive-page-heading">
				<h1>
					<?php echo $gantry->get( $mimo_page_type . '-page-heading-text' ); ?>
				</h1>
			</div>
			<?php else : ?>
				<div class="archive-page-heading">																								
				<h1>
					<?php printf( _r( 'Category Archives: %s' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?>
				</h1>
</div>
			<?php endif; ?>

		<?php endif; ?>
		<?php if ($gantry->get( $mimo_page_type . '-page-filter-enabled', '1' ) ){
				
$category_id = get_cat_ID(single_cat_title( '', false ));
		 $parent_terms = get_terms('category', array('parent' => 0, 'orderby' => 'slug', 'hide_empty' => true));

	$terms = get_terms('category', array('parent' => $category_id, 'orderby' => 'slug', 'hide_empty' => true));
						
       
                 $count = count($terms);
				 
				  echo '<div class="allnewswallmimo-filter"><ul id="newswallmimo-filter">';  
                   
                 echo '<li><a class="selected" href="#" data-filter="*" title="">All</a></li>';  
                 if ( $count > 0 ){  
  
                        foreach ( $terms as $term ) {  
  
                            $termname = strtolower($term->name);  
                            $termname = str_replace(' ', '-', $termname);  
                            echo '<li><a href="#" title="" data-filter=".'.$termname.'">'.$term->name.'</a></li>';  
                        }  
                 }  
                 echo '</ul></div>'; 
             
                         }  ?>
		<?php /** End Page Heading **/ ?>

		<?php /** Begin Posts **/ ?>
        <div class="rock_newswall">
					<ul id="container_newswall" class="container_newswall_class large<?php echo $gantry->get( 'category-columns' );?>"> 									
		<?php while ($custom_query->have_posts()) : $custom_query->the_post();
		$terms = wp_get_post_terms($post->ID,'category', array('orderby' => 'slug', 'hide_empty' => true));
		 if ( $terms && ! is_wp_error( $terms ) ) :  
            $links = array();  
  
            foreach ( $terms as $term )  
            {  
            $links[] = $term->name;  
            }  
            $links = str_replace(' ', '-', $links);  
            $tax = join( " ", $links );  
            else :  
            $tax = '';  
            endif;  
                       
           $more = 0; ?>
        <?php /** Begin Query Setup **/ ?>

            
            

						

 <li  class="mimo_newswall covernews <?php echo strtolower($tax); ?> item2 <?php  if (has_post_format( 'image' ) ):$mimo_post_type = 'imageitem'; endif; ?><?php if (has_post_format( 'link' ) ):$mimo_post_type = 'linkitem'; endif; ?><?php if (has_post_format( 'audio' ) ):$mimo_post_type = 'audioitem'; endif; ?><?php if (has_post_format( 'video' ) ):$mimo_post_type = 'videoitem'; endif; ?><?php if (has_post_format( 'gallery' ) ):$mimo_post_type = 'galleryitem'; endif; ?><?php if (has_post_format( 'aside' ) ):$mimo_post_type = 'asideitem'; endif; ?><?php if (has_post_format( 'status' ) ):$mimo_post_type = 'statusitem'; endif; ?><?php if (has_post_format( 'quote' ) ):$mimo_post_type = 'quoteitem'; endif; ?><?php if ( !get_post_format() ) { $mimo_post_type = 'standarditem'; }; echo $mimo_post_type;?> ">

					<?php

					$format = get_post_format();
					if ( false === $format )
						$format = 'standard';

					?>

			<?php $this->gantry_get_template_part( 'content/content', ( post_type_supports( get_post_type(), 'post-formats' ) ? get_post_format() : get_post_type() ) ); ?>
		</li>
		<?php endwhile; ?>
		</ul>
        </div>
		<?php /** End Posts **/ ?>
		 <?php if ($gantry->get($mimo_page_type . '-infinitescroll') == 'button'){ ?>
					<div class="navigation">
<?php next_posts_link('<i class="icon-plus"></i>',$custom_query->max_num_pages) ?>
</div>

            		<?php }; ?>
           
             <?php if ($gantry->get($mimo_page_type . '-infinitescroll') == 'auto' ){ ?>
					<div class="navigation hidden">
<?php next_posts_link('More Posts',$custom_query->max_num_pages) ?>
</div>
            		</div>
            		<?php }; ?>
		<?php /** Begin Pages Navigation **/ ?>
			
		<?php if( $gantry->get( 'pagination-enabled', '1' ) && $custom_query->max_num_pages > 1 ) gantry_pagination($custom_query); ?>

		<?php /** End Pages Navigation **/ ?>
		
	</div><div class="clear"></div></div>	<script type="text/javascript">
    
	jQuery(window).load(function(){ 
	 
    
	
	
	
var mimonewswall = function () {
  
	  var mimocontainer = jQuery('#container_newswall');
      mimoitems = jQuery('.item2');
	  var LargeItem = mimoitems.filter('.large');
	  mimocontainer.imagesLoaded( function(){
      mimocontainer.isotope({
      itemSelector: '.item2',
	  resizesContainer : true,
	  masonry: {  columnWidth:  jQuery('#container_newswall .item2:last').outerWidth(true) },
      getSortData : {
      fitOrder : function( mimoitems ) {
        var order,
            index = mimoitems.index();
        
        if ( mimoitems.hasClass('large') && index % 2 ) {
          order = index + 1.5;
        } else {
          order = index;
        }
        return order;
      }
     },
     sortBy : 'fitOrder'
     })
	 .isotope('reLayout' )
      
      // trigger layout and sort
      .isotope({
    // update columnWidth to a percentage of container width
    masonry: { rowHeight: jQuery('#container_newswall .item2:last').outerWidth(true),
	 }
  	});
 	 });
  	jQuery(window).smartresize( function(){
  	mimonewswall();
	});
		
	
  
  <?php if ($gantry->get($mimo_page_type . '-infinitescroll') == 'auto'){ ?>
	
   jQuery(function() {
   
    var jQuerycontainer = jQuery('#container_newswall');
        jQuerycontainer.infinitescroll({
        navSelector  : '.navigation',    // selector for the paged navigation 
        nextSelector : '.navigation a',  // selector for the NEXT link (to page 2)
        itemSelector : '.item2', 
		behavior: 'twitter',    // selector for all items you'll retrieve
		debug: false,
		errorCallback: function() { 
          // fade out the error message after 2 seconds
          jQuery('#infscr-loading').animate({opacity:0},2000);   
        }
        },
         // trigger Isotope as a callback
    function( newElements ) {
      // hide new items while they are loading
      var $newElems = jQuery( newElements ).css({ opacity: 0 });
      // ensure that images load before adding to Isotope layout
      $newElems.imagesLoaded(function() {
        // show elems now they're ready
        $newElems.animate({ opacity: 1 });
	jQuerycontainer.append( $newElems ).isotope( 'appended', $newElems );
	$newElems.find('.flexslider').flexslider({
		selector: '.selector',
	    directionNav: true,
		slideshow: false,
		pauseOnAction: true,
		pauseOnHover: true,
		mousewheel:true, 
		easing: 'swing',
		video:true,
		controlNav: false, 
		before: function(slider){
			if ( jQuery('#youtube_video')[0] ) { 
		callPlayer('youtube_video', 'pauseVideo');}
		if ( jQuery('#player_1')[0] ) { 
		var myid ='player_1';
		jQuery("[id*="+myid+"]").each(function(){
			Froogaloop(  jQuery(this)[0] ).api('pause');});
      	}              //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
 
	  
		}});
		$newElems.hover(function(){
					jQuery(this).find('.all-article-social').stop().animate({'height': 'show'},'fast');
					}, function(){
					jQuery(this).find('.all-article-social').stop().animate({'height': 'hide'},'fast');
				});	
		$newElems.find('.flexslider').hover(function(){
					jQuery(this).children('.flex-direction-nav').stop().animate({height : 'show'},'fast');
					}, function(){
					jQuery(this).children('.flex-direction-nav').stop().animate({height : 'hide'},'fast');
				});	
		$newElems.find('.flex-direction-nav a').hover(function(){
					jQuery(this).stop().animate({opacity : 1},'fast');
					}, function(){
					jQuery(this).stop().animate({opacity : 0.7},'fast');
				});	
		mimonewswall();
      });
    });

});
  	<?php }; ?>
	<?php if ($gantry->get($mimo_page_type . '-infinitescroll') == 'button'){ ?>
	
	
		
	
	jQuery(function() {
    var jQuerycontainer = jQuery('#container_newswall');
        jQuerycontainer.infinitescroll({
         navSelector  : '.navigation',  // selector for the paged navigation 
        nextSelector : '.navigation a',  // selector for the NEXT link (to page 2)
        itemSelector : '.item2', 
		behavior: 'twitter',
		debug: false,
		errorCallback: function() { 
          // fade out the error message after 2 seconds
          jQuery('#infscr-loading').animate({opacity:0},2000);   
        }
        },
         // trigger Isotope as a callback
    function( newElements ) {
      // hide new items while they are loading
      var $newElems = jQuery( newElements ).css({ opacity: 0 });
      // ensure that images load before adding to Isotope layout
      $newElems.imagesLoaded(function() {
        // show elems now they're ready
        $newElems.animate({ opacity: 1 });
	jQuerycontainer.append( $newElems ).isotope( 'appended', $newElems );
	$newElems.find('.flexslider').flexslider({
		selector: '.selector',
	    directionNav: true,
		slideshow: false,
		pauseOnAction: true,
		pauseOnHover: true,
		mousewheel:true, 
		easing: 'swing',
		video:true,
		controlNav: false, 
		before: function(slider){
			if ( jQuery('#youtube_video')[0] ) { 
		callPlayer('youtube_video', 'pauseVideo');}
		if ( jQuery('#player_1')[0] ) { 
		var myid ='player_1';
		jQuery("[id*="+myid+"]").each(function(){
			Froogaloop(  jQuery(this)[0] ).api('pause');});
      	}              //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
 
	  
		}});		
		$newElems.hover(function(){
					jQuery(this).find('.all-article-social').stop().animate({'height': 'show'},'fast');
					}, function(){
					jQuery(this).find('.all-article-social').stop().animate({'height': 'hide'},'fast');
				});	
		$newElems.find('.flexslider').hover(function(){
					jQuery(this).children('.flex-direction-nav').stop().animate({height : 'show'},'fast');
					}, function(){
					jQuery(this).children('.flex-direction-nav').stop().animate({height : 'hide'},'fast');
				});	
		$newElems.find('.flex-direction-nav a').hover(function(){
					jQuery(this).stop().animate({opacity : 1},'fast');
					}, function(){
					jQuery(this).stop().animate({opacity : 0.7},'fast');
				});		
	 mimonewswall();
      });
    });
	});
jQuery(window).unbind('.infscr');

		jQuery('.navigation a').click(function(){
    jQuery('#container_newswall').infinitescroll('retrieve');
	jQuery('.navigation').show();
	
 return false;
 
});

	<?php }; ?>
  
	
};

	
mimonewswall();
	});
jQuery('#newswallmimo-filter a').click(function(){
	  var mimocontainer = jQuery('#container_newswall');
      var selected2 = jQuery(this);
      // don't proceed if already selected
      if ( selected2.hasClass('selected') ) {
        return;
      }

      var optionSet2 = selected2.parents('#newswallmimo-filter');
      // change selected class
      optionSet2.find('.selected').removeClass('selected');
      selected2.addClass('selected');

       var selector = jQuery(this).data('filter');
      mimocontainer.isotope({ filter: selector });

      return false;
    });

    </script>

		
	
	<?php else : ?>
																															
		<h1>
			<?php _re("Sorry, but there aren't any posts matching your query."); ?>
		</h1>
													
	<?php endif; ?>
													
	<?php wp_reset_query(); ?>
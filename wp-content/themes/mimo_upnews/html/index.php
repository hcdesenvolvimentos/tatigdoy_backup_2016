<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined( 'ABSPATH' ) or die( 'Restricted access' );
?>

<?php global $post, $posts, $query_string; 


	?>

	<div class="blog-featured">
	
		<?php /** Begin Page Heading **/ ?>

		<?php if( $gantry->get( 'blog-page-heading-enabled', '1' ) && $gantry->get( 'blog-page-heading-text' ) != '' ) : ?>
		
			<h1>
				<?php echo $gantry->get( 'blog-page-heading-text' ); ?>
			</h1>
		
		<?php endif; ?>
		
		<?php /** End Page Heading **/ ?>
		
		<?php /** Begin Query Setup **/ ?>
<?php echo '<div class="all_portfolio">'; ?>
			
			<?php if ($gantry->get( 'blog-page-filter-enabled', '1' ) ){
				
				if($gantry->get( 'blog-cat' ) ==! '') {
					
					 $terms = get_terms('category', array(
 	'include' => $ntax,
 	'hide_empty' => 1,
	'depth' => 1,
 ));  
					 
					} else {
						
						$terms = get_terms('category', array(
 	'hide_empty' => 1,
	'depth' => 1,
 ));    };
       
                 $count = count($terms); 
				  echo '<div class="allnewswallmimo-filter"><ul id="newswallmimo-filter">';  
                   
                 echo '<li><a class="selected" href="#" data-filter="*" title="">All</a></li>';  
                 if ( $count > 0 ){  
  
                        foreach ( $terms as $term ) {  
  
                            $termname = strtolower($term->name);  
                            $termname = str_replace(' ', '-', $termname);  
                            echo '<li><a href="#" title="" data-filter=".'.$termname.'">'.$term->name.'</a></li>';  
                        }  
                 }  
                 echo '</ul></div>'; 
             
                         }  

			 echo '<div class="rock_newswall">'; 
			wp_reset_query(); ?> 
            
            

						<ul id="container_newswall" class="container_newswall_class large<?php echo $gantry->get( 'blog-post-columns' );?>"> 

 
	
			<?php global $more, $post;    // Declare global $more (before the loop). ?>
		<?php 

		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$posts_per_page = get_option( 'posts_per_page' );
			
		if ( $gantry->get( 'blog-query-custom' ) != '' ) {
		
			$custom_query = new WP_Query( 'posts_per_page=' . $posts_per_page . '&paged=' . $paged . '&' . $gantry->get( 'blog-query-custom' ) );
		
		} else {
		
			$custom_query = new WP_Query( 'posts_per_page=' . $posts_per_page . '&paged=' . $paged . '&orderby=' . $gantry->get( 'blog-query-order', 'date' ) . '&cat=' . $gantry->get( 'blog-cat' ) . '&post_type=' . $gantry->get( 'blog-query-type', 'post' ) );
		
		}
		
		?>

		<?php /** End Query Setup **/ ?>

		<?php /** Begin Leading Posts **/ ?>

		

		<?php if( $custom_query->have_posts()  ) : ?>

			

				<?php while ( $custom_query->have_posts() ) : $custom_query->the_post();
				
				$terms = get_the_terms( $post->ID, 'category' );  

            if ( $terms && ! is_wp_error( $terms ) ) :  
            $links = array();  
  
            foreach ( $terms as $term )  
            {  
            $links[] = $term->name;  
            }  
            $links = str_replace(' ', '-', $links);  
            $tax = join( " ", $links );  
            else :  
            $tax = '';  
            endif;  
                       
           $more = 0;
		   
		    ?>

			 <li  class="mimo_newswall covernews <?php echo strtolower($tax); ?> item2 <?php  if (has_post_format( 'image' ) ):$post_type = 'imageitem'; endif; ?><?php if (has_post_format( 'link' ) ):$post_type = 'linkitem'; endif; ?><?php if (has_post_format( 'audio' ) ):$post_type = 'audioitem'; endif; ?><?php if (has_post_format( 'video' ) ):$post_type = 'videoitem'; endif; ?><?php if (has_post_format( 'gallery' ) ):$post_type = 'galleryitem'; endif; ?><?php if (has_post_format( 'aside' ) ):$post_type = 'asideitem'; endif; ?><?php if (has_post_format( 'status' ) ):$post_type = 'statusitem'; endif; ?><?php if (has_post_format( 'quote' ) ):$post_type = 'quoteitem'; endif; ?><?php if ( !get_post_format() ) { $post_type = 'standarditem'; }; echo $post_type;?> ">

					<?php

					$format = get_post_format();
					if ( false === $format )
						$format = 'standard';

					?>

					<?php $this->gantry_get_template_part( 'content/content', ( post_type_supports( get_post_type(), 'post-formats' ) ? $format : get_post_type() ) ); ?>

				</li>

				

				<?php endwhile; ?>

			</ul>
            </div>

		<?php endif; ?>

		<?php /** End Leading Posts **/ ?>

		<?php /** Begin Posts **/ ?>

		

		<?php /** End Posts **/ ?>
		<!-- ANTIGA POSIÇÃO DA NAVEGAÇÃO -->
		
	<div class="clear"></div></div><div class="clear"></div></div>	

		 <?php if ($gantry->get('blog-infinitescroll') == 'button'){ ?>
					<div class="navigation">
<?php next_posts_link('<i class="icon-plus"></i>',$custom_query->max_num_pages) ?>
</div>

            		<?php }; ?>
           
             <?php if ($gantry->get('blog-infinitescroll') == 'auto' ){ ?>
					<div class="navigation hidden">
<?php next_posts_link('More Posts',$custom_query->max_num_pages) ?>
</div>
            		</div>
            		<?php }; ?>
		<?php /** Begin Pages Navigation **/ ?>
			
		<?php if( $gantry->get( 'pagination-enabled', '1' ) && $custom_query->max_num_pages > 1 ) gantry_pagination($custom_query); ?>

		<?php /** End Pages Navigation **/ ?>

	<!--<script type="text/javascript">
    
	jQuery(window).load(function(){ 
	 
    
	
	
	
var mimonewswall = function () {
  
	  var mimocontainer = jQuery('#container_newswall');
      mimoitems = jQuery('.item2');
	  var LargeItem = mimoitems.filter('.large');
	  mimocontainer.imagesLoaded( function(){
      mimocontainer.isotope({
      itemSelector: '.item2',
	  resizesContainer : true,
	  masonry: {  columnWidth:  jQuery('#container_newswall .item2:last').outerWidth(true) },
      getSortData : {
      fitOrder : function( mimoitems ) {
        var order,
            index = mimoitems.index();
        
        if ( mimoitems.hasClass('large') && index % 2 ) {
          order = index + 1.5;
        } else {
          order = index;
        }
        return order;
      }
     },
     sortBy : 'fitOrder'
     })
	 .isotope('reLayout' )
      
      // trigger layout and sort
      .isotope({
    // update columnWidth to a percentage of container width
    masonry: { rowHeight: jQuery('#container_newswall .item2:last').outerWidth(true),
	 }
  	});
 	 });
  	jQuery(window).smartresize( function(){
  	mimonewswall();
	});
		
	
  
  <?php if ($gantry->get('blog-infinitescroll') == 'auto'){ ?>
	
   jQuery(function() {
   
    var jQuerycontainer = jQuery('#container_newswall');
        jQuerycontainer.infinitescroll({
        navSelector  : '.navigation',    // selector for the paged navigation 
        nextSelector : '.navigation a',  // selector for the NEXT link (to page 2)
        itemSelector : '.item2', 
		behavior: 'twitter',    // selector for all items you'll retrieve
		debug: false,
		errorCallback: function() { 
          // fade out the error message after 2 seconds
          jQuery('#infscr-loading').animate({opacity:0},2000);   
        }
        },
         // trigger Isotope as a callback
    function( newElements ) {
      // hide new items while they are loading
      var $newElems = jQuery( newElements ).css({ opacity: 0 });
      // ensure that images load before adding to Isotope layout
      $newElems.imagesLoaded(function() {
        // show elems now they're ready
        $newElems.animate({ opacity: 1 });
	jQuerycontainer.append( $newElems ).isotope( 'appended', $newElems );
	$newElems.find('.flexslider').flexslider({
		selector: '.selector',
	    directionNav: true,
		slideshow: false,
		pauseOnAction: true,
		pauseOnHover: true,
		easing: 'swing',
		mousewheel:true, 
		video:true,
		controlNav: false, 
		before: function(slider){
			if ( jQuery('#youtube_video')[0] ) { 
		callPlayer('youtube_video', 'pauseVideo');}
		if ( jQuery('#player_1')[0] ) { 
		var myid ='player_1';
		jQuery("[id*="+myid+"]").each(function(){
			Froogaloop(  jQuery(this)[0] ).api('pause');});
      	}              //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
 
	  
		}});
		$newElems.hover(function(){
					jQuery(this).find('.all-article-social').stop().animate({'height': 'show'},'fast');
					}, function(){
					jQuery(this).find('.all-article-social').stop().animate({'height': 'hide'},'fast');
				});	
		$newElems.find('.flexslider').hover(function(){
					jQuery(this).children('.flex-direction-nav').stop().animate({height : 'show'},'fast');
					}, function(){
					jQuery(this).children('.flex-direction-nav').stop().animate({height : 'hide'},'fast');
				});	
		$newElems.find('.flex-direction-nav a').hover(function(){
					jQuery(this).stop().animate({opacity : 1},'fast');
					}, function(){
					jQuery(this).stop().animate({opacity : 0.7},'fast');
				});	
		mimonewswall();
      });
    });

});
  	<?php }; ?>
	<?php if ($gantry->get('blog-infinitescroll') == 'button'){ ?>
	
	
		
	
	jQuery(function() {
    var jQuerycontainer = jQuery('#container_newswall');
        jQuerycontainer.infinitescroll({
         navSelector  : '.navigation',  // selector for the paged navigation 
        nextSelector : '.navigation a',  // selector for the NEXT link (to page 2)
        itemSelector : '.item2', 
		behavior: 'twitter',
		debug: false,
		errorCallback: function() { 
          // fade out the error message after 2 seconds
          jQuery('#infscr-loading').animate({opacity:0},2000);   
        }
        },
         // trigger Isotope as a callback
    function( newElements ) {
      // hide new items while they are loading
      var $newElems = jQuery( newElements ).css({ opacity: 0 });
      // ensure that images load before adding to Isotope layout
      $newElems.imagesLoaded(function() {
        // show elems now they're ready
        $newElems.animate({ opacity: 1 });
	jQuerycontainer.append( $newElems ).isotope( 'appended', $newElems );
	$newElems.find('.flexslider').flexslider({
		selector: '.selector',
	    directionNav: true,
		slideshow: false,
		pauseOnAction: true,
		pauseOnHover: true,
		easing: 'swing',
		video:true,
		mousewheel:true, 
		controlNav: false, 
		before: function(slider){
			if ( jQuery('#youtube_video')[0] ) { 
		callPlayer('youtube_video', 'pauseVideo');}
		if ( jQuery('#player_1')[0] ) { 
		var myid ='player_1';
		jQuery("[id*="+myid+"]").each(function(){
			Froogaloop(  jQuery(this)[0] ).api('pause');});
      	}              //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
 
	  
		}});
		$newElems.hover(function(){
					jQuery(this).find('.all-article-social').stop().animate({'height': 'show'},'fast');
					}, function(){
					jQuery(this).find('.all-article-social').stop().animate({'height': 'hide'},'fast');
				});	
		$newElems.find('.flexslider').hover(function(){
					jQuery(this).children('.flex-direction-nav').stop().animate({height : 'show'},'fast');
					}, function(){
					jQuery(this).children('.flex-direction-nav').stop().animate({height : 'hide'},'fast');
				});	
		$newElems.find('.flex-direction-nav a').hover(function(){
					jQuery(this).stop().animate({opacity : 1},'fast');
					}, function(){
					jQuery(this).stop().animate({opacity : 0.7},'fast');
				});		
	 mimonewswall();
      });
    });
	});
jQuery(window).unbind('.infscr');

		jQuery('.navigation a').click(function(){
    jQuery('#container_newswall').infinitescroll('retrieve');
	jQuery('.navigation').show();
	
 return false;
 
});

	<?php }; ?>
  
	
};

	
mimonewswall();
	});
jQuery('#newswallmimo-filter a').click(function(){
	  var mimocontainer = jQuery('#container_newswall');
      var selected2 = jQuery(this);
      // don't proceed if already selected
      if ( selected2.hasClass('selected') ) {
        return;
      }

      var optionSet2 = selected2.parents('#newswallmimo-filter');
      // change selected class
      optionSet2.find('.selected').removeClass('selected');
      selected2.addClass('selected');

       var selector = jQuery(this).data('filter');
      mimocontainer.isotope({ filter: selector });

      return false;
    });

    </script> -->
<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined( 'ABSPATH' ) or die( 'Restricted access' );

global $mimo_post_type;
// Create a shortcut for params.
$category = get_the_category();

 
 
 ?>
<?php  if (has_post_format( 'image' ) ):$mimo_post_type = 'image'; endif; ?><?php if (has_post_format( 'link' ) ):$mimo_post_type = 'linkpost'; endif; ?><?php if (has_post_format( 'audio' ) ):$mimo_post_type = 'audio'; endif; ?><?php if (has_post_format( 'video' ) ):$mimo_post_type = 'video'; endif; ?><?php if (has_post_format( 'gallery' ) ):$mimo_post_type = 'gallery'; endif; ?><?php if (has_post_format( 'aside' ) ):$mimo_post_type = 'aside'; endif; ?><?php if (has_post_format( 'status' ) ):$mimo_post_type = 'status'; endif; ?><?php if (has_post_format( 'quote' ) ):$mimo_post_type = 'quote'; endif; ?><?php if ( !get_post_format() ) { $mimo_post_type = 'blog'; }; ?>

			<?php /** Begin Post **/ ?>
					
			<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">

				
<div class="all_post">
				

				<?php get_template_part('html/content/slidertemplate'); ?>
               
				<?php /** Begin Article Title **/ ?>

				<?php if( $gantry->get($mimo_post_type .'-post-title-enabled', '1' ) ) : ?>
<div class="article-title">

				
					<!-- <h2 class="title-main">
                    
						<?php if( $gantry->get($mimo_post_type .'-post-title-link', '0' ) ) : ?>
							<a href="<?php the_permalink(); ?>" title="<?php esc_attr_e( get_the_title() ); ?>"><?php the_title(); ?></a>
						<?php else : ?>
							<?php the_title(); ?>
						<?php endif; ?>
					</h2> -->

					<h2 class="title-main">
                    
						<?php if( $gantry->get($mimo_page_type .'-post-title-link', '0' ) ) : ?>
							<a href="<?php the_permalink(); ?>" title="<?php esc_attr_e( get_the_title() ); ?>">
							<?php 

							$caracteres = mb_strlen(trim(get_the_title()));

								switch ($caracteres) {
									case($caracteres > 70):
										echo substr(get_the_title(), 0, 70)."...";
										break;

									case($caracteres <= 26):
										the_title(); echo '<br /><br /><br />'; 
										break;

									case($caracteres <= 51):
										the_title(); echo '<br /><br />'; 
										break;
									
									default:
										the_title();
										break;
								}
								
							?>
							</a>
						<?php else : ?>
							<?php 

							$caracteres = mb_strlen(trim(get_the_title()));

								switch ($caracteres) {
									case($caracteres > 70):
										echo substr(get_the_title(), 0, 70)."...";
										break;

									case($caracteres <= 26):
										the_title(); echo '<br /><br /><br />'; 
										break;

									case($caracteres <= 51):
										the_title(); echo '<br /><br />'; 
										break;
									
									default:
										the_title();
										break;
								}
								
							?>
						<?php endif; ?>
					</h2>

                    <div class="clear"> </div>
</div>
				<?php endif; ?>
                

				<?php /** End Article Title **/ ?>
                 <?php if( $gantry->get( $mimo_post_type .'-page-content-enabled', '1' ) ) : ?>
                     <div class="mimo-content">  
                     <div class="inside-mimo-content">      		
				<?php if( $gantry->get( $mimo_post_type .'-page-content', 'content' ) == 'excerpt' ) : ?>
			
				<?php the_excerpt(); ?>
								
			<?php else : ?>

				<?php the_content( false ); ?>
									
			<?php endif; ?>
				</div></div>
                <?php endif; ?>
                <?php get_template_part('html/content/articleinfotemplate'); ?>
                 
                
            
			</div>	</div>
			<?php /** End Post **/ ?>
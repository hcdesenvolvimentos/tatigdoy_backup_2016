<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */
// no direct access
defined( 'ABSPATH' ) or die( 'Restricted access' );

        
       


// Create a shortcut for params.
			$category = get_the_category();
			global $mimo_review_mb;$metareview = $mimo_review_mb->the_meta($post->ID);
			$mimo_review_mb->have_fields_and_multi('mimoreviews');
			$mimo_review_mb->the_field ('mimoreviews');
			$mimo_review_mb->the_field ('note'); 
			$mimo_review_mb->the_field ('summary'); 
			
			$urlnewswall = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			global $mimo_custom_mb;$meta = $mimo_custom_mb->the_meta($post->ID);$mimo_custom_mb->have_fields_and_multi('mimoimages');$mimo_custom_mb->the_field ('mimoimages');
			global $mimo_vimeo_mb;$metavimeo = $mimo_vimeo_mb->the_meta($post->ID);$mimo_vimeo_mb->have_fields_and_multi('mimovimeos');$mimo_vimeo_mb->the_field ('mimovimeos');
			global $mimo_youtube_mb;$metayoutube = $mimo_youtube_mb->the_meta($post->ID);$mimo_youtube_mb->have_fields_and_multi('mimoyoutubes');$mimo_youtube_mb->the_field ('mimoyoutubes');?>

			<?php /** Begin Post **/ ?>
			  <?php /** Begin Featured Image **/ ?>
			 <?php if(($meta) || ($metavimeo) || ($metayoutube)) { ?>	
					<div class="flexslider-single all_image_inside_content ">
						<div class="blog_thumb_image">
                    		<div  class="flexslider portfolio_slider format_images" >
    							<ul id="allimg_id<?php the_Id(); ?>" class="format_images slides">
                <?php if($meta):		
						foreach ($meta['mimoimages'] as $item ){
 							$imagen = wp_get_attachment_image_src($item, 'feature'); // Obtenemos la imagen "full". En vez de full podemos poner otro que dispongamos en nuestro tema
  							$n = 0;$n++;
 							$laurl = $item['imgurl'];
 							echo '<li class="selector delujo"><a data-rel="prettyPhoto" class="lightbox nopretty" href="'. $laurl .'"><img  src="'. $laurl .'"  title="'. $n .'"';
   							echo ' alt="none"';
  							echo ' /></a></li>';
 
							};
						endif; ?>
	
	
  				<?php if($metavimeo):	
				foreach($metavimeo['mimovimeos'] as $itemvideo){
 					$vimeourl = $itemvideo['ids'];
  					echo '<li class="selector"><div class="js-video [vimeo, widescreen]"><iframe id="player_1" class="vimeoiframe" src="http://player.vimeo.com/video/'. $vimeourl .'?api=1&player_id=player_1"  frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></li>';
 				};endif; ?>
    
   
  				<?php if($metayoutube):
						foreach($metayoutube['mimoyoutubes'] as $itemyoutube){
 						$youtubeurl = $itemyoutube['youtubeids'];
 					 	echo '<li class="selector"><div id="youtube_video"  class="js-video [vimeo, widescreen]"><iframe id="player_1"  height="100%" src="http://www.youtube.com/embed/'.$youtubeurl.'?enablejsapi=1" frameborder="0" wmode="Opaque" allowfullscreen></iframe></div></li>';
 						};
					endif;?>
                    
                  				</ul>
<script type="text/javascript">
    jQuery(document).ready(function(){
		jQuery('.format_images').flexslider({
		selector: '.selector',
	    directionNav: true,
		slideshow: false,
		pauseOnAction: true,
		pauseOnHover: true,
		easing: 'swing',
		video:true,
		controlNav: false, 
		smoothHeight: true,
		mousewheel:true, 
		before: function(slider){
			if ( jQuery('#youtube_video')[0] ) { 
		callPlayer('youtube_video', 'pauseVideo');}
		if ( jQuery('#player_1')[0] ) { 
		var myid ='player_1';
		jQuery("[id*="+myid+"]").each(function(){
			Froogaloop(  jQuery(this)[0] ).api('pause');});
      	}              //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
 
	  }});
		});
</script> 
						<div class="clear"></div>	
                     </div>
				</div>
				<?php get_template_part('html/content/socialtemplate'); ?>  
			</div> 
 

<?php } else if($urlnewswall) {   ?>
<div class="flexslider-single all_image_inside_content">
	<div class="blog_thumb_image">
		<div class="newswall-image portfolio_slider">
								<a data-rel="prettyPhoto" href="<?php echo $urlnewswall; ?>"><?php if(function_exists('the_post_thumbnail') && has_post_thumbnail()) : the_post_thumbnail('feature', array('class' => 'rt-image '.$gantry->get('thumb-position')
)); endif; ?>					

			<div class="clear"></div>
            </a>				
		</div>
     </div>
     <?php get_template_part('html/content/socialtemplate'); ?>  
</div>	<?php } else {}; ?>	
        		

				
			
  
                 
				<?php /** End Featured Image **/ ?>
				
			
			<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
  
                    

        
					
					<div class="clear"></div>
 
				

				   <?php /** Begin Article Title **/ ?>

				<?php if( $gantry->get( 'single-title-enabled', '1' ) ) : ?>
				<div class="article-title-single">
					<h1>
						<?php if( $gantry->get( 'single-title-link', '0' ) ) : ?>
							<a href="<?php the_permalink(); ?>" title="<?php esc_attr_e( get_the_title() ); ?>"><?php the_title(); ?></a>
						<?php else : ?>
							<?php the_title(); ?>
						<?php endif; ?>
					</h1><div class="clear"></div>
                 </div> 
              <div class="clear"></div>
				<?php endif; ?>
		
                <?php /** Begin Post Content **/ ?>		
				<div class="blog_content_standard">	
					<div class="mimo-content-single">  
                		<?php echo the_content(); ?>
                	</div>
                			
							
						
												
						<?php if( $gantry->get( 'single-footer', '1' ) ) : ?>
								<div class="post-footer">
									<small>
					
										<?php _re('This entry was posted'); ?>
										<?php /* This is commented, because it requires a little adjusting sometimes.
										You'll need to download this plugin, and follow the instructions:
										http://binarybonsai.com/archives/2004/08/17/time-since-plugin/ */
										/* $entry_datetime = abs(strtotime($post->post_date) - (60*120)); echo time_since($entry_datetime); echo ' ago'; */ ?>
										<?php _re('on'); ?> <?php the_time('l, F jS, Y') ?> <?php _re('at'); ?> <?php the_time() ?>
										<?php _re('and is filed under'); ?> <?php the_category(', ') ?>.
										<?php _re('You can follow any responses to this entry through the'); ?> <?php post_comments_feed_link('RSS 2.0'); ?> <?php _re('feed'); ?>.
				
										<?php if (('open' == $post->comment_status) && ('open' == $post->ping_status)) {
										// Both Comments and Pings are open ?>
										<?php _re('You can'); ?> <a href="#respond"><?php _re('leave a response'); ?></a>, <?php _re('or'); ?> <a href="<?php trackback_url(); ?>" rel="trackback"><?php _re('trackback'); ?></a> <?php _re('from your own site.'); ?>
				
										<?php } elseif (!('open' == $post->comment_status) && ('open' == $post->ping_status)) {
										// Only Pings are Open ?>
										<?php _re('Responses are currently closed, but you can'); ?> <a href="<?php trackback_url(); ?> " rel="trackback"><?php _re('trackback'); ?></a> <?php _re('from your own site.'); ?>
				
										<?php } elseif (('open' == $post->comment_status) && !('open' == $post->ping_status)) {
										// Comments are open, Pings are not ?>
										<?php _re('You can skip to the end and leave a response. Pinging is currently not allowed.'); ?>
				
										<?php } elseif (!('open' == $post->comment_status) && !('open' == $post->ping_status)) {
										// Neither Comments, nor Pings are open ?>
										<?php _re('Both comments and pings are currently closed.'); ?>
				
										<?php } edit_post_link(_r('Edit this entry'),'','.'); ?>
									</small>
								</div>
							<?php endif; ?>
							
							<?php get_template_part('html/content/articleinfotemplate'); ?>

							
						<?php /** Begin Comments **/ ?>
							<?php if( comments_open() && $gantry->get( 'single-comments-form-enabled', '1' ) ) : ?>
								<?php if ($gantry->get('wordpress-comments') == 'facebook') { ?>
									<div id="fb-root"></div>
										<script>
											window.fbAsyncInit = function() {
											FB.init({appId: '473914802626246', status: true, cookie: true,
											xfbml: true});
											};
											(function() {
												var e = document.createElement('script'); e.async = true;
												e.src = document.location.protocol +
												'//connect.facebook.net/en_US/all.js';
												document.getElementById('fb-root').appendChild(e);
												}());
										</script>
				        <div id="facebook-comments">
							<fb:comments href="<?php the_permalink(); ?>" num_posts="8" class="fb" ></fb:comments>
						</div>
						<?php   } else  { ?>
							<?php echo $gantry->displayComments( true, 'standard', 'standard' ); ?>
						<?php }; endif;  ?>
						<?php /** End Comments **/ ?>
						
					</div>
						<!--Begin  navigation -->
					  <?php if( $gantry->get( 'single-pagination', '1' ) ) : ?>
					   		<div class="navigation-single">
						   		<div class="">
							   		<?php if( get_previous_post() == true  ) :?><div class="first"><?php previous_post_link('%link'); ?>
							   			</div>
							   		<?php endif; ?>
							   		<?php if( get_next_post() == true  ) :?> 
							   			<div class="right">
							   				<?php  next_post_link('%link');?>
							   			</div>
							   		<?php endif; ?>
							   	</div>
							   	<div class="clear">
							</div>
						</div>
					<?php endif; ?>
					<!-- End navigation -->

				<?php wp_link_pages( 'before=<div class="pagination">' . _r( 'Pages:' ) . '&after=</div>' ); ?>
				
					<div id="tabvanilla">
					<ul class="tabnav-widget ui-tabs-nav">
						<li>
							<a class="" href="#tab1-widget">Escrito por</a>
						</li>
						<?php if($metareview): ?>
						<li>
							<a class="" href="#tab2-widget">Revisão</a>
						</li>
						<?php endif; ?>
						<li>
							<a class="" href="#tab3-widget">Postagens relacionados</a>
						</li>
						<li>
							<a class="" href="#tab4-widget">Nesta categoria</a>
						</li>
						
					</ul>
					<div class="clear"></div>
					 <?php /** Begin Author Bio **/ ?>
               <div id="tab1-widget" class="review-all tabdiv-widget ui-tabs-panel tabdiv-widget">
                <div class="mimo-author-bio">
                	 
                	<div class="mimo-author-inside">
                    	
                        
                        <div class="clear"></div>
                        <div class="author-bio">
                        <h4><?php the_author_meta( 'user_nicename' ); ?></h4>
                        <?php the_author_meta( 'description' ); ?> 
                        </div>
                        <div class="clear"></div>
                        <div class="review">
                        <div class="author-infos">
                        
                    		<div class="author-info-item">
                        		<a href="<?php the_author_meta( 'url' ); ?>" target="_blank"><i class="icon-plus"></i></a>
                    		</div>
                            <div class="author-info-item">
                            <a href="http://www.twitter.com/<?php echo the_author_meta( 'twitter' ); ?>" target="_blank"><i class="icon-twitter"></i></a>
                    		</div>
                        	<div class="author-info-item">
                            <a href="http://www.facebook.com/<?php echo the_author_meta( 'facebook' ); ?>" target="_blank"><i class="icon-facebook"></i></a>
                    		</div>
                        	<div class="author-info-item">
                             <a href="http://www.google.plus.com/<?php echo the_author_meta( 'google' ); ?>" target="_blank"><i class="icon-google-plus"></i></a>
                    		</div>
                            <div class="author-info-item">
                             <a href="http://www.linkedin.com/<?php echo the_author_meta( 'linkedin' ); ?>" target="_blank"><i class="icon-linkedin"></i></a>
                    		</div>
                             <div class="author-info-item">
                             <a href="http://www.flickr.com/<?php echo the_author_meta( 'flickr' ); ?>" target="_blank"><i class="icon-flickr"></i></a>
                    		</div>
                    </div></div>
                	</div>
                    
				</div>
                
                </div>
               
                <?php /** End Author Bio **/ ?>

						<?php /** Begin Reviews **/ ?>	
						<?php  
				
							if($metareview): 
							 $sum = 0; 
					  		$n = 0; 
							$note =  $metareview['note'];
							
							foreach($metareview['mimoreviews'] as $itemreview){
								$itemcriteria = $itemreview['ids'];
								$itemvalue = $itemreview['s_field'];
								$sum = $sum + $itemvalue;
	  							$n++;$total = $sum/$n;
								$totalnodecimal = number_format($total, 2, '.', ' ');  } ?>
                             
                            <div id="tab2-widget" class="review-all tabdiv-widget ui-tabs-panel tabdiv-widget">
                            	<div class="review">
                  					
                            	
							<?php
						foreach($metareview['mimoreviews'] as $itemreview){
							$itemcriteria = $itemreview['ids'];
							$itemvalue = 0;
							$itemvalue = $itemreview['s_field'];
							$sum = $sum + $itemvalue;
							$twenty = 20;
							$therealvalue = $itemvalue * $twenty;
							if(isset($metareview['summary'])): $summary =  $metareview['summary'];endif;
  							echo '<div class="mimo_review"><div class="criteria">'. $itemcriteria .'<span>'. $therealvalue .'%</span></div><div class="clear"></div>';
  							echo '<div class="thevaluebg" ><div class="thevalue" style="width:'. $therealvalue .'%;height:10px;"></div></div><div class="clear"></div></div>';
  							$n++;$total = $sum/$n;
							$totalnodecimal = number_format($total, 2, '.', ' '); ?> 
							<div class="none">
                            </div>
							<?php }?>
                            <?php $forty = 20;	$thetotalvalue = $totalnodecimal * $forty; ?>
                                 	<div  class="review-global">
                                    <div class="criteria_total">
                                    
                             					<h2><?php echo $note ?></h2><span><?php echo $thetotalvalue ?> %</span><div class="clear"></div>
                                                <div class="thevaluebg" ><div class="linkcolorbg thevalue" style="width:<?php echo $thetotalvalue ?>%;height:10px;"></div></div><div class="clear"></div>
                                			</div>
                                            <div class="clear"></div>
                                <?php  if(isset($metareview['summary'])): ?>
                                <div class="summary">
                                      	 <div class="module-title">
                  					<h2 class="title">summary</h2>
                                   
                    			</div>
                                
                                            <div class="criteria_summary"><?php echo $summary ?></div>
                                       
                            		</div> 
                                		   <?php endif; ?>    
                                    </div>
                                 
                                 </div>
                                
                             
                             
                             
                                
                                
                            
                    </div><?php endif; ?>
<?php /** End Reviews **/ ?>
				

				
				
				<?php /** End Post Content **/ ?>
               				
                <div id="tab3-widget" class="review-all tabdiv-widget ui-tabs-panel tabdiv-widget">
                
                <?php /** Begin Related Posts **/ ?>
                <div class="widget_gantry_archives widget"><ul>
		<?php
//for use in the loop, list 5 post titles related to first tag on current post
$tags = wp_get_post_tags($post->ID);
if ($tags) {
  
  $first_tag = $tags[0]->term_id;
  $args=array(
    'tag__in' => array($first_tag),
    'post__not_in' => array($post->ID),
    'showposts'=>5,
    'caller_get_posts'=>1
   );
  $my_query = new WP_Query($args);
  if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post(); ?>
      <li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
      <?php
    endwhile;
  }
}
?>
</ul></div>
<?php /** End Related Posts **/ ?>
<div class="clear"></div>
</div>
    <?php /** Begin More From This Cat Posts **/ ?>
                <?php echo mm_more_from_cat( 'in this category' ); ?>
                 <?php /** End More From This Cat Posts **/ ?>           
<div class="clear"></div>
			
   
				

</div>
            <div class="clear"></div>
         
		<script type="text/javascript">
			
			
		</script>		
			</div>
            
			<?php /** End Post **/ ?>
			
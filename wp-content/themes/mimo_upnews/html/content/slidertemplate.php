 <?php /** Begin Extended Meta **/ ?>

 <?php global $gantry, $mimo_page_type, $post; if (has_post_format( 'image' ) ):$mimo_page_type = 'image'; endif; ?><?php if (has_post_format( 'link' ) ):$mimo_page_type = 'linkpost'; endif; ?><?php if (has_post_format( 'audio' ) ):$mimo_page_type = 'audio'; endif; ?><?php if (has_post_format( 'video' ) ):$mimo_page_type = 'video'; endif; ?><?php if (has_post_format( 'gallery' ) ):$mimo_page_type = 'gallery'; endif; ?><?php if (has_post_format( 'aside' ) ):$mimo_page_type = 'aside'; endif; ?><?php if (has_post_format( 'status' ) ):$mimo_page_type = 'status'; endif; ?><?php if (has_post_format( 'quote' ) ):$mimo_page_type = 'quote'; endif; ?><?php if ( !get_post_format() ) { $mimo_page_type = 'standard'; }; ?>
 <?php if(is_single()):$mimo_page_type = 'single'; endif; ?>
 
 <?php /** Begin Featured Image **/ ?>
			<?php 			
				?>	
                            <?php if( $gantry->get($mimo_page_type .'-images-enabled', '1' ) ) : ?>
							<div class="all_image_inside_content">
								<div class="blog_thumb_image">
									<?php 
										global $mimo_custom_mb;
										$meta = $mimo_custom_mb->the_meta($post->ID);
										$mimo_custom_mb->have_fields_and_multi('mimoimages');
										$mimo_custom_mb->the_field ('mimoimages');
					  
										global $mimo_vimeo_mb;
										$metavimeo = $mimo_vimeo_mb->the_meta($post->ID);
										$mimo_vimeo_mb->have_fields_and_multi('mimovimeos');
										$mimo_vimeo_mb->the_field ('mimovimeos');
					  
										global $mimo_youtube_mb;
										$metayoutube = $mimo_youtube_mb->the_meta($post->ID);
										$mimo_youtube_mb->have_fields_and_multi('mimoyoutubes');
										$mimo_youtube_mb->the_field ('mimoyoutubes');

										$urlnewswall = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
											
										if( $gantry->get( $mimo_page_type .'-images-content' ) == 'featured' ) : 
			
											if($urlnewswall) :   ?>
												<div class="newswall-image portfolio_slider">
													<a href="<?php echo the_permalink(); ?>">
														<?php if(function_exists('the_post_thumbnail') && has_post_thumbnail()) : 
															the_post_thumbnail('feature', array('data-original' => $urlnewswall,'class' =>  'rt-image')); 
															endif; ?>					
															<div class="clear"></div>
													</a>				
												</div>	
											<?php endif; ?>	
								
											<?php else : ?>

											<?php $mimoid = get_the_ID();			
					
												if(($meta) || ($metavimeo) || ($metayoutube)) { ?>
													<div  class="flexslider portfolio_slider format_images" >
														<ul id="allimg_id<?php the_Id(); ?>" class="format_images slides">
															<?php if($meta):		
																foreach ($meta['mimoimages'] as $item ){
																	$imagen = wp_get_attachment_image_src($item, 'feature');  
																	$n = 0;
																	$n++;
																	$laurl = $item['imgurl'];
																	echo '<li class="selector delujo"><a class="lightbox nopretty" href="'.get_permalink( $mimoid ).'"><img  src="'. $laurl .'"  title="'. $n .'"';
																	echo ' alt="none" class="lazy"';
																	echo ' /></a></li>';
 
																	} ;
															endif;

															if($metavimeo):	
																foreach($metavimeo['mimovimeos'] as $itemvideo){
																	$vimeourl = $itemvideo['ids'];
																		echo '<li class="selector"><div class="js-video [vimeo, widescreen]">
      <iframe id="player_1" class="vimeoiframe" src="http://player.vimeo.com/video/'. $vimeourl .'?api=1&player_id=player_1"  frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
     </div></li>';
	 															};
	 														endif; ?>
	 														<?php if($metayoutube):
		 														foreach($metayoutube['mimoyoutubes'] as $itemyoutube){
			 														$youtubeurl = $itemyoutube['youtubeids'];
			 														echo '<li class="selector"><div id="youtube_video"  class="js-video [vimeo, widescreen]">
      <iframe id="player_1"  height="100%" src="http://www.youtube.com/embed/'.$youtubeurl.'?enablejsapi=1" frameborder="0" wmode="Opaque" allowfullscreen></iframe>
	   
     </div></li>';
	 															};
	 														endif; ?>
	 													</ul>
	 													<script type="text/javascript">
		 													jQuery(document).ready(function(){
			 													jQuery('.format_images').flexslider({
				 													selector: '.selector',
																    directionNav: true,
																	slideshow: false,
																	pauseOnAction: true,
																	pauseOnHover: true,
																	easing: 'swing',
																	video:true,
																	controlNav: false,
																	mousewheel:true, 
																	before: function(slider){
																		if ( jQuery('#youtube_video')[0] ) { 
																	callPlayer('youtube_video', 'pauseVideo');}
																	if ( jQuery('#player_1')[0] ) { 
																	var myid ='player_1';
																	jQuery("[id*="+myid+"]").each(function(){
																		Froogaloop(  jQuery(this)[0] ).api('pause');});
															      	}              
																  
																	}});
																	});
													</script> 
											</div>
									
										<?php }; ?>
				
									<?php endif; ?>     
								</div>
								<div class="clear"></div>
							</div> 
						<?php endif; ?>

				<?php /** End Featured Image **/ ?>

				                	
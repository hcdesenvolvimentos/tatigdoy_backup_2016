 <?php /** Begin Extended Meta **/ ?>

 <?php  if (has_post_format( 'image' ) ):$mimo_page_type = 'image'; endif; ?><?php if (has_post_format( 'link' ) ):$mimo_page_type = 'linkpost'; endif; ?><?php if (has_post_format( 'audio' ) ):$mimo_page_type = 'audio'; endif; ?><?php if (has_post_format( 'video' ) ):$mimo_page_type = 'video'; endif; ?><?php if (has_post_format( 'gallery' ) ):$mimo_page_type = 'gallery'; endif; ?><?php if (has_post_format( 'aside' ) ):$mimo_page_type = 'aside'; endif; ?><?php if (has_post_format( 'status' ) ):$mimo_page_type = 'status'; endif; ?><?php if (has_post_format( 'quote' ) ):$mimo_page_type = 'quote'; endif; ?><?php if ( !get_post_format() ) { $mimo_page_type = 'standard'; }; ?>
 <?php if(is_single()):$mimo_page_type = 'single'; endif; ?>
				<?php global $gantry;if( $gantry->get( $mimo_page_type . '-meta-author-enabled', '1' ) || $gantry->get( $mimo_page_type . '-meta-category-enabled', '1' ) || $gantry->get( $mimo_page_type . '-meta-date-enabled', '1' ) || $gantry->get( $mimo_page_type . '-meta-modified-enabled', '1' ) || $gantry->get( $mimo_page_type . '-meta-comments-enabled', '1' ) || $gantry->get( $mimo_page_type . '-meta-rating-enabled', '1' ) || $gantry->get( $mimo_page_type . '-formaticon-enabled', '1' ) ) : ?>
				<div class="all-article-info<?php if(is_single()): $single = '-single'; echo $single; endif; ?>">
					<ul class="article-info<?php if(is_single()): $single = '-single'; echo $single; endif; ?>">
					
					<?php if( $gantry->get( $mimo_page_type . '-formaticon-enabled', '1' ) ) : ?>

						<li class="format">
							<?php if (has_post_format( 'link' )):?><i class="icon-share-alt"></i><span class="ispan">link</span><?php endif; ?>
							<?php if (has_post_format( 'audio' )):?><i class="icon-music"></i>audio<?php endif; ?>
							<?php if (has_post_format( 'video' )):?><i class="icon-picture"></i>video<?php endif; ?>
							<?php if (has_post_format( 'gallery' )):?><i class="icon-th-large"></i>gallery<?php endif; ?>
							<?php if (has_post_format( 'aside' )):?><i class="icon-asterisk"></i>aside<?php endif; ?>
							<?php if (has_post_format( 'status' )):?><i class="icon-repeat"></i>status<?php  endif; ?>
							<?php if (has_post_format( 'quote' )):?><i class="icon-quote-right"></i>quote<?php endif; ?>
							<?php  if (has_post_format( 'image' )):?><i class="icon-picture"></i>image<?php endif; ?>
							<?php if ( !get_post_format() ) { ?><i class="icon-plus"></i><span class="ispan">standard</span><?php ; }; ?>
						</li>
					<?php endif; ?>
						
	
						<?php /** Begin Category **/ ?>

						<?php if( $gantry->get( $mimo_page_type . '-meta-category-enabled', '1' )  ) : ?>

							<li class="category-name"> 
								<?php 
									
									$category = get_the_category(); 
									echo $category[0]->cat_name;

								?>

								<?php if( $gantry->get( $mimo_page_type . '-meta-category-prefix' ) != '' ): echo $gantry->get( $mimo_page_type . '-meta-category-prefix' );endif; ?>
			
								<?php if( $gantry->get( $mimo_page_type . '-meta-category-link', '1' ) ) : ?>
									<?php echo $url; ?>
								<?php else : ?>
									<?php echo $title; ?>
								<?php endif; ?>
							</li>

						<?php endif; ?>

						<?php /** End Category **/ ?>

						<?php /** Begin Date & Time **/ ?>

						<?php if( $gantry->get( $mimo_page_type . '-meta-date-enabled', '1' ) ) : ?>

							<li class="create"> <?php if( $gantry->get( $mimo_page_type . '-meta-date-prefix' ) != '' ) echo $gantry->get( $mimo_page_type . '-meta-date-prefix' ) . ' '; ?><?php the_time( $gantry->get( $mimo_page_type . '-meta-date-format', 'd F Y' ) ); ?></li>

						<?php endif; ?>

						<?php /** End Date & Time **/ ?>

						<?php /** Begin Modified Date **/ ?>

						<?php if( $gantry->get( $mimo_page_type . '-meta-modified-enabled', '1' ) ) : ?>

							<li class="modified"> <?php if( $gantry->get( $mimo_page_type . '-meta-modified-prefix' ) != '' ) echo $gantry->get( $mimo_page_type . '-meta-modified-prefix' ) . ' '; ?><?php the_modified_date( $gantry->get( $mimo_page_type . '-meta-modified-format', 'd F Y' ) ); ?></li>

						<?php endif; ?>

						<?php /** End Modified Date **/ ?>

						<?php /** Begin Author **/ ?>
					
						<?php if( $gantry->get( $mimo_page_type . '-meta-author-enabled', '1' ) ) : ?>

							<li class="createdby"><?php if( $gantry->get( $mimo_page_type . '-meta-author-prefix' ) != '' ) echo $gantry->get( $mimo_page_type . '-meta-author-prefix' ) . ' '; ?><?php the_author(); ?></li>

						<?php endif; ?>

						<?php /** End Author **/ ?>

						<?php /** Begin Comments Count **/ ?>

						<?php if( $gantry->get( $mimo_page_type . '-meta-comments-enabled', '1' ) ) : ?>

							<?php if( $gantry->get( $mimo_page_type . '-meta-comments-link', '0' ) ) : ?>

								<li class="comments-count"> 
									<a href="<?php comments_link(); ?>">
										<?php comments_number( _r( '0' ), _r( '1' ), _r( '%' ) ); ?>
									</a>
								</li>

							<?php else : ?>

								<li class="comments-count"> <?php comments_number( _r( '0' ), _r( '1' ), _r( '%' ) ); ?></li>

							<?php endif; ?>

						<?php endif; ?>

						<?php /** End Comments Count **/ ?>
                                <?php /** Begin Rating Count **/ 
						  
									global $mimo_review_mb;$metareview = $mimo_review_mb->the_meta($post->ID);
									$mimo_review_mb->have_fields_and_multi('mimoreviews');$mimo_review_mb->the_field ('mimoreviews');
									$mimo_review_mb->the_field ('note'); $mimo_review_mb->the_field ('summary');
									 
									if($metareview): 
				 						$sum = 0; $n = 0; $note =  $metareview['note'];if(isset($metareview['summary'])):$summary =  $metareview['summary'];endif;
				 						foreach($metareview['mimoreviews'] as $itemreview){
					 						$itemcriteria = $itemreview['ids'];
					 						$itemvalue = $itemreview['s_field'];
					 						$sum = $sum + $itemvalue;
					 						$n++;$total = $sum/$n;
					 						$totalnodecimal = number_format($total, 0, ',', ' '); }?>
					 				<?php endif; ?>

						<?php if(( $gantry->get( $mimo_page_type . '-meta-rating-enabled', '1' ) ) && ($metareview)) : ?>
						

							<?php if( $gantry->get( $mimo_page_type . '-meta-rating-link', '0' ) ) : ?>

								<li class="rating-count"> 
									
										 				
                                    	<a href="<?php the_permalink(); ?>"><?php echo $totalnodecimal;?></a>

					
									
								</li>

							<?php else : ?>

								<li class="rating-count"> <?php echo $totalnodecimal;?></li>

							<?php endif; ?>

						<?php endif; ?>
                        
  <?php /** Begin Tags **/ ?>
				
				<?php if( has_tag() && $gantry->get( $mimo_page_type . '-meta-tags-enabled', '1' ) ) : ?>
							<li class="tagcloud">																																<?php the_tags('',','); ?>
							</li>
				<?php endif; ?>

				<?php /** End Tags **/ ?>

						<?php /** End Rating Count **/ ?>
                        <?php if( $gantry->get( $mimo_page_type . '-meta-edit-enabled', '1' ) ) : ?>
                        
                    
                		<?php edit_post_link( _r( 'Edit' ), '<li class="edit-link">', '</li>' ); ?>
                        
                		<?php endif; ?>
                		
                        </ul>
                    </div>
				<div class="clear"></div>
				<?php endif; ?>
                	
<?php  if (has_post_format( 'image' ) ):$post_type = 'image'; endif; ?><?php if (has_post_format( 'link' ) ):$post_type = 'link'; endif; ?><?php if (has_post_format( 'audio' ) ):$post_type = 'audio'; endif; ?><?php if (has_post_format( 'video' ) ):$post_type = 'video'; endif; ?><?php if (has_post_format( 'gallery' ) ):$post_type = 'gallery'; endif; ?><?php if (has_post_format( 'aside' ) ):$post_type = 'aside'; endif; ?><?php if (has_post_format( 'status' ) ):$post_type = 'status'; endif; ?><?php if (has_post_format( 'quote' ) ):$post_type = 'quote'; endif; ?><?php if ( !get_post_format() ) { $post_type = 'standard'; }; ?>
 <?php if(is_single()):$post_type = 'single'; endif; ?>

 					


<?php global $gantry;if( $gantry->get( $post_type . '-meta-facebook-enabled', '0' ) || $gantry->get( $post_type . '-meta-twitter-enabled', '0' ) || $gantry->get( $post_type . '-meta-google-enabled', '0' )) : ?><div class="all-article-social">
                    <dl class="article-social<?php if(is_single()): $single = '-single'; echo $single; endif; ?>">
                                                <?php /** Begin Facebook **/ ?>
					<dt></dt>
						<?php if( $gantry->get( $post_type . '-meta-facebook-enabled', '1' ) ) : ?>

							<dd class="facebookshare"><a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>" target="_blank"><i class="icon-facebook"></i></a></dd>

						<?php endif; ?>

						<?php /** End Facebook **/ ?>
                        <?php /** Begin Twitter **/ ?>
					
						<?php if( $gantry->get( $post_type . '-meta-twitter-enabled', '1' ) ) : ?>

							<dd class="twittershare"><a href="https://twitter.com/share?url=<?php echo the_permalink(); ?>" target="_blank"><i class="icon-twitter"></i></a></dd>

						<?php endif; ?>

						<?php /** End Twitter **/ ?>
                        <?php /** Begin Google **/ ?>
					
						<?php if( $gantry->get( $post_type . '-meta-google-enabled', '1' ) ) : ?>

							<dd class="googleshare"><a href="https://plus.google.com/share?url=<?php echo the_permalink(); ?>" target="_blank"><i class="icon-google-plus"></i></a></dd>

						<?php endif; ?>

						
						
					

				<?php /** End Extended Meta **/ ?>		
				 </dl>
                 <div class="clear"></div>
                </div>    
				
				<?php endif; ?>
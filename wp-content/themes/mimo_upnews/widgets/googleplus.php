<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

defined( 'GANTRY_VERSION' ) or die();
gantry_import('core.gantrygizmo');
gantry_import( 'core.gantrywidget' );
add_action('widgets_init', array("GantryWidgetGooglePlus","init"));
add_action('admin_head-widgets.php', array('GantryWidgetGooglePlus','addHeaders'),-1000);

class GantryWidgetGooglePlus extends GantryWidget {
	var $short_name = 'googleplus';
	var $wp_name = 'gantry_googleplus';
	var $long_name = 'Mimo GooglePlus';
	var $description = 'Mimo GooglePlus Widget';
	var $css_classname = 'widget_gantry_googleplus';
	var $width = 200;
	var $height = 400;

	function init() {
		register_widget( 'GantryWidgetGooglePlus' );
		wp_enqueue_script('googleplus');
	}
 function render_title($args, $instance) {
    	global $gantry;
    	if($instance['title'] != '') :
    		echo $instance['title'];
    	endif;
    }

 

	function render( $args, $instance ) {
		global $gantry;
		$googlepage =  $instance['googlepage'];
		
		ob_start();
		?>
		
     <div class="g-plus" data-href="https://plus.google.com/<?php echo $googlepage ?>" data-width="260"></div>
		<?php
		echo ob_get_clean();
	}
	function addHeaders(){
        global $gantry;
        $gantry->addScript(get_template_directory_uri() .'/js/admin/mimogoogleplus.js');
		
    }
}

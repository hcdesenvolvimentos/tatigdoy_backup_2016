<?php
/**
 
 * News Ticker Widget by framecero
 
 **/ 
 
defined('GANTRY_VERSION') or die();

gantry_import('core.gantrywidget');

add_action('widgets_init', array("GantryWidgetMimoTabs","init"));
add_action('admin_head-widgets.php', array('GantryWidgetMimoTabs','addHeaders'),-1000);
add_action('comment_post', array("GantryWidgetMimoTabs", 'gantry_flush_widget_cache'));
add_action('transition_comment_status', array("GantryWidgetMimoTabs", 'gantry_flush_widget_cache'));


class GantryWidgetMimoTabs extends GantryWidget {
    var $short_name = 'mimotabs';
    var $wp_name = 'gantry_mimotabs';
    var $long_name = 'Mimo Tabs';
    var $description = 'Mimo Custom Tabs Widget';
    var $css_classname = 'widget_gantry_mimotabs';
    var $width = 200;
    var $height = 400;
    
    function gantry_flush_widget_cache() {
		wp_cache_delete('gantry_mimotabs', 'widget');
	}

    function init() {
        register_widget("GantryWidgetMimoTabs");
    }
    
    function render_title($args, $instance) {
    	global $gantry;
    	if($instance['title'] != '') :
    		echo $instance['title'];
    	endif;
    }

    function render($args, $instance){
        global $gantry;
	    
	    ob_start();
	    
	    
	    
	 
		$tab1title = $instance['tab1title'];
		$tab2title = $instance['tab2title'];
		$tab3title = $instance['tab3title'];
		
			
		
		
		
		
		$cache = wp_cache_get('gantry_tabs', 'widget');

		if (!is_array($cache))
			$cache = array();

		if (isset($cache[$args['widget_id']])) {
			echo $cache[$args['widget_id']];
			return;
		}

 		
		 
		 global $gantry; ?>
		<div id="tabvanilla_widget" class="widget">
        <ul class="tabnav-widget">
        <?php /** Begin First Tab **/ if ($gantry->countModules('tab1')) : ?>
			<li><a class="" href="#mytab1-widget"><?php echo $tab1title ;?></a><div class="clear"></div></li>
            <?php /** End First Tab **/ endif; ?>
            <?php /** Begin Second Tab **/ if ($gantry->countModules('tab2')) : ?>
			<li><a class="" href="#mytab2-widget"><?php echo $tab2title ;?></a><div class="clear"></div></li>
            <?php /** End Second Tab **/ endif; ?>
            <?php /** Begin Third Tab **/ if ($gantry->countModules('tab3')) : ?>
			<li><a class="" href="#mytab3-widget"><?php echo $tab3title ;?></a><div class="clear"></div></li>
            <?php /** End Third Tab **/ endif; ?>
            <div class="clear"></div>
            </ul>
<?php /** Begin First Tab **/ if ($gantry->countModules('tab1')) : ?>
		<div id="mytab1-widget" class="tabdiv-widget">
			
				<?php echo $gantry->displayModules('tab1','standard','standard'); ?>
				<div class="clear"></div>
			
		</div>
		<?php /** End First Tab **/ endif; ?>
        <?php /** Begin Second Tab **/ if ($gantry->countModules('tab2')) : ?>
		<div id="mytab2-widget" class="tabdiv-widget">
			
				<?php echo $gantry->displayModules('tab2','standard','standard'); ?>
				<div class="clear"></div>
			
		</div>
		<?php /** End Second Tab **/ endif; ?>
        <?php /** Begin Third Tab **/ if ($gantry->countModules('tab3')) : ?>
		<div id="mytab3-widget" class="tabdiv-widget">
			
				<?php echo $gantry->displayModules('tab3','standard','standard'); ?>
				<div class="clear"></div>
			
		</div>
		<?php /** End Third Tab **/ endif; ?>
	
			
		
		
        </div>
<div class="clear"></div>

		

		<?php
		
		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('gantry_tabs', $cache, 'widget');
	    
	}
	function addHeaders(){
        global $gantry;
        $gantry->addScript(get_template_directory_uri() .'/js/admin/mimotabs.js');
		
    }	
}
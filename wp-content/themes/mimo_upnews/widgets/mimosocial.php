<?php
/**
 
 * Social Widget by framecero
 
 **/ 
 
defined('GANTRY_VERSION') or die();

gantry_import('core.gantrywidget');

add_action('widgets_init', array("GantryWidgetMimoSocial","init"));
add_action('admin_head-widgets.php', array('GantryWidgetMimoSocial','addHeaders'),-1000);


class GantryWidgetMimoSocial extends GantryWidget {
    var $short_name = 'mimosocial';
    var $wp_name = 'gantry_mimosocial';
    var $long_name = 'Mimo Custom Social';
    var $description = 'Mimo Custom Social Widget';
    var $css_classname = 'widget_gantry_mimosocial';
    var $width = 200;
    var $height = 400;
    
    

    function init() {
        register_widget("GantryWidgetMimoSocial");
    }
    function render_title($args, $instance) {
    	global $gantry;
    	if($instance['title'] != '') :
    		echo $instance['title'];
    	endif;
    }
    
    function render($args, $instance){
        global $gantry;
	    
	    ob_start();
	    
		$menu_class = $instance['menu_class'];
	    $title 	 = $instance['title'];
	    $twitter = $instance['twitter'];
	    $facebook = $instance['facebook'];
		  $google = $instance['google'];
		  $pinterest = $instance['pinterest'];
		  $linkedin = $instance['linkedin'];
		  $rss = $instance['rss'];
		  $mail = $instance['mail'];
			
		
		
		
		
		

 		?>
	<div>
		<ul class="mimo_social">
        	
		
			<?php if ($twitter !== ''){ ?>
				<li>
					<div class="all_icon twitterall">
					<div class="socialinline">
						<div class="socialhidden">
							<div class="socialtext">
								<?php echo mm_twitterCounts($twitter);?> <?php _e('Followers',"upnews"); ?>
							</div>
						</div>
					</div>
						<div class="twitter socialinline">
							<a href="https:&#47;&#47;twitter.com&#47;<?php echo $twitter; ?>" target="_blank" >
								<i class="icon-twitter"></i>
							</a>
						</div>
					</div>
	
				</li>
			<?php  };?>
			<?php if ($facebook !== ''){ ?>
				<li>
					<div class="all_icon facebookall">
					<div class="socialinline">
						<div class="socialhidden">
							<div class="socialtext">
								<?php echo mm_facebook_like_count($facebook);?> <?php _e('Likes',"upnews"); ?>
							</div>
						</div>
					</div>
						<div class="facebook socialinline">
							<a href="https:&#47;&#47;facebook.com&#47;<?php echo $facebook ?>" target="_blank" ><i class="icon-facebook"></i></a>
						</div>
					</div>
				</li>
			<?php  };?>
			<?php if ($google !== ''){  ?>
				<li>
					<div class="all_icon googleall">
						<div class="google socialinline">
							<a href="https:&#47;&#47;plus.google.com&#47;<?php echo $google; ?>" target="_blank" >
								<i class="icon-google-plus"></i>
							</a>
						</div>
					</div>
				</li>
			<?php  };?>
			<?php if ($pinterest !== ''){  ?>
				<li>
					<div class="all_icon pinterestall">
						<div class="pinterest socialinline">
							<a href="https:&#47;&#47;pinterest.com&#47;<?php echo $pinterest; ?>" target="_blank" >
								<i class="icon-pinterest"></i>
							</a>
						</div>
					</div>
				</li>
			<?php  };?>
			<?php if ($linkedin !== ''){  ?>
				<li>
					<div class="all_icon linkedinall">
						<div class="linkedin socialinline">
							<a href="https:&#47;&#47;linkedin.com&#47;<?php echo $linkedin; ?>" target="_blank" >
								<i class="icon-linkedin"></i>
							</a>
						</div>
					</div>
				</li>
			<?php  };?>
			<?php if ($rss !== ''){  ?>
				<li>
					<div class="all_icon rssall">
						<div class="socialinline">
							<div class="socialhidden">
								<div class="socialtext">
									RSS
								</div>
							</div>
						</div>
						<div class="rss socialinline">
							<a href="<?php echo $rss; ?>" target="_blank" >
								<i class="icon-rss"></i>
							</a>
						</div>
					</div>
				</li>
			<?php  };?>
			<?php if ($mail !== ''){  ?>
				<li>
					<div class="all_icon mailall">
						<div class="socialinline">
						<div class="socialhidden">
							<div class="socialtext">
							<?php _e('Mail us',"upnews"); ?>
							</div>
						</div>
					</div>
						<div class="mail socialinline">
							<a href="mailto:<?php echo $mail; ?>" target="_blank" >
								<i class="icon-mail"></i>
							</a>
						</div>
					</div>
					<div class="clear"></div>
				</li>			
			 <?php  };?>	
     
			 	
		</ul>
        <div class="clear"></div>	
    </div>
       

		<?php
		echo ob_get_clean();
	}

function addHeaders(){
        global $gantry;
        $gantry->addScript(get_template_directory_uri() .'/js/admin/mimosocial.js');
		
    }	    
	
} 
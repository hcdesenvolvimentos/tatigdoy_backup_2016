<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

defined( 'GANTRY_VERSION' ) or die();
gantry_import('core.gantrygizmo');
gantry_import( 'core.gantrywidget' );
add_action('widgets_init', array("GantryWidgetYoutube","init"));
add_action('admin_head-widgets.php', array('GantryWidgetYoutube','addHeaders'),-1000);

class GantryWidgetYoutube extends GantryWidget {
	var $short_name = 'youtube';
	var $wp_name = 'gantry_youtube';
	var $long_name = 'Mimo Youtube';
	var $description = 'Mimo Youtube Widget';
	var $css_classname = 'widget_gantry_youtube';
	var $width = 200;
	var $height = 400;

	function init() {
		register_widget( 'GantryWidgetYoutube' );
	}
 function render_title($args, $instance) {
    	global $gantry;
    	if($instance['title'] != '') :
    		echo $instance['title'];
    	endif;
    }

 

	function render( $args, $instance ) {
		global $gantry;
		$youtube =  $instance['youtube'];
		
		ob_start();
		?>
		<div id="youtube_video"  class="js-video [youtube, widescreen]">
      <iframe id="player_1"  height="100%" src="http://www.youtube.com/embed/<?php echo $youtube ?>?enablejsapi=1" frameborder="0" wmode="Opaque" allowfullscreen></iframe>
	   
     </div>
		<?php
		echo ob_get_clean();
	}
	function addHeaders(){
        global $gantry;
        $gantry->addScript(get_template_directory_uri() .'/js/admin/mimoyoutube.js');
		
    }
}

<?php
 /**
 
 * Horizontal Bar Widget by framecero
 
 **/ 
 
defined('GANTRY_VERSION') or die();

gantry_import('core.gantrywidget'); 

add_action('widgets_init', array("GantryWidgetMimoHorizontalbar","init"));
add_action('admin_head-widgets.php', array('GantryWidgetMimoHorizontalbar','addHeaders'),-1000);


class GantryWidgetMimoHorizontalbar extends GantryWidget {
    var $short_name = 'mimohorizontalbar';
    var $wp_name = 'gantry_mimohorizontalbar';
    var $long_name = 'Mimo Custom Horizontal Bar';
    var $description = 'Insert line separator';
    var $css_classname = 'widget_gantry_mimohorizontalbar';
    var $width = 200;
    var $height = 400;
    
    

    function init() {
        register_widget("GantryWidgetMimoHorizontalbar");
    }
    
   

    function render($args, $instance){
        global $gantry;
	    
	    ob_start();
	    
	    $type = $instance['type'];
		 $color = $instance['color'];
		  $plus = $instance['plus'];
		
		
		
		
		
		
		

 		?>
		
		
        
				 <?php if ($plus == 'before'){ ?>
                 <i class="icon-plus <?php echo $color; ?>i"></i><div class="clear"></div>
                    <?php  };?>
	 	<div class="<?php echo $type; ?> <?php echo $color; ?>"><div class="clear"></div>	</div>
			<?php if ($plus == 'after'){ ?>
                 <i class="icon-plus <?php echo $color; ?>i"></i><div class="clear"></div>
                    <?php  };?>
     
     
     
     
     
     
     
     
     

			
		
		
<div class="clear"></div>	
		<?php
		echo ob_get_clean();
	}

function addHeaders(){
        global $gantry;
        $gantry->addScript(get_template_directory_uri() .'/js/admin/mimohorizontalbar.js');
		
    }	    
	
} 
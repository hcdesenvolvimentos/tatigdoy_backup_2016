<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

defined( 'GANTRY_VERSION' ) or die();
gantry_import('core.gantrygizmo');
gantry_import( 'core.gantrywidget' );
add_action('widgets_init', array("GantryWidgetFlickr","init"));
add_action('admin_head-widgets.php', array('GantryWidgetFlickr','addHeaders'),-1000);

class GantryWidgetFlickr extends GantryWidget {
	var $short_name = 'flickrfeed';
	var $wp_name = 'gantry_flickr';
	var $long_name = 'Mimo Flickr';
	var $description = 'Mimo Flickr Widget';
	var $css_classname = 'widget_gantry_flickr';
	var $width = 200;
	var $height = 400;

	function init() {
		register_widget( 'GantryWidgetFlickr' );
		wp_enqueue_script('flickr');
	}
 function render_title($args, $instance) {
    	global $gantry;
    	if($instance['title'] != '') :
    		echo $instance['title'];
    	endif;
    }

 

	function render( $args, $instance ) {
		global $gantry;
		$flickr =  $instance['flickr'];
		$limit =  $instance['limit'];
		
		ob_start();
		?>
		
			
            <script>jQuery(document).ready(function(){jQuery('#basicuse').jflickrfeed({
	limit: <?php echo $limit ?>,
	qstrings: {
		id: '<?php echo $flickr ?>'
	},
	itemTemplate: 
	'<li><a class="lightbox nopretty" href="http://www.flickr.com/photos/<?php echo $flickr ?>/" target="_blank"><img src="{{image}}" alt="{{title}}" /></a></li>'
});});</script>
<ul id="basicuse" class="thumbs"></ul>
		<?php
		echo ob_get_clean();
	}
	function addHeaders(){
        global $gantry;
        $gantry->addScript(get_template_directory_uri() .'/js/admin/mimoflickr.js');
		
    }
}

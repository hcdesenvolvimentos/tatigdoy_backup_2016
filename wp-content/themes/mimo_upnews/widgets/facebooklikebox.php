<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

defined( 'GANTRY_VERSION' ) or die();
gantry_import('core.gantrygizmo');
gantry_import( 'core.gantrywidget' );
add_action('widgets_init', array("GantryWidgetFacebook","init"));
add_action('admin_head-widgets.php', array('GantryWidgetFacebook','addHeaders'),-1000);

class GantryWidgetFacebook extends GantryWidget {
	var $short_name = 'facebooklikebox';
	var $wp_name = 'gantry_facebook';
	var $long_name = 'Mimo Facebook';
	var $description = 'Mimo Facebook Widget';
	var $css_classname = 'widget_gantry_facebook';
	var $width = 200;
	var $height = 400;

	function init() {
		register_widget( 'GantryWidgetFacebook' );
	}
 function render_title($args, $instance) {
    	global $gantry;
    	if($instance['title'] != '') :
    		echo $instance['title'];
    	endif;
    }

 

	function render( $args, $instance ) {
		global $gantry;
		$facebookuser =  $instance['facebookuser'];
		$facebookappid =  $instance['facebookappid'];
		$showfaces =  $instance['showfaces'];
		$showstream =  $instance['showstream'];
		$showheader =  $instance['showheader'];
		$boxheight =  $instance['boxheight'];
		
		ob_start();
		?>
	
 
<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2F<?php echo $facebookuser ?>&amp;width=292&amp;height=590&amp;show_faces=<?php echo $showfaces ?>&amp;colorscheme=light&amp;stream=<?php echo $showstream ?>&amp;border_color=%23ffffff&amp;header=<?php echo $showheader ?>&amp;appId=<?php echo $facebookappid ?>" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:<?php echo $boxheight ?>px;border-color:#ffffff;" allowTransparency="true"></iframe>


		<?php
		echo ob_get_clean();
	}
	function addHeaders(){
        global $gantry;
        $gantry->addScript(get_template_directory_uri() .'/js/admin/mimofacebook.js');
		
    }
}

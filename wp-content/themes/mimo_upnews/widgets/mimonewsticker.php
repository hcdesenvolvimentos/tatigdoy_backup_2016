<?php
/**
 
 * News Ticker Widget by framecero
 
 **/ 
 
defined('GANTRY_VERSION') or die();

gantry_import('core.gantrywidget');

add_action('widgets_init', array("GantryWidgetMimoNewsTicker","init"));
add_action('admin_head-widgets.php', array('GantryWidgetMimoNewsTicker','addHeaders'),-1000);
add_action('comment_post', array("GantryWidgetMimoNewsTicker", 'gantry_flush_widget_cache'));
add_action('transition_comment_status', array("GantryWidgetMimoNewsTicker", 'gantry_flush_widget_cache'));


class GantryWidgetMimoNewsTicker extends GantryWidget {
    var $short_name = 'mimonewsticker';
    var $wp_name = 'gantry_mimonewsticker';
    var $long_name = 'Mimo NewsTicker';
    var $description = 'Mimo Custom NewsTicker Widget';
    var $css_classname = 'widget_gantry_mimonewsticker';
    var $width = 200;
    var $height = 400;
    
    function gantry_flush_widget_cache() {
		wp_cache_delete('gantry_mimonewsticker', 'widget');
	}

    function init() {
        register_widget("GantryWidgetMimoNewsTicker");
    }
     function render_title($args, $instance) {
    	global $gantry;
    	if($instance['title'] != '') :
    		echo $instance['title'];
    	endif;
    }
    
    function render($args, $instance){
        global $gantry, $more, $post;
	    
	    ob_start();
	    $menu_class = $instance['menu_class'];
	    $title 	 = $instance['title'];
	    $number = $instance['number'];
	    $cat = $instance['cat'];
		$showexcerpt = $instance['showexcerpt'];
		$showtitle = $instance['showtitle'];
		$showdate = $instance['showdate'];
		$showcomments = $instance['showcomments'];
		$showreadon = $instance['showreadon'];
		$showrating = $instance['showrating'];
			
		
		
		
		
		$cache = wp_cache_get('gantry_newsticker', 'widget');

		if (!is_array($cache))
			$cache = array();

		if (isset($cache[$args['widget_id']])) {
			echo $cache[$args['widget_id']];
			return;
		}

 		$rp = new WP_Query(array('showposts' => $number, 'nopaging' => 0, 'post_status' => 'publish', 'ignore_sticky_posts' => 1, 'category_name' => $cat));
		if ($rp->have_posts()) : ?>
		
        <div class="all-ticker">
        <div class="icon-ticker">
								<i class="icon-chevron-right"></i> 
							</div>	
        	<div class="uls">
        	
				<ul id="ticker"  >
		
					<?php  while ($rp->have_posts()) : $rp->the_post(); ?>
						<li class="recent news-item">
							 
								<div class="all-ticker-content">
								<?php if ($showtitle !== 'no'){ ?>
									<div class="article-title-ticker">
										<a title="<?php the_title(); ?>" href="<?php echo the_permalink(); ?>" class="mimo_ticker_link"><?php the_title(); ?></a>
									</div>
								<?php  };?>
                     
								<?php if (($showdate !== 'no') || ($showcomments !== 'no') || ($showrating !== 'no')){ ?>
									<div class="all-article-info-ticker">
										<dl class="article-info">
											<dt></dt>
												<?php if ($showdate !== 'no'){ ?>
													<dd class="create">
														<?php the_time('d/m') ?>
													</dd>
												<?php  };?>
                  
												<?php if ($showcomments !== 'no'){ ?>
													<dd class="comments-count">
														<a href="<?php comments_link(); ?>">
															<?php comments_number( _r( '0' ), _r( '1' ), _r( '%' ) ); ?>
														</a>
													</dd>
												<?php  };?>
												<?php if ($showrating !== 'no'){ 
			
														global $mimo_review_mb;
														$metareview = $mimo_review_mb->the_meta($post->ID);
														$mimo_review_mb->have_fields_and_multi('mimoreviews');$mimo_review_mb->the_field ('mimoreviews');
														$mimo_review_mb->the_field ('note'); $mimo_review_mb->the_field ('summary'); 
														if($metareview): 
														$sum = 0; $n = 0; $note =  $metareview['note'];if(isset($metareview['summary'])):$summary =  $metareview['summary'];endif;
														foreach($metareview['mimoreviews'] as $itemreview){
															$itemcriteria = $itemreview['ids'];
															$itemvalue = $itemreview['s_field'];
															$sum = $sum + $itemvalue;
															$n++;$total = $sum/$n;
															$totalnodecimal = number_format($total, 0, ',', ' '); }
														endif; ?>
			
				
													<dd class="rating-count"> 
														<a href="<?php echo the_permalink(); ?>"><?php echo $totalnodecimal;?></a>
													</dd>
												<?php  };?> 
											</dl>
										</div>
									<?php  };?>
   
										<?php if ($showexcerpt !== 'no'){ ?>
											<div class="ticker_content">
												<?php the_excerpt(); ?>
											</div>
										<?php  };?>
                  
										<?php if ($showreadon !== 'no'){ ?>
											<div class="readon-div">
												<a href="<?php echo the_permalink(); ?>" class="readon_ticker"><?php _e('Read more', 'upnews'); ?></a>
											</div>
											
										
										<?php  };?>
    
								</div>
									</li>
	
								<?php endwhile; ?>
							</ul>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>

<script>

	function tick(){
		jQuery('#ticker li:first').slideUp( function () { jQuery(this).appendTo(jQuery('#ticker')).slideDown(); });
	}
	setInterval(function(){ tick () }, 5000);


</script>
        
		<?php wp_reset_query(); ?><?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;
		
		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('gantry_newsticker', $cache, 'widget');
	    
	}
	function addHeaders(){
        global $gantry;
        $gantry->addScript(get_template_directory_uri() .'/js/admin/mimonewsticker.js');
        $gantry->addStyle(get_template_directory_uri() .'/css/admin/mimo_admin_widgets.css');
		
    }	
}
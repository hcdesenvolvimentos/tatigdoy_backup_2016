<?php
/**
 * @version   4.0.4 March 22, 2013
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

defined( 'GANTRY_VERSION' ) or die();
gantry_import('core.gantrygizmo');
gantry_import( 'core.gantrywidget' );
add_action('widgets_init', array("GantryWidgetVimeo","init"));
add_action('admin_head-widgets.php', array('GantryWidgetVimeo','addHeaders'),-1000);

class GantryWidgetVimeo extends GantryWidget {
	var $short_name = 'vimeo';
	var $wp_name = 'gantry_vimeo';
	var $long_name = 'Mimo Vimeo';
	var $description = 'Mimo Vimeo Widget';
	var $css_classname = 'widget_gantry_vimeo';
	var $width = 200;
	var $height = 400;

	function init() {
		register_widget( 'GantryWidgetVimeo' );
	}
 function render_title($args, $instance) {
    	global $gantry;
    	if($instance['title'] != '') :
    		echo $instance['title'];
    	endif;
    }

 

	function render( $args, $instance ) {
		global $gantry;
		$vimeo =  $instance['vimeo'];
		
		ob_start();
		?>
		<div class="js-video [vimeo, widescreen]">
      <iframe id="player_1" class="vimeoiframe" src="http://player.vimeo.com/video/<?php echo $vimeo ?>?api=1&player_id=player_1"  frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
		<?php
		echo ob_get_clean();
	}
	function addHeaders(){
        global $gantry;
        $gantry->addScript(get_template_directory_uri() .'/js/admin/mimovimeo.js');
		
    }
}
